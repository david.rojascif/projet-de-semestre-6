import {createApp} from 'vue'
import App from './App.vue'
import Store from './plugins/vuexStore'
import 'vuetify/styles'
import {createVuetify} from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const myBlueTheme = {
    dark: false,
    colors: {
        background: '#828af1',
        surface: '#FFFFFF',
        primary: '#6200EE',
        'primary-darken-1': '#3700B3',
        secondary: '#03DAC6',
        'secondary-darken-1': '#018786',
        error: '#B00020',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FB8C00',
    }
}

const vuetify = createVuetify({
    components,
    directives,
    theme: {
        defaultTheme: 'myBlueTheme',
        themes: {
            myBlueTheme,
        }
    },
})

const store = Store()

createApp(App)
    .use(vuetify)
    .use(store)
    .mount('#app')
