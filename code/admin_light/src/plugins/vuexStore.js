import {createStore} from 'vuex';

const store ={
    state: {
        queuedMsgCounters: [
            [10, 9, 1],
            [2, 0, 1],
            [0, 0, 1],
            [2, 2, 0],
            [6, 2, 0],
            [3, 1, 0],
            [2, 0, 1],
            [2, 0, 0],
            [2, 0, 0]
        ],
        bufferedMsgCounters: [2, 2, 2, 2, 3, 2, 2, 3, 3],
        aliases: [
            ["Spencer", "Mike", "Max"],
            [null, null, null],
            [null, null, null],
            [null, "Tony", null],
            ["Kevin", null, null],
            [null, null, null],
            [null, null, "Thomas"],
            [null, null, null],
            [null, null, null]
        ],
        connectivity: [
            [false, true, false, false, true, false, false, false, false],
            [true, false, true, false, true, false, false, false, false],
            [false, true, false, false, false, true, false, false, false],
            [false, false, false, false, true, false, true, true, false],
            [true, true, false, true, false, true, false, false, true],
            [false, false, true, false, true, false, false, false, true],
            [false, false, false, true, false, false, false, true, false],
            [false, false, false, true, false, false, true, false, true],
            [false, false, false, false, true, true, false, true, false]
        ],
        snapshotMessage:{
            type : "Snapshot",
            param:{
                action : "write",
                course : "HEIA-FR Téléinformatique 1",
                class : "T-a",
                round : 1,
                comment : "Snapshot de la classe T-a round 1"
            }
        },
        mainMenuItems: [
            {
                title: 'Set up',
                disabled:false,
                active: true,
                id: "Phase1",
            }, {
                title: 'Playing',
                disabled:true,
                active: false,
                id: "Phase2",
            }, {
                title: 'End',
                disabled:true,
                active: false,
                id: "Phase3",
            },

        ]

    },
    socket:null,
    windowState:true,
    setUpState:false,
    nodePlayerToSend:-1,
    mutations: {
        monitorInit(state,data) {
            state.queuedMsgCounters=data.queuedMsgCounters
            state.bufferedMsgCounters=data.bufferedMsgCounters
            state.aliases=data.aliases
        },
        connectivityUpdate(state,data){
            state.connectivity=data.connectivity
        },
        saveSnapshot(state,message){
            state.snapshotMessage=message
        },
        setActive(state,index) {
            state.mainMenuItems.forEach((item) => {
                item.active = state.mainMenuItems[index] === item;
                item.disabled = true;
            })
           state.mainMenuItems[index].disabled=false
        },
        setSocket(state,webSocket){
            state.socket=webSocket
        },
        setWindowState(state,windowStateValue){
            state.windowState=windowStateValue;
        },
        getWindowState(state){
            return state.windowState;
        },
        setSetUpState(state,setupStateValue){
            state.setUpState=setupStateValue;
        },
        getSetUpState(state){
            return state.setUpState;
        },
        setNodePlayer(state,nodeVal){
            state.nodePlayerToSend=nodeVal;
        }
    },
    actions:{
        monitorInit({commit,state}, data) {
            commit('monitorInit',data)
        },
        connectivityUpdate({commit,state}, data) {
            commit('connectivityUpdate',data)
        },
        saveSnapshot({commit,state}, message) {
            commit('saveSnapshot',message)
        },
        setActive({commit,state},index){
            commit('setActive',index)
        },
        setSocket({commit,state}, webSocket) {
            commit('setSocket',webSocket)
        },
        setWindowState({commit,state}, windowStateValue) {
            commit('setWindowState',windowStateValue)
        },
        getWindowState({commit,state}) {
            return commit('getWindowState');
        },
        setSetUpState({commit,state}, setUpStateValue) {
            commit('setSetUpState',setUpStateValue)
        },
        getSetUpState({commit,state}) {
            return commit('getSetUpState');
        },
        setNodePlayer({commit,state},nodeToSend){
            commit('setNodePlayer',nodeToSend);
        }
    }
}

export default function (){
    return createStore(store)
}
