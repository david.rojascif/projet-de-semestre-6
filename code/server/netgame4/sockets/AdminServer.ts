import { default as WebSocket, default as ws } from 'ws';
import { logger } from '../log';
import { WsMessage, WsParam } from './messages/WsMessage';
import { Admin } from '../netgame/network/Admin';
import { Admin2Controller } from '../controllers/Admin2Controller';
import { ConnectivityUpdate } from './messages/monitor/ConnectivityUpdate';
import { MonitorInit } from './messages/monitor/MonitorInit';

export class AdminServer {

    /** The websocket server used to handle the /websocket/admin route */
    public static readonly server = new ws.Server({ noServer: true });

    /** set containing every connected admin */
    public static admins = new Map();

    /**
     * broadcast a message to all the admin interfaces connected to the server
     * @param msg : message to send to the admin interfaces
     */
    public static broadcast(msg: WsMessage<WsParam>): void {
        this.server.clients.forEach(client => client.send(JSON.stringify(msg), err => { if (err) logger.error(err) }));
    }

    /**
     * Close the WS connection of a specific admin
     * @param id
     */
     public static closeWS(id : number): void {
        const admin = AdminServer.admins.get(id);
        if (typeof admin !== "undefined") {
            admin._socket.close();
        }
    }
}

/** Keep track of the websockets id */
let adminWsId: number = 0;

AdminServer.server.on('connection', (ws) => {
    const id = adminWsId++;
    logger.info("New admin (now " + AdminServer.server.clients.size + " active)");

    let adminWS = new Admin(ws);
    AdminServer.admins.set(id,adminWS);

    // On connection send the config and connectivity matrix
    ws.send(JSON.stringify(new MonitorInit()));
    ws.send(JSON.stringify(new ConnectivityUpdate()));


    ws.onopen = function (event: WebSocket.CloseEvent) {
        logger.debug(event.reason);
    };


    ws.onmessage = function (event: WebSocket.MessageEvent) {
        //logger.info(event.data);
        // Dispatch the incoming message
        let message = JSON.parse(event.data.toString()) as WsMessage<WsParam>;
        Admin2Controller.dispatch(adminWS, message);
    };

    ws.onclose = function (event: WebSocket.CloseEvent) {
        logger.info("admin has gone (" + AdminServer.server.clients.size + " remain)");
        logger.debug(event.reason);
    };

    ws.onerror = (event: WebSocket.ErrorEvent) => logger.error(event.message);
});