import { default as WebSocket, default as ws } from 'ws';
// NetGame core
import { NetGame } from "../netgame/Netgame";
// messages
import { WsMessage, WsParam } from './messages/WsMessage';
import { ConnectivityUpdate } from './messages/monitor/ConnectivityUpdate';
import { MonitorInit } from './messages/monitor/MonitorInit';
import { HideOverlay, ToggleOverlay, ToggleOverlayParams } from './messages/monitor/ToggleOverlay';
// monitor class
import { Monitor } from "./Monitor";
// logger
import { logger } from '../log';

/**
 * WebSocket server for monitors
 */
export class MonitorServer {

    /** The websocket server used to handle the /websocket/monitor route */
    public static readonly server = new ws.Server({noServer: true});

    /** true if an overlay is currently diplayed */
    private static _currentOverlay: string = null;

    /** number of monitors with role game */
    public static monitorGameCount = 0;

    /** number of monitors with role admin */
    public static monitorAdminCount = 0;

    public static monitors = new Map();

    /** returns true if an overlay is displayer, else false */
    public static get currentOverlay(): string {
        return this._currentOverlay ?? "none";
    }

    public static isOverlayDisplayed(): boolean {
        return this._currentOverlay != null;
    }

    /**
     * broadcast a message to all the monitors connected to the server
     * @param msg : message to send to the monitors
     */
    public static broadcast(msg: WsMessage<WsParam>): void {
        this.server.clients.forEach(client => client.send(JSON.stringify(msg), err => {
            if (err) logger.error(err)
        }));
    }

    /**
     * Hide the overlay that is currently displayed
     */
    public static hideOverlay(): void {
        this.broadcast(new HideOverlay());
        this._currentOverlay = null;
    }

    /**
     * if no overlay is shown currently, display the overlay
     * @param overlay : the name fo the overlay to display
     */
     public static showOverlay(overlay: string): string {
        if (this.isOverlayDisplayed()) return "Can't show overlay, another one is already displayed";
        if (MonitorServer.monitorGameCount<=0) return "Can't show "+overlay+" overlay, there's no game monitor active !";
        this.broadcast(new ToggleOverlay(new ToggleOverlayParams(overlay)));
        this._currentOverlay = overlay;
        return "";
    }

    /**
     * if no overlay is shown currently, display the routing overlay
     * @param overlay : the routing overlay parameters
     */
     public static showRoutingOverlay(params: ToggleOverlayParams): string {
        if (this.isOverlayDisplayed()) return "Can't show overlay, another one is already displayed";
        if (MonitorServer.monitorGameCount<=0) return "Can't show routing overlay, there's no game monitor active !";
        const toggleOverlay = new ToggleOverlay(params);
        this.broadcast(toggleOverlay);
        this._currentOverlay = "routing";
        return "";
    }

    /**
     * Close the WS connection of a specific monitor
     * @param id
     */
    public static closeWS(id : number): void {
        const monitor = MonitorServer.monitors.get(id);
        if (typeof monitor !== "undefined") {
            monitor._socket.close();
        }
    }

    /**
     * If there are no admin websocket left, suspend the game.
     * Called 10 sec after the last admin websocket left, to avoid suspending just upon refresh
     */
    public static suspendGameAfterTimeout(): void {
        if (MonitorServer.monitorAdminCount <=0) {
            NetGame.getNetGame().gameRunning = false;
        }
    }
}

/** Keep track of the websockets id */
let monitorWsId: number = 0;

MonitorServer.server.on('connection', (ws, req) => {
    const id = monitorWsId++;
    logger.info("New monitor (now " + MonitorServer.server.clients.size + " active)");
    const monitor = new Monitor(ws);
    MonitorServer.monitors.set(id, monitor);
    monitor.id = id;
    monitor.ipAddress = req.socket.remoteAddress;
    const arg = req.url.split(`?`)[1];
    monitor.role = arg.split('&ip=')[0];
    if (monitor.role === "gameMonitor") {
        MonitorServer.monitorGameCount++;
    } else {
        MonitorServer.monitorAdminCount++;
    }
    // On connection send the config and connectivity matrix
    ws.send(JSON.stringify(new MonitorInit()));
    ws.send(JSON.stringify(new ConnectivityUpdate()));

    ws.onclose = function (/*event: WebSocket.CloseEvent*/) {
        logger.info("monitor has gone (" + MonitorServer.server.clients.size + " remain)");
        MonitorServer.monitors.delete(id);
        if (monitor.role === "gameMonitor") {
            MonitorServer.monitorGameCount--;
        } else {
            MonitorServer.monitorAdminCount--;
            // suspend the game if there are no more admin websocket
            if (MonitorServer.monitorAdminCount === 0) {
                setTimeout(MonitorServer.suspendGameAfterTimeout, 10000);
            }
        }
    };

    ws.onerror = (event: WebSocket.ErrorEvent) => logger.error(event.message);
});
