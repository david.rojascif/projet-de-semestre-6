import WebSocket from "ws";

export class Monitor {
    /** the websocket for this monitor, will not change  */
    private readonly _socket: WebSocket;

    /** the role for this monitor */
    public role: string;

    /** the role of this monitor */
    public ipAddress: string;

    /** the id of the monitor */
    public id: number = -1;

    public constructor(socket: WebSocket) {
        this._socket = socket;
    }

    public get socket(): WebSocket {
        return this._socket;
    }

    public hasValidSocket() : boolean {
        return this._socket != null && this._socket.readyState < 2;
    }
}