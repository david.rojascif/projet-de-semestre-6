import { default as WebSocket, default as ws } from 'ws';
import { logger } from '../log';
import { PlayerController } from '../controllers/PlayerController';
import { Player } from '../netgame/network/Player';
import { ClearRegister } from './messages/player/From/ClearRegister';
import { InitConfig } from './messages/player/InitConfig';
import { WsMessage, WsParam } from './messages/WsMessage';
import { MonitorServer } from "./MonitorServer";
import { AliasReset } from "./messages/monitor/AliasReset";

//SCH import player from "../routes/player";

/**
 * WebSocket server for players.
 */
export class PlayerServer {
    /** The websocket server used to handle the /websocket/playing route */
    public static readonly server = new ws.Server({noServer: true});

    /** set containing every connected player */
    public static players = new Map();

    /**
     * broadcast a message to all the players connected to the server
     * @param msg : message to send to the players
     */
    public static broadcast(msg: WsMessage<WsParam>): void {
        PlayerServer.server.clients.forEach(client => client.send(JSON.stringify(msg), err => {
            if (err) logger.error(err)
        }));
    }

    /**
     * unicast a message to a specific playerid
     * @param id
     * @param msg
     */
    public static unicast(id : number, msg: WsMessage<WsParam>): void{
        const player = PlayerServer.players.get(id);
        if (typeof player !== "undefined") {
            player.sendMessage(msg);
        }
    }

    /**
     * Close the WS connection of a specific playerid
     * @param id
     */
    public static closeWS(id : number): void {
        const player = PlayerServer.players.get(id);
        if (typeof player !== "undefined") {
            player._socket.close();
            if (player.node>=0 && player.layer>=0) {
                MonitorServer.broadcast(new AliasReset(player.node, player.layer));
            }
        }
    }
}


/** Keep track of the websockets id */
let playerWsId: number = 0;

PlayerServer.server.on('connection', (ws, req) => {
    const id = playerWsId++;
    logger.info("New player at " + req.socket.remoteAddress + " (id: " + playerWsId + " / " + PlayerServer.server.clients.size + " active)");

    // Create a new player for each connected socket
    const player = new Player(ws);
    PlayerServer.players.set(id, player);
    player.ipAddress = req.socket.remoteAddress;
    player.id = id;

    // Send the initial config
    const init = new InitConfig();
    init.param.wsID = id;
    ws.send(JSON.stringify(init));

    ws.onmessage = function (event: WebSocket.MessageEvent) {
        logger.debug(event.data);
        // Dispatch the incoming message
        const message = JSON.parse(event.data.toString()) as WsMessage<WsParam>;
        PlayerController.dispatch(player, message);
    };

    ws.onclose = function (event: WebSocket.CloseEvent) {
        logger.info("Removing player (" + PlayerServer.server.clients.size + " remain)");
        logger.debug(event.reason);
        PlayerServer.players.delete(id);

        // Clear registration for the disconnected player
// TODO SCH: dispatcher does not return CRG error message anyway
//        ws.send(JSON.stringify(Dispatcher.dispatch(player, new ClearRegister())));
        PlayerController.dispatch(player, new ClearRegister());
    };
    ws.onerror = (event: WebSocket.ErrorEvent) => logger.error(event.message);
});
