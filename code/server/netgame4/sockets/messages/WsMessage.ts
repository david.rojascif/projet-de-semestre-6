/**
 * Base class to send a message to a websocket
 * Every message should extends this class
 * @template T : Type of the parameters, if no parameters should be set to undefined
 */
export abstract class WsMessage<T extends WsParam>{
    /** Type of the message, should always be the name of the subclass */
    public readonly type: string;

    /** Parameters of the message, should be set to an instance of T */
    public readonly param: T;

    /** Error code of the message, if no error the field should not be set */
    public readonly error: string;

    /**
     * Creates a new WsMessage with the given parameters
     * @param type : type of the message we want to create
     * @param param : parameters corresponding to the given type
     * @param error : optional, some messages have this error field
     */
    constructor(type: string, param: T, error?: string) {
        this.type = type;
        this.param = param;
        this.error = error;
    }
}

/**
 * Base class of the WsMessage paramters, this class is used for typecasting only
 */
export abstract class WsParam {
}