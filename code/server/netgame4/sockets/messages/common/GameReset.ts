import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the game is reseted
 * @template T : GameResetParams contains the necessary data to reset the clients
 */
export class GameReset extends WsMessage<GameResetParams>{
    public constructor() {
        super(GameReset.name, new GameResetParams());
    }
}

/**
 * Parameters to send when a GameReset messsage is sent
 */
export class GameResetParams extends WsParam {
    /** The reason why the game has been resetted, should be admin or init */
    reason: string;

    /** Connectivity matrix for the monitors */
    connectivity: boolean[][];
}