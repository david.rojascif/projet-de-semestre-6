import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the game state or registration state is changed
 * Game : Running -> Stopped OR Stopped -> Running
 * Registration : Open -> Closed OR Closed -> Open
 * @template T: GameStateParams contains the necessary data to send the gamestate to the clients
 */
export class GameState extends WsMessage<GameStateParams>{
    public constructor() {
        super(GameState.name, new GameStateParams());
    }
}

/**
 * Parameters to send to the websocket when a GameState is sent
 */
export class GameStateParams extends WsParam {
    /** true if netgame is running */
    readonly gameState: boolean = NetGame.getNetGame().gameRunning;

    /** true if registration is opened */
    readonly regState: boolean = NetGame.getNetGame().isRegistrationOpen;
}