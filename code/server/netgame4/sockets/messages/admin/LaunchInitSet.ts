import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the admin wants to change the set the init set
 * @template T : LaunchInitSetParams contains the new init set selected
 */
export class LaunchInitSet extends WsMessage<LaunchInitSetParam>{
    public constructor() {
        super(LaunchInitSet.name, new LaunchInitSetParam());
    }
}

/**
 * Parameter to send to the websocket when a LaunchInitSet message is sent
 */
export class LaunchInitSetParam extends WsParam {

    /** The init set to launch */
    initSet: number;
}