import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the admin wants to change the state of the registration
 * @template T : ChangeRegStateParam contains the new state of the registration
 */
export class ChangeRegState extends WsMessage<ChangeRegStateParam>{
    public constructor() {
        super(ChangeRegState.name, new ChangeRegStateParam());
    }
}

/**
 * Parameter to send to the websocket when a ChangeRegState message is sent
 */
export class ChangeRegStateParam extends WsParam {

    /** true if registration is open */
    public readonly regOpen: boolean = NetGame.getNetGame().isRegistrationOpen;
}