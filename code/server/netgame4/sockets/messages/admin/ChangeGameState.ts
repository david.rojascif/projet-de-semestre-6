import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the admin wants to change the state of the game
 * @template T : ChangeGameStateParam contains the new state of the game
 */
export class ChangeGameState extends WsMessage<ChangeGameStateParam>{
    public constructor() {
        super(ChangeGameState.name, new ChangeGameStateParam());
    }
}

/**
 * Parameter to send to the websocket when a ChangeGameState message is sent
 */
export class ChangeGameStateParam extends WsParam {

    /** true if netgame is running */
    public readonly gameRunning: boolean = NetGame.getNetGame().gameRunning;
}