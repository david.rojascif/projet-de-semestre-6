import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the admin wants to change the set the init set
 * @template T : LaunchInitSetParams contains the new init set selected
 */
export class AgentsTrigger extends WsMessage<AgentsTriggerParams>{
    public constructor() {
        super(AgentsTrigger.name, new AgentsTriggerParams());
    }
}

/**
 * Parameters to send to the websocket when a AgentsTrigger message is sent
 */
export class AgentsTriggerParams extends WsParam {

    /** Determines if the repeat trigger is active */
    repeated : boolean;
    
    /** The trigger time selected */
    time: number;
}