import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the admin wants to reset the registration
 * @template T : RegResetParams contains the information about the reset of the registration
 */
export class RegReset extends WsMessage<RegResetParams>{
    public constructor() {
        super(RegReset.name, new RegResetParams());
    }
}

/**
 * Parameter to send to the websocket when a RegReset message is sent
 */
export class RegResetParams extends WsParam {

    /** 
     * Determines if the reset is for all or only for a specific player
     * True if the reset is for all
     */
    all : boolean;

    /** The specific node to be reseted */
    node: number;

    /** The specific layer to be reseted */
    layer: number;
}