import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message sent when the admin wants to process one or more messages
 * @template T : AutoProcessMsgParams contains the information about the treated messsages
 */
export class AutoProcessMsg extends WsMessage<AutoProcessMsgParams>{
    public constructor() {
        super(AutoProcessMsg.name, new AutoProcessMsgParams());
    }
}

/**
 * Parameter to send to the websocket when a ChangeGameState message is sent
 */
export class AutoProcessMsgParams extends WsParam {

    /** List of messages to be processed */
    msgID: number[];

    //Rescue function, TODO 
    /** The id of the node to whom half of the messages should be treated */
    nodeID: number;

    /** The id of the layer to whom half of the messages should be treated  */
    LayerID: number;

}