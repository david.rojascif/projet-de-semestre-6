import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send to signal a player alias have been reseted by admin
 * @template T : AliasRestParam contains the information to identify the player
 */
export class AliasReset extends WsMessage<AliasResetParam>{
    public constructor(nodeId: number, layerId: number) {
        super(AliasReset.name, new AliasResetParam(nodeId, layerId));
    }
}

/**
 * Parameters to send when an AlisasRest messsage is sent
 */
export class AliasResetParam extends WsParam {
    /**
     * Create a new AliasResetParam for the given node and layer
     * @param nodeId : node in which we reset the alias
     * @param layerId : layer in the node that is reseted
     */
    constructor(nodeId: number, layerId: number) {
        super();
        this.nodeID = nodeId;
        this.layerID = layerId;
    }

    /** Node of the player whose alias has been reseted */
    public readonly nodeID: number;

    /** Layer of the player in the node */
    public readonly layerID: number;
}