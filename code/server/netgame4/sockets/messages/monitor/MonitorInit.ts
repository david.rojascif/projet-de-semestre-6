import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when a new monitor connects to the game
 * @template T : MonitorInitParams contains the information to display the game
 */
export class MonitorInit extends WsMessage<MonitorInitParams> {
    /**
     * Create a new MonitorInit with the parameter already filled
     */
    public constructor() {
        super(MonitorInit.name, new MonitorInitParams());
    }
}

/**
 * Parameters to send when an MonitorInit messsage is sent
 */
export class MonitorInitParams extends WsParam {
    /** true if netgame is running */
    public readonly gameState: boolean = NetGame.getNetGame().gameRunning;

    /** true if registration is opened */
    public readonly regState: boolean = NetGame.getNetGame().isRegistrationOpen;

    /** the number of messages queud for each node & layer  [nodeId][layerId] */
    public readonly queuedMsgCounters: number[][] = NetGame.getNetGame().queuedMessagesCounter;

    /** the number of buffered messages in PHL for each node  [nodeId]*/
    public readonly bufferedMsgCounters: number[] = NetGame.getNetGame().bufferedMessagesCounter;

    /** the aliases for each player in each node [nodeId][layerId] */
    public readonly aliases: string[][] = NetGame.getNetGame().aliases;
}