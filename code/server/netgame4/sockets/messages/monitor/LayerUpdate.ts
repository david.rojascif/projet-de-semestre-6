import { Layers } from "../../../netgame/enums/Layers";
import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when a layer has been updated
 * @template T : LayerUpdateParam contains the new information about the layer
 */
export class LayerUpdate extends WsMessage<LayerUpdateParam>{
    /**
     * Creates a message to update the player for given node and layer
     * @param nodeId : node of the player
     * @param layerId : layer of the player
     */
    public constructor(nodeId: number, layerId: number) {
        super(LayerUpdate.name, new LayerUpdateParam(nodeId, layerId));
    }
}

/**
 *  Parameters to send when an LayerUpdate messsage is sent
 */
export class LayerUpdateParam extends WsParam {
    /**
     * Creates a message to update the player for given node and layer
     * @param nodeId : node of the player
     * @param layerId : layer of the player
     */
    constructor(nodeId: number, layerId: number) {
        super();
        this.nodeID = nodeId;
        this.layerID = layerId;
        this.queuedMsgCnt = NetGame.getNetGame().queuedMessagesCounter[this.nodeID][this.layerID];

        if (layerId == Layers.DLL) {
            this.bufferedMsgCnt = NetGame.getNetGame().bufferedMessagesCounter[this.nodeID]
        }
    }

    /** Id of the node that has been updated */
    public readonly nodeID: number;

    /** Id of the layer in the [nodeID] that has been updated  */
    public readonly layerID: number;

    /** new number of queued messages for this layer */
    public readonly queuedMsgCnt: number;

    /** if DLL, the number of buffered messages for this node */
    public readonly bufferedMsgCnt: number;
}