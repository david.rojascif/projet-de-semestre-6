import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send to signal a player alias have been updated by admin
 * @template T : AliasUpdateParam contains the information to identify the player
 */
export class AliasUpdate extends WsMessage<AliasUpdateParam>{
    /**
     * Creates a new AliasUpdate with the given parameters
     * @param nodeId : Id of the node of the player
     * @param layerId : Id of the layer of the player
     * @param alias : New Alias for this player
     */
    public constructor(nodeId: number, layerId: number, alias: string) {
        super(AliasUpdate.name, new AliasUpdateParam(nodeId, layerId, alias));
    }
}

/**
 *  Parameters to send when an AliasUpdate messsage is sent
 */
export class AliasUpdateParam extends WsParam {
    /** 
     * Creates a new AliasUpdateParam with the given parameters
     * @param nodeId : Id of the node of the player
     * @param layerId : Id of the layer of the player
     * @param alias : New Alias for this player
     */
    constructor(nodeId: number, layerId: number, alias: string) {
        super();
        this.nodeID = nodeId;
        this.layerID = layerId;
        this.alias = alias;
    }

    /** Node of the player whose alias has been updated */
    public readonly nodeID: number;

    /** Layer of the player in the node */
    public readonly layerID: number;

    /** new alias of the player */
    public readonly alias: string;
}
