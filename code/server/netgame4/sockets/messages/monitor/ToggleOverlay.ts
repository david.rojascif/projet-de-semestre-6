import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send to toggle the overlay of the monitors
 * @template T : ToggleOverlayParams contains the information to display the overlay
 */
export class ToggleOverlay extends WsMessage<ToggleOverlayParams>{
    public constructor(param: ToggleOverlayParams) {
        super(ToggleOverlay.name, param);
    }
}

/**
 * Message to send to hide the overlay of the monitors
 * @template T : undefined, this Message doesn't contains any additional data
 */
export class HideOverlay extends WsMessage<ToggleOverlayParams>{
    public constructor() {
        super(ToggleOverlay.name, new ToggleOverlayParams());
    }
}

/**
 * Parameters to send when an ToggleOverlayParams messsage is sent
 * To hide overlay every field must be set to null
 */
export class ToggleOverlayParams extends WsParam {
    /** Name of the overlay to toggle ranking / routing */
    public readonly overlay: string;

    /** contains the traffic stats for each player [nodeId][layerId] */
    trafficStats: number[][];

    /** contains the destined stat for each node [nodeId] */
    destinedStats: number[];

    /** contains the arrived stat for each node [nodeId] */
    arrivedStats: number[];

    /** contains the sent stat for each node [nodeId] */
    sentStats: number[];

    /** contains the sent and arrived stats for each node [nodeId] */
    sentAndArrivedStats: number[];

    public constructor(type?: string) {
        super();
        this.overlay = type;
    }
}