import { WsMessage } from "../WsMessage";

/**
 * Message to send to signal all the player aliases have been reseted
 * @template T : undefined, this Message doesn't send any additional data
 */
export class AliasAllReset extends WsMessage<undefined>{
    public constructor() {
        super(AliasAllReset.name, undefined);
    }
}
