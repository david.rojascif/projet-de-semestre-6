import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send to signal the connectivity of the nodes has been updated
 * @template T : ConnectivityUpdateParams contains the information about the new connectivity
 */
export class ConnectivityUpdate extends WsMessage<ConnectivityUpdateParams> {
    public constructor() {
        super(ConnectivityUpdate.name, new ConnectivityUpdateParams());
    }
}

/**
 * Parameters to send when an ConnectivityUpdate messsage is sent
 */
export class ConnectivityUpdateParams extends WsParam {
    /** The new connectivityMatrix of the game */
    public readonly connectivity: boolean[][] = NetGame.getNetGame().connectivityMatrix;
}