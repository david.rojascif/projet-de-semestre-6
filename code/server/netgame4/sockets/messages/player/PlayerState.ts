import { Layers } from "../../../netgame/enums/Layers";
import { Message } from "../../../netgame/messages/Message";
import { NetGame } from "../../../netgame/Netgame";
import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the state of the player has been updated
 * @template T : PlayerStateParam contains the state of the player
 */
export class PlayerState extends WsMessage<PlayerStateParam>{

    /**
     * Creates a message to update the player for given node and layer
     * @param node : node of the player
     * @param layer : layer of the player
     */
    public constructor(node: number, layer: number, score: number) {
        super(PlayerState.name, new PlayerStateParam(node, layer, score));
    }
}

/**
 * Parameters to send when a Notification messsage is sent
 */
export class PlayerStateParam extends WsParam {

    /**
     * Create new parameter with the right message queue and buffer
     * @param node node of the player we want to update
     * @param layer layer of the palyer we want to update
     */
    public constructor(node: number, layer: number, score: number) {
        super();
        this.msgQueue = NetGame.getNetGame().Nodes[node].Layers[layer].queue.map(x => x.message);
        this.score = score;

        // Add buffer, if this is for DLL layer
        if (layer == Layers.DLL) {
            this.bufferQueue = NetGame.getNetGame().Nodes[node].Layers[Layers.DLL].buffered.map(x => x.message);
        }
    }

    /** Contains all the messages in the queue for this player */
    public readonly msgQueue: Message[];

    /** Contains all the buffered messages for the DLL  */
    public readonly bufferQueue: Message[];

    /** Current score for the player */
    public readonly score: number;
}