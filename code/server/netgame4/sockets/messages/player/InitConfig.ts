import { NetGame } from "../../../netgame/Netgame";
import { NetGameInit } from "../../../netgame/NetgameInit";
import { WsMessage, WsParam } from "../WsMessage";


/**
 * Message to send when the player wants to register to the game
 * @template T : InitParams contains the necessary data display the infos to register a player
 */
export class InitConfig extends WsMessage<InitConfigParams>{
    public constructor() {
        super(InitConfig.name, new InitConfigParams());
    }
}

/**
 * Parameters to send when a InitConfig messsage is sent
 */
export class InitConfigParams extends WsParam {

    /** Id of the websocket for the current player */
    wsID: number;

    /** true if registration is opened */
    public readonly gameState: boolean = NetGame.getNetGame().gameRunning;

    /** true if registration is opened */
    public readonly regState: boolean = NetGame.getNetGame().isRegistrationOpen;

    /** Names of the nodes */
    public readonly nodes: string[] = NetGameInit.NODE_NAMES;

    /** Names of the layers */
    public readonly layers: string[] = NetGameInit.LAYER_NAMES.slice(0, 3);

}