import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the give the registration state to a player
 * @template T : Parameters contains the state of the registration
 */
export class RegisterAllow extends WsMessage<RegisterAllowParam>{
    public constructor() {
        super(RegisterAllow.name, new RegisterAllowParam());
    }
}

/**
 * Parameters to send when a RegisterAllow messsage is sent
 */
export class RegisterAllowParam extends WsParam {
    /** true if registration is opened */
    regState: boolean;
}


