import { WsMessage } from "../WsMessage";

/**
 * Message to send to invite the AL to send a new message
 * @template T : undefined, this Message doesn't send any additional data
 */
export class LaunchMessage extends WsMessage<undefined>{
    public constructor() {
        super(LaunchMessage.name, undefined)
    }
}
