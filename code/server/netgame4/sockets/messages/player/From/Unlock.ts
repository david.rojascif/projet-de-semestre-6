import { WsMessage } from "../../WsMessage";

/**
 * Message received when a player want to unlock the current locked message
 * @template T : undefined, this Message doesn't contains any additional data
 */
export class Unlock extends WsMessage<undefined>{
    public constructor() {
        super(Unlock.name, undefined);
    }
}