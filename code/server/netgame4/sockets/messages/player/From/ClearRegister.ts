import { WsMessage } from "../../WsMessage";

export enum CRGCodes {
    CRG01 = "CRG01",
    CRG02 = "CRG02",
    SUCC = "SUCC"
}

/**
 * Message received when a player registration has been cleared by admin
 * @template T : undefined, this Message doesn't contains any additional data
 */
export class ClearRegister extends WsMessage<undefined>{
    public constructor() {
        super(ClearRegister.name, undefined);
    }
}