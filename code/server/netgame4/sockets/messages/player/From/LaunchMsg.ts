import { WsMessage, WsParam } from "../../WsMessage";

/**
 * Message received when a player in NL launches a new message
 * @template T : LaunchMsgParam contains the information about the message
 */
export class LaunchMsg extends WsMessage<LaunchMsgParam>{
    public constructor() {
        super(LaunchMsg.name, new LaunchMsgParam());
    }
}

/**
 * Parameters revceived with a LaunchMsg message
 */
export class LaunchMsgParam extends WsParam {
    /** The id of the node of the player that launched the message */
    fromNode: number;

    /** The id of the node the player wants to send a message to  */
    toNode: number;

    /** The text the player wants to send */
    msgTxt: string;
}