import { WsMessage, WsParam } from "../../WsMessage";

/**
 * Message received when a player want to lock a message fpr processing
 * @template T : LockParam contains the information to lock the desired message
 */
export class Lock extends WsMessage<LockParam>{
    public constructor() {
        super(Lock.name, new LockParam());
    }
}

/**
 * Parameters revceived with a Lock message
 */
export class LockParam extends WsParam {
    /** Id of the message the player wants to lock */
    MsgId: number;
}