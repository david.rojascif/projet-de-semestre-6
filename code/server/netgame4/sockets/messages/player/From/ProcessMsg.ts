import { WsMessage, WsParam } from "../../WsMessage";

/**
 * Message received when a player has processed a message
 * @template T : ProcessMsgParam contains the information about the treated messsage
 */
export class ProcessMsg extends WsMessage<ProcessMsgParam>{
    public constructor() {
        super(ProcessMsg.name, new ProcessMsgParam());
    }
}

/**
 * Parameters revceived with a ProcessMsg message
 */
export class ProcessMsgParam extends WsParam {
    /** The id of the message the player has processed */
    MsgId: number;

    /** The relevant value entered by player, if <0, automatic processing  */
    changedValue: number;

    /** The list of error codes (CSV) */
    errorCodes: string;      // send errorCodes as String with comma-separated codes

    /** The time the player has taken to process this message */
    processingTime: number;  //Math.floor((processingTime+0.5)/1000)
}