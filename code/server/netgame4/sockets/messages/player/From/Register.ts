import { WsMessage, WsParam } from "../../WsMessage";

/**
 * Message received when a player wants to register to the game
 * @template T : RegisterParam contains the information the player want to register with
 */
export class Register extends WsMessage<RegisterParam>{
    public constructor() {
        super(Register.name, new RegisterParam());
    }
}

/**
 * Parameters revceived with a Register message
 */
export class RegisterParam extends WsParam {
    /** The node the player wants to be in */
    Node: number;

    /** The layer the player wants to play in the node */
    Layer: number;

    /** The username the player wants to use */
    Alias: string;

    /** The ip address of the player */
    IP: string;

    /** true if the registration was forced */
    Forced: boolean;
}
