import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when the connectivity matrix has been updated
 * Allows the player to get the new connectivity for its node
 * @template T : ConnectivitParam contains the necessary data to update the connectivity of the player
 */
export class Connectivity extends WsMessage<ConnectivityUpdatePlayerParam>{
    public constructor() {
        super(Connectivity.name, new ConnectivityUpdatePlayerParam());
    }
}

/**
 * Parameters to send when a GameReset messsage is sent
 */
export class ConnectivityUpdatePlayerParam extends WsParam {
    /** the connection to each node if[nodeId] is true, the current node is connected to nodeId */
    connectivity: boolean[];
}

