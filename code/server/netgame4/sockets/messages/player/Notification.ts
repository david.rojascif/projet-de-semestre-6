import { WsMessage, WsParam } from "../WsMessage";

/**
 * Message to send when a notification should be displayed to a player
 * @template T : NotificationParam contains the necessary data to display the notification
 */
export class Notification extends WsMessage<NotificationParam>{
    public constructor() {
        super(Notification.name, new NotificationParam());
    }
}

/**
 * Parameters to send when a Notification messsage is sent
 */
export class NotificationParam extends WsParam {
    /** Message to display to the player */
    message: string;
}