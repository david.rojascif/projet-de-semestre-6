import { WsMessage, WsParam } from "../WsMessage";

export enum RegCodes {
    REG00 = "REG00",
    REG01 = "REG01",
    REG02 = "REG02",
    REG03 = "REG03",
    REG04 = "REG04",
    SUCC = "SUCC"    //Success
}

/**
 * Message to send when a player tries to register
 * @template T : Parameters contains the state of the registration for this player
 */
export class RegisterResult extends WsMessage<RegisterResultParam>{
    /**
     * Creates a new registerResult with the given code
     * @param code : the code we want to send back to the player
     */
    public constructor(code: RegCodes) {
        super(RegisterResult.name, new RegisterResultParam(code));
    }
}

/**
 * Parameters to send when a RegisterResult messsage is sent
 */
export class RegisterResultParam extends WsParam {

    /**
     * Constructor for a RegisterRestult, for a specific result code
     *  @param code: The result code for this message, success will be set based on this value
     */
    public constructor(code: RegCodes) {
        super();
        this.code = code;
        this.success = this.code == RegCodes.SUCC;
    }

    /** result code for this registration */
    public readonly code: RegCodes;

    /** true if the registration has been successful */
    public readonly success: boolean;

    /** NL only, contains the connectivity to other nodes */
    connectivity: boolean[];
}
