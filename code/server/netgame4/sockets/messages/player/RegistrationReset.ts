import { WsMessage } from "../WsMessage";

/**
 * Message to send to reset the registration of a player
 * To reset the registration of all player, this messages should be broadcasted
 * @template T : undefined, this Message doesn't send any additional data
 */
export class RegistrationReset extends WsMessage<undefined>{
    public constructor() {
        super(RegistrationReset.name, undefined);
    }
}
