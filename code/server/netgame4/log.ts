import {Logger} from "tslog";
import dayjs from "dayjs";
import * as fs from "fs";

// levels: 0: silly, 1: trace, 2: debug, 3: info, 4: warn, 5: error, 6: fatal
export const logger = new Logger({minLevel: 3});

logger.attachTransport((logObj) => {
    if (!fs.existsSync("./logs")) {
        fs.mkdirSync("./logs")
    }
    fs.appendFileSync("logs/log-" + dayjs().format("YY-MM-DD") + ".json", JSON.stringify(logObj, replacer) + "\n");
});


logger.info('Logger level : ' + logger.settings.minLevel);

function replacer(key : string, value : string) {
    if (key==="hostname"||key==="instanceName") {
        return undefined;
    }
    return value;
}