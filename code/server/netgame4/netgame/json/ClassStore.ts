import { NetGameMsg } from "../messages/NetGameMsg";
import { Message } from "../messages/Message";
import { MessageLog } from "../messages/MessageLog";
import { Node } from "../network/Node";
import { Layer } from "../network/Layer";
import { Player } from "../network/Player";
import { ConnectivityMatrix } from "../network/ConnectivityMatrix";

/**
 * Stores all classes that may be re-instantiated
 * 
 * IMPORTANT
 * - each class has to implement function annotateClass()
 * - each class has to provide a constructor without parameters (or just optional ones)
 * 
 */

/* eslint-disable @typescript-eslint/no-explicit-any */
export const ClassStore: any = {
    NetGameMsg,
    Message,
    MessageLog,
    Node,
    Layer,
    Player,
    ConnectivityMatrix
}
