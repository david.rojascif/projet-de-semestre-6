import { stringify, parse } from "./cycle";
import { logger } from "../../log";

import fs = require('fs');

export class Snapshot {

    // regular expression to recognize date strings
    public static reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
   
    /**
     * Writes game state to file
     * @param path where to save JSON file to
     * @param obj data to write
     * @returns true if there were no errors writing file
     */
    public static writeToFile(path: string, obj: any): boolean { // TODO define SnapShot object
        try {
            fs.writeFileSync(path, stringify(obj, null, 2));
        } catch (err) {
            logger.error(err)
            return false;
        }
        return true;
    }

    /**
     * JSON reviver function to convert data strings back to Date objects
     * @param _key property name
     * @param value property value
     * @returns original value or Date object
     */
    static dateParser = function (_key: string, value: any) { // TODO still needed ?
        if (typeof value === 'string') {
            const a = Snapshot.reISO.exec(value);
            if (a)
                return new Date(value);
        }
        return value;
    };

    /**
     * 
     * @param path where to read JSON file
     * @returns data structure read or error string on failure
     */
    public static readFromFile(path: string): any {
        let json: string;
        try {
            json = fs.readFileSync(path).toString();
        } catch (err) {
            logger.error(err)
            return err;
        }
        // use reviver function to convert data strings back to Date object
        return parse(json, Snapshot.dateParser); 
    }


}
