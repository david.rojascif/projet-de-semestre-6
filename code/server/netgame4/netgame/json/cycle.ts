/* eslint-disable @typescript-eslint/no-explicit-any */
import { ClassStore } from "./ClassStore";
//import { logger } from "../../log";

// Based on work of Douglas Crockford
//  https://github.com/jan-molak/JSON-js/blob/master/cycle.js

/**
 * @desc
 *  Converts a JavaScript value to a JavaScript Object Notation (JSON) string.
 *  Supports objects with cyclic references.
 *
 * @param {any} value
 *  A JavaScript value, usually an object or array, to be converted.
 *
 * @param {function(this: any, key: string, value: any): any} [replacer]
 *  A function that transforms the results.
 *
 * @param {string | number} [space]
 *  Adds indentation, white space, and line break characters to the return-value JSON text to make it easier to read.
 */
export function stringify(value: any, replacer?: (this: any, key: string, value: any) => any, space?: string | number): string {
    return JSON.stringify(decycle(value), replacer, space);
}

/**
 * @desc
 *  Converts a JavaScript Object Notation (JSON) string into an object.
 *  Supports objects with cyclic references.
 *
 * @param text A valid JSON string.
 * @param reviver A function that transforms the results. This function is called for each member of the object.
 *   If a member contains nested objects, the nested objects are transformed before the parent object is.
 */
export function parse(text: string, reviver?: (this: any, key: string, value: any) => any): any {
    return retrocycle(JSON.parse(text, reviver));
}

/**
 * @desc
 *  Makes a deep copy of an object or array, assuring that there is at most
 *  one instance of each object or array in the resulting structure. The
 *  duplicate references (which might be forming cycles) are replaced with
 *  an object of the form:
 *
 *  ```
 *      {"$ref": PATH}
 *  ```
 *
 *  where the PATH is a JSONPath string that locates the first occurrence.
 *
 *  So,
 *  ```
 *      var a = [];
 *      a[0] = a;
 *      return JSON.stringify(decycle(a));
 *  ```
 *
 *  produces the string `[{"$ref":"$"}]`.
 *
 *  JSONPath is used to locate the unique object. $ indicates the top level of
 *  the object or array. [NUMBER] or [STRING] indicates a child element or property.
 *
 *  Based on work by Douglas Crockford
 *   https://github.com/jan-molak/JSON-js/blob/master/cycle.js
 *
 * @param {any} object
 *
 * @package
 */
function decycle(object: any) {
    const objects = new WeakMap<any, string>();     // object to path mappings

    // The derez function recurses through the object, producing a deep copy
    return (function derez(value, path) {

        let old_path,   // The path of an earlier occurance of value
            nu: any;         // The new object or array

        if (
            typeof value === 'object'
            && value !== null
            && !(value instanceof Boolean)
            && !(value instanceof Date)
            && !(value instanceof Number)
            && !(value instanceof RegExp)
            && !(value instanceof String)
        ) {
            // If the value is an object or array, look to see if we have already
            // encountered it. If so, return a {"$ref":PATH} object.

            old_path = objects.get(value);
            if (old_path !== undefined) {
                return { $ref: old_path };
            }
            // Otherwise, accumulate the unique value and its path.

            objects.set(value, path);

            // If it is an array, replicate the array.

            if (Array.isArray(value)) {
                nu = [];
                value.forEach(function (element, i) {
                    nu[i] = derez(element, path + '[' + i + ']');
                });
            }

            // If it is an object, replicate the object.
            else {

                //--- added by SCH
                // call function annotateClass() if defined
                if (typeof value.annotateClass === 'function') 
                    value = value.annotateClass();

                nu = {};
                Object.keys(value).forEach(function (name) {
                    nu[name] = derez(
                        value[name],
                        path + '[' + JSON.stringify(name) + ']'
                    );
                });
            }
            return nu;
        }
        return value;
    }(object, '$'));
}

/**
 * @desc
 *  Restore an object that was reduced by decycle. Members which values are
 *  objects of the form
 *  ```
 *       {$ref: PATH}
 *  ```
 *  are replaced with references to the value found by the PATH.
 *  This will restore cycles. The object will be MUTATED.
 *
 *  The eval function is used to locate the values described by a PATH. The
 *  root object is kept in a $ variable. A regular expression is used to
 *  assure that the PATH is extremely well formed. The regexp contains nested
 *  * quantifiers. That has been known to have extremely bad performance
 *  problems on some browsers for very long strings. A PATH is expected to be
 *  reasonably short. A PATH is allowed to belong to a very restricted subset of
 *  Goessner's JSONPath.
 *
 *  So,
 *  ```
 *       var s = '[{"$ref":"$"}]';
 *       return retrocycle(JSON.parse(s));
 *  ```
 *  produces an array containing a single element which is the array itself.
 *
 *  Based on work by Douglas Crockford
 *   https://github.com/jan-molak/JSON-js/blob/master/cycle.js
 *
 * @param {any} $
 *
 * @package
 */
 function retrocycle($: any) {
    /* eslint-disable no-control-regex, no-useless-escape */
    const px = /^\$(?:\[(?:\d+|"(?:[^\\"\u0000-\u001f]|\\(?:[\\"\/bfnrt]|u[0-9a-zA-Z]{4}))*")\])*$/;

    (function rez(value /*, lvl */) {

        // The rez function walks recursively through the object looking for $ref
        // properties. When it finds one that has a value that is a path, then it
        // replaces the $ref object with a reference to the value that is found by
        // the path.

        if (value && typeof value === 'object') {
            if (Array.isArray(value)) {
                value.forEach(function (element, i) {
//                    console.log(" ".repeat(lvl) + "start array index : " + i);
                    if (typeof element === 'object' && element !== null) {
                        const path = element.$ref;
                        if (typeof path === 'string' && px.test(path)) {
                            value[i] = eval(path);      // tslint:disable-line:no-eval
//                            console.log(" ".repeat(lvl) + "dereferenced : ", value[i]);
                        } else {
                            //--- added by SCH
                            // if there's a class annotation, then cast object to this class
                            if (value[i].$class !== undefined) {
                                value[i] = castToClass(value[i] /*, lvl */);
                            }

                            rez(value[i] /*, lvl+1 */);
                        }
                    }
//                    console.log(" ".repeat(lvl) + "end array index : " + i);
                });
//                value.forEach(function (element, i) {
//                    console.log(" ".repeat(lvl) + "["+i+"] ", element);
//                });
            } else {
                Object.keys(value).forEach(function (name) {
                    const item = value[name];
//                    console.log(" ".repeat(lvl) + "start obj attr : " + name);
                    if (typeof item === 'object' && item !== null) {
                        const path = item.$ref;
                        if (typeof path === 'string' && px.test(path)) {
                            value[name] = eval(path);   // tslint:disable-line:no-eval
//                            console.log(" ".repeat(lvl) + "dereferenced : ", value(name));
                        } else {
                            //--- added by SCH
                            // if there's a class annotation, then cast object to this class
                            if (value[name].$class !== undefined) {
                                value[name] = castToClass(value[name] /*, lvl*/);
                            }
                            
                            rez(value[name] /*, lvl+1 */);
                        }
                    }
//                    console.log(" ".repeat(lvl) + "end obj attr : " + name);
                });

            }
        }
//        console.log("*** value after: ", value);
    }($ /*, 0 */));

    return $;
}

//--- added by SCH
function castToClass(value: any /*, lvl: number */) {
//    console.log(" ".repeat(lvl) + "found object of type '" + value.$class + "'", value);
    const newClass = new ClassStore[value.$class]();

    // does this class provide a createInstance() function ?
    if (typeof (newClass.createInstance) === 'function') {
        // thus use it to "cast" object to class instance
        value = newClass.createInstance(value);
//        console.log(" ".repeat(lvl) + "*** CREATE INSTANCE ***");
    } else {
        value = Object.assign(newClass, value); // copy object props to class instance
        delete value.$class; // remove class annotation
//        console.log(" ".repeat(lvl) + "-> replaced by : ", value);
    }
    return value;
}
