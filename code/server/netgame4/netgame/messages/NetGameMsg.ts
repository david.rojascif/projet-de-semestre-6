import { Layers } from "../enums/Layers";
import { Fields } from "./enums/Fields";
import { Status } from "./enums/Status";
import { Type } from "./enums/Type";
import { Message } from "./Message";
import { MsgInit } from "./MsgInit";
import { NetGameInit } from "../NetgameInit";
import { Request } from "express";
import dayjs from "dayjs";
import duration from 'dayjs/plugin/duration';
dayjs.extend(duration);

/** Used by NetGame to handle messages sent over game network
 *  Contains core message and additional control information.
 *  Provides convenience methods to handle game messages.
 */
export class NetGameMsg {

    /** Core message information */
    public readonly message: Message = new Message();

    // ---- additional control information

    /** number of fields in core message */
    public readonly maxFields = Fields.MAX

    /** Node the message is coming from (previous) */
    public fromNode: number = -1;

    /** layer that sent the message (previous) */
    public fromLayer: number = -1;

    /** Node the message is currently in (current) */
    public actNode: number = -1;

    /**layer the message is currently in (current) */
    public actLayer: number = -1;

    /** the next node for this message (next) */
    public toNode: number = -1;

    /** the next layer for this message (next) */
    public toLayer: number = -1;

    /** Age of the message since the start of the game (ms) */
    public timestamp: number = Date.now();

    /** Status of message */
    public status: number = Status.OK;

    /** auto treated by agent */
    public auto: boolean = false;

    /** ID of the related message,
     *  e.g: {ID:1, type=Data, msg=Data} {ID:2, type=ACK, related=1} */
    public related: number = -1;

    /**
     * NetGameMsg constructor, if a NetGameMsg is passed as param, it will be cloned
     * @param msg Optional, the message to clone
     */
    public constructor(msg?: NetGameMsg) {
        if (msg) {
            // Deep clone the message 
            this.message = new Message(msg.message);
            this.copy(msg);
        }
    }

    private copy(msg: NetGameMsg): void {
        // Clone all the fields
        this.fromNode = msg.fromNode;
        this.fromLayer = msg.fromLayer;
        this.actNode = msg.actNode;
        this.actLayer = msg.actLayer;
        this.toNode = msg.toNode;
        this.toLayer = msg.toLayer;
        this.related = msg.related;
        this.timestamp = Date.now(); //msg.timestamp;

        if (this.toLayer == 0) this.message.rawMsg = msg.message.rawMsg;
    }

    public annotateClass() : NetGameMsg {
        return Object.assign({}, this, { $class: "NetGameMsg" });
    }

    /**
     * Get age of this message (time since game started)
     * @returns time string in format hh:mm:ss
     */
    public getAgeString(): string {
        return dayjs.duration(Date.now() - this.timestamp).format('HH:mm:ss');
    }

    public getAgeInSeconds(): number {
        return Math.floor((Date.now() - this.timestamp) / 1000);
    }

    /** 
     * Checks if this message is of type DAT
     * @returns : returns true if this NetGameMsg is a DAT message 
     */
    public isDAT(): boolean {
        return this.message.msgFields[Fields.TYPE] == Type.DAT;
    }

    /** 
     * Checks if this message is of type ACK or NAK
     * @returns : returns true if this NetGameMsg is a ACK or NAK message 
     */
    public isACKorNAK(): boolean {
        return (this.message.msgFields[Fields.TYPE] == Type.ACK) || this.isNAK();
    }

    /** 
     * Checks if this message is of type NAK
     * @returns : returns true if this NetGameMsg is a NAK message 
     */
     public isNAK(): boolean {
        return this.message.msgFields[Fields.TYPE] == Type.NAK;
    }

    /** 
     * Checks if field FCS of this message is correct
     * @returns : true if the calculated FCS is the same as the msgFields FCS 
     */
    public checkFCS(): boolean {
        return this.calculateFCS() == this.message.msgFields[Fields.FCS];
    }

    /** 
     * Calculates FCS value for this message, counts all non-whitespace characters in msgString
     * @returns : correct FCS value  
     */
    public calculateFCS(): number {
        // use the \s quantifier to remove all white space
        return this.message.msgString.replace(/\s/g, "").length;
    }

    /**
     * Returns value of FCS in the rawMsg
     * @returns : the value of the FCS, if error -1
     */
    public retrieveFCSinRaw(): number {
        const last = this.message.rawMsg.split('/').pop();
        const parsed = parseInt(last, 10);
        return isNaN(parsed) ? -1 : parsed;
    }

    /**
     * Prepares this message to be used/returned as NAK
     */
    public prepareFieldsForNAK(): void {
        // swap src/dest
        const temp = this.message.msgFields[Fields.SRC];
        this.message.msgFields[Fields.SRC] = this.message.msgFields[Fields.DEST];
        this.message.msgFields[Fields.DEST] = temp;

        this.message.msgFields[Fields.TYPE] = Type.NAK;
        this.message.msgFields[Fields.FCS] = 0; // reset FCS

        for (let i = 4; i <= 8; i++) { this.message.msgFields[i] = -1; } // clear NL/AL fields
        this.message.msgString = null;
    }

    /**
     * Constructs the rawMsg for this message
     * The raw message will contain all msgFields, separated by slashes ('/')
     */
    public constructRawMsg(): void {
        const lastField = (this.message.msgFields[Fields.TYPE] != Type.DAT ? Fields.SEQNO + 1 : Fields.MAX);
        const all: string[] = [];

        for (let i = 0; i < lastField; i++) {
            // if message add escaped message
            if (i == Fields.MSG && this.message.msgString != null) {
                all.push(this.message.msgString.replace('/', '//'));
            }
            else {
                // Field is not set, we put error
                if (this.message.msgFields[i] < 0) {
                    all.push("ERROR");
                } 
                // if values are null put the value of the field
                else if (MsgInit.msgFieldValues[i] == null) {
                    all.push(this.message.msgFields[i].toString());
                } 
                // if values aren't null, add the value from list
                else {
                    all.push(MsgInit.msgFieldValues[i][this.message.msgFields[i]]);
                }
            }
        }

        // add FCS if ACK/NAK
        if (this.message.msgFields[Fields.TYPE] != Type.DAT) {
            all.push(this.message.msgFields[Fields.FCS].toString());
        }

        // Build message with / separation
        this.message.rawMsg = all.join('/');
    }

    /**
     * Sets all the DLL fields to -1 (SRC, DEST, TYPE, SEQNO, FCS)
     */
    public clearDLLFields(): void {
        for (let i = 0; i <= 3; i++) {
            this.message.msgFields[i] = -1;
        }
        this.message.msgFields[Fields.FCS] = -1;
    }

    /**
     * Changes the node / layer of this message, act -> from, to -> act, params -> to
     * @param node : is the next node (toNode)
     * @param layer : is the next layer (toLayer)
     */
    public shiftLocationInfo(node: number, layer: number): void {
        this.fromNode = this.actNode;
        this.fromLayer = this.actLayer;
        this.actNode = this.toNode;
        this.actLayer = this.toLayer;
        this.toNode = node;
        this.toLayer = layer;

        // update the direction of the message
        this.message.direction = (this.toLayer - this.actLayer);
    }

    /**
     * Inits location related information for this message (used when a new message is injected in the game)
     * @param node : fromNode
     * @param layer : fromLayer
     * @param node2 : actNode
     * @param layer2 : actLayer
     * @param node3 : toNode
     * @param layer3 : toLayer
     */
    public initLocationInfo(node: number, layer: Layers, node2: number, layer2: Layers, node3: number, layer3: Layers): void {
        this.fromNode = node;
        this.fromLayer = layer;
        this.actNode = node2;
        this.actLayer = layer2;
        this.toNode = node3;
        this.toLayer = layer3;

        // update the direction of the message
        this.message.direction = (this.toLayer - this.actLayer);
    }

    /**
     * Returns the status of this message in a string
     */
    public getStatus(): string {
        return Status[this.status];
    }

    /**
     * Tells if this message has arrived at its final destination
     * @returns true if current state is "arrived"
     */
    public hasArrived(): boolean {
        return this.status == Status.ARRIVED;
    }

    /**
     * Returns the location of this message in a string
     */
    public getLocation(): string {
        return "" + (this.toNode < 0 ? "?" : NetGameInit.NODE_NAMES[this.toNode]) + "/"
            + (this.toLayer < 0 ? "?" : NetGameInit.LAYER_SHORT_NAMES[this.toLayer]);
    }

    /**
     * Returns a HTML representation of a message field value
     * @param ndx index of field that should be returned
     */
    public getHTMLFieldValue(ndx: number): string {
        if (ndx == Fields.MSG)
            return (this.message.msgString == null ? "" : this.message.msgString);
        const val = this.message.msgFields[ndx];
        return (val < 0 ? "" : (MsgInit.msgFieldValues[ndx] == null ? val : MsgInit.msgFieldValues[ndx][val])).toString();
    }

    /**
     * Internal method to correctly retrieve integer parameters from a request
     * @param parm the parameter value as string
     * @returns an integer value (-1 for null, undefined, empty strings and non numeric values)
     */
    private static getIntParameterValue(parm: string) : number {
        if ((parm ?? "") == "") return -1; // null, undefined and "": -1
        const val: number = Number(parm);
        if (Number.isNaN(val)) return -1; // not a number
        return val;
     }

    /**
     * Updates this message with values from request parameters
     * @param req the request
     */
    public editValues(req : Request): void {
        this.fromNode = NetGameMsg.getIntParameterValue(req.body.fromNodeID);
        this.fromLayer = NetGameMsg.getIntParameterValue(req.body.fromLayerID);
        this.actNode = NetGameMsg.getIntParameterValue(req.body.actNodeID);
        this.actLayer = NetGameMsg.getIntParameterValue(req.body.actLayerID);
        this.toNode = NetGameMsg.getIntParameterValue(req.body.toNodeID);
        this.toLayer = NetGameMsg.getIntParameterValue(req.body.toLayerID);
        for (let i = 0; i < MsgInit.MAX_FIELDS; i++) {
            if (i == Fields.MSG) {
                this.message.msgString = req.body["msgfield"+i];
            }
            else if (req.body["msgfield"+i] != -1) {
                this.message.msgFields[i] = NetGameMsg.getIntParameterValue(req.body["msgfield"+i]);
            }
        }
        if (this.message.rawMsg) {
            this.constructRawMsg();
        }
    }
}