import { NetGameMsg } from "./NetGameMsg";

/**
 * Log of all instances of a message with a specific ID
 * Allows to get the history of changes made to the message
 */
export class MessageLog {
    
    /** ID of message instances in this log */
    public readonly ID: number;

    /** Log of instances */
    public msgLog: NetGameMsg[] = [];

    /**
     * Create a new MessageLog for the given message ID
     * @param msg : first message to put in newly created log
     */
    public constructor(msg?: NetGameMsg) {
        if (msg) {
            this.ID = msg.message.ID;
            this.msgLog.push(msg);
        }
    }

    public annotateClass() : MessageLog {
        return Object.assign({}, this, { $class: "MessageLog" });
    }        

    public getCurrent(): NetGameMsg {
        return this.msgLog[this.msgLog.length-1];
    }
}
