/** Status of this message, can be changed by admin */
export enum Status { 
    BANNED  = -2,
    ERROR   = -1,
    OK      =  0,
    ARRIVED =  1,
}