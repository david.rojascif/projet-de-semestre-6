/** Used to get the correct field in msgFields, number is the position in Message.msgFields*/
export enum Fields {
    SRC   = 0,  // SRC is at pos 0 
    DEST  = 1,  // DEST is at pos 1
    TYPE  = 2,  // TYPE is at pos 2 
    SEQNO = 3,  // SEQNO is at pos 3 
    NEXT  = 4,  // NEXT is at pos 4
    FINAL = 5,  // FINAL is at pos 5
    FROM  = 6,  // FROM is at pos 6
    TO    = 7,  // TO is at pos 7
    MSG   = 8,  // MSG is at pos 8 
    FCS   = 9,  // FCS is at pos 9 
    MAX   = 10  // maximum number of fields,
}
