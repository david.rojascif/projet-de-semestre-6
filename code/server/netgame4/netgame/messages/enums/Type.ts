/** Use to determine the type of the message */
export enum Type {
    INVALID = -1,
    DAT     =  0,
    ACK     =  1,
    NAK     =  2,
}
