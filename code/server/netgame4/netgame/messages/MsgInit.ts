import { NetGameInit } from "../NetgameInit";

/**
 * Holds constants related to netgame messages
 */
export class MsgInit {

    public static readonly MAX_FIELDS = 10;

    /** symbolic MAC addresses of each node  */
    public static readonly nodeAddrMAC: string[] = [
        "1111",
        "2222",
        "3333",
        "4444",
        "5555",
        "6666",
        "7777",
        "8888",
        "9999"
    ];

    /** symbolic IP addresses of each node */
    public static readonly nodeAddrIP: string[] = [
        "1.1.1.1",
        "2.2.2.2",
        "3.3.3.3",
        "4.4.4.4",
        "5.5.5.5",
        "6.6.6.6",
        "7.7.7.7",
        "8.8.8.8",
        "9.9.9.9"
    ];

    /** Type of the message */
    public static readonly msgTypes: string[] = [
        "DAT",
        "ACK",
        "NAK" // negative acknowledge
    ];

    /** Used for lookup when making the rawMsg,
     *  it tells where to get the correct value
     *  if null, just put the value in the rawMsg 
     */
    public static readonly msgFieldValues = [
        MsgInit.nodeAddrMAC,        // MAC addr. sender
        MsgInit.nodeAddrMAC,        // MAC addr. destination
        MsgInit.msgTypes,           // msg type
        null,                       // sequence no.
        MsgInit.nodeAddrIP,         // IP addr. next node
        MsgInit.nodeAddrIP,         // IP addr. final node
        NetGameInit.NODE_NAMES,     // source
        NetGameInit.NODE_NAMES,     // destination
        null,                       // message
        null
    ];

    // --- used for admin interface only ---

    // message status abbreviations
    public static readonly statusValues: string[] = [
        "BAN", // banned (desactivated)
        "ERR",
        "OK",
        "Arr" // arrived
    ];

    public static readonly fieldNames: string[] = [
        "Source",
        "Destination",
        "Type",
        "Seq.No.",
        "Next Node",
        "Final Node",
        "From",
        "To",
        "Message",
        "FCS"
    ];

    // field sizes for HTML tag <input>
    static msgFieldSizes: number[] = [
        0,
        0,
        0,
        3,
        0,
        0,
        0,
        0,
        30,
        2
    ];

    // help to determine fields to edit
    public static readonly EDIT_ALL  =  [true, true, true, true, true, true, true, true, true, true];
    public static readonly EDIT_APL  =  [false, false, false, false, false, false, true, true, true, false];
    public static readonly EDIT_NONE  =  [false, false, false, false, false, false, false, false, false, false];


}