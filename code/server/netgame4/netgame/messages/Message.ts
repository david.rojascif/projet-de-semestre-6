import { Fields } from "./enums/Fields";

/** Message that will be sent over the game network.
 *  Contains core message information and is main part of NetGameMsg.
 *  This core part only will be used when sent over websocket.
 */
export class Message {

    /** core message fields, transfered from server to client */
    public ID: number = -1;

    /** 0-3 : DLL Fields   (SRC, DEST, TYPE, SEQNO)
     * 4-8 : NL/AL Fields (NEXT, FINAL, FROM, TO, MSG)
     * 9-9 : FCS Field    (FCS)

     * 0 : Id (0 to 8) of the source in nodeAddrMAC, changes every times the message is sent
     * 1 : Id (0 to 8) of the destination in nodeAddrMAC, changes every times the message is sent
     * 2 : int (-1 to 2) -> converted to Type for current type
     * 3 : int -> sequence number 
     * 4 : Id (0 to 8) of the next node in nodeAddrIP
     * 5 : Id (0 to 8) of the final node in nodeAddrIP, never changes
     * 6 : Id (0 to 8) of the source node in NODE_NAMES, never changes
     * 7 : Id (0 to 8) of the destination node in NODE_NAMES, never changes
     * 8 : always -1, if we lookup for this, redirect to msgstring
     * 9 : Number of non space chars in msgString, calculated by DLL when coming from NL, then never changes
     */
    public msgFields: number[] = new Array<number>(Fields.MAX).fill(-1);

    /** if msgFields[2] == data : SRC DEST TYPE SEQNO 
     * else : SRC DEST TYPE SEQNO NEXT FINAL FROM TO MSG( / escaped) */
    public rawMsg: string = null;

    /** Content of the message */
    public msgString: string = null;

    /** Information about transfer between nodes/layers
     * -1: down, 0: horizontal (from node to node), 1: up
     */
    public direction: number = 0;

    /**
     * Message Contructor, if a Message is passed as param, it will be cloned
     * @param msg Optional, the message to clone
     */
    public constructor(msg?: Message) {
        if (msg) {
            this.msgFields = [...msg.msgFields];
            this.msgString = msg.msgString;
            this.ID = msg.ID;
            this.direction = msg.direction;
        }
    }

    public annotateClass() : Message {
        return Object.assign({}, this, { $class: "Message" });
    }        

}