import { Layers } from "../../netgame/enums/Layers";

export class RankingStats {
    /** The number of messages the player has processed */
    public msgCount: number = 0;

    /** The number of errors the player has commited */
    public errorCount: number = 0;

    /** The sum of the score for this player */
    public scoreSum: number = 0;

    /** The average score for this player */
    public scoreAvg: number = 0;

    /**
     * Updated the score for the player
     * @param layerID : the layer the player is in
     * @param errCnt : the number of errors the player has made
     * @param processingTime : the time the player has taken to process the message
     */
    public updateScore(layerID: number, errCnt: number, processingTime: number): number {
        this.msgCount++;
        this.errorCount += errCnt;

        let actScore = Math.max(40, 100 - (15 * errCnt)); // -15pts per error
        processingTime = Math.max(0, processingTime - 5); // start time penalty at 5s

        if (layerID != Layers.NL)
            processingTime /= 2; // reduce time penalty for DLL and PHL

        actScore = Math.max(10, actScore - processingTime); // -1pt per second
        this.scoreSum += Math.min(100, actScore); // limit to 100

        // calc sliding average (60% average + 40% new value)
        if (this.msgCount == 1)  // handle start situation
            this.scoreAvg = actScore;
        this.scoreAvg = ((6 * this.scoreAvg) + (4 * actScore)) / 10;
        return this.scoreAvg;
    }
}
