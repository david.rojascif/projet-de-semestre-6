import { NetGame } from "../Netgame";
import { NetGameMsg } from "../messages/NetGameMsg";
import { MsgInit } from "../messages/MsgInit";
import { MessageLog } from "../messages/MessageLog";
import { Fields } from "../messages/enums/Fields";
import { Layers } from "../enums/Layers";
import { Type } from "../messages/enums/Type";
import { logger } from '../../log';

enum STATE {
    DAT = 0, // inital message transmission
    ACKed = 1, // data message was acknowledged
    NAKed = 2, // data message was ngeatively acknowledged
    RETR = 3,  // ongoing message retransmission
    RETR_ACKed = 4 // retransmitted message is acknowledged
}

/**  */
export class MessageHistory {

    // remember last message ID and the its calculated path
    public static lastMsgID: number = -1;
    /* eslint-disable @typescript-eslint/ban-types */
    public static lastData: object = null;
    /* eslint-enable @typescript-eslint/ban-types */

    public static getMessageHistory(msgID: number) {

        this.lastMsgID = msgID; // remember msgID for history overlay

        const msgLogs: MessageLog[] = NetGame.getNetGame().messageLogs;
        const msgLog: NetGameMsg[] = msgLogs.find(log => log.ID == msgID).msgLog;
        if (msgLog.length <= 0)
            return null;

        const history: MessageStep[] = [];
        let currentNode = msgLog[0].actNode;
        const nodes: number[] = [currentNode];
        let ndx = 0;

        msgLog.forEach((msg, logIndex) => {
            const type = msg.message.msgFields[Fields.TYPE];
            const isRetr = ndx < history.length; // it's a retransmission if we're not at the end
            if (msg.actNode != currentNode && !isRetr) { // other node ?
                currentNode = msg.actNode;
                nodes.push(currentNode);
            }

            if (type == Type.DAT || msg.actLayer > Layers.DLL) { // treat as DAT if layer above DLL
                if (!isRetr) {
                    history[ndx++] = new MessageStep(STATE.DAT, msg.actNode, msg.actLayer, msg.toNode, msg.toLayer);
//                    logger.debug("added history["+(ndx-1)+"]: ", history[ndx-1]);
                    if (logIndex == msgLog.length-1 && msg.toNode != currentNode)
                        nodes.push(msg.toNode);
                } else {
                    history[ndx++].state = STATE.RETR; // NAKed -> RETR
                }
            }
            else if (type == Type.NAK) {
                history[--ndx].state = STATE.NAKed;
            }
            else if (type == Type.ACK) {
                if (!msg.hasArrived()) // ignore last instance if ACK has arrived
                    history[ndx++] = new MessageStep(STATE.DAT, msg.actNode, msg.actLayer, msg.toNode, msg.toLayer);
                if (logIndex == msgLog.length-1 && msg.toNode != currentNode && msg.toNode >= 0)
                    nodes.push(msg.toNode);
            }
            else {
                logger.error("unexpected message type : ", MsgInit.msgTypes[type]);
            }

            // go back and add acknowledge info to entries
            if (msg.isDAT() && msg.fromLayer == Layers.PHL && msg.actLayer == Layers.DLL && msg.related >= 0) { // handle returned ack
                const ackLog = msgLogs.find(msgLog => msgLog.ID == msg.related).msgLog;
                const backSteps = Math.min(ackLog.length,3);
                for (let i=0; i<backSteps; i++) {
                    history[ndx-i-2].state += 1; // DAT -> ACKed -OR- RETR -> RETR_ACKed
                }
            }
        });

        // prepare raw data format to be passed to view
        const raw: number[][] = [];
        history.forEach((h,i) => {
            raw[i] =  Object.values(h);
        });
        this.lastData = { data: raw, nodes: nodes }; // remember compiled history

        return this.lastData;
   }
}

class MessageStep {

    state: STATE;
    readonly actNode: number;
    readonly actLayer: number;
    readonly toNode: number;
    readonly toLayer: number;

    constructor(st: number, aN: number, aL: number, tN: number, tL: number) {
        this.state = st;
        this.actNode = aN;
        this.actLayer = aL;
        this.toNode = tN;
        this.toLayer = tL;
    }

}