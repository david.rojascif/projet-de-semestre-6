export class ErrorStats {
    /** the list of errors commited by the player */
    private _errors: ErrorCount[] = [];

    /**
     * Add the error in the list if it doesn't exist
     * increments the count for this error
     * @param name : name of the error we want to increment or create
     */
    public AddError(name: string): void {
        if (!this._errors.find(x => x.name == name)) {
            this._errors.push(new ErrorCount(name));
        }

        this._errors.find(x => x.name == name).count++;
    }

    public get Errors(): ErrorCount[] {
        return this._errors;
    }
}

export class ErrorCount {
    /** the name of the error, cannot be changed*/
    public readonly name: string;

    /** the number of times this error has been commites */
    public count: number;

    /**
     * Create a new error with the specified name and count of 0
     * @param name : name of the error
     */
    public constructor(name: string) {
        this.name = name;
        this.count = 0;
    }
}