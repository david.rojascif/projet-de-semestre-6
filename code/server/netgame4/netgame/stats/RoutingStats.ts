import { NetGameInit } from "../NetgameInit";
import { Fields } from "../../netgame/messages/enums/Fields";
import { NetGame } from "../Netgame";
import { NetGameMsg } from "../messages/NetGameMsg";
import { ToggleOverlayParams } from "../../sockets/messages/monitor/ToggleOverlay";

export class RoutingStats {
  
    public static fillRoutingStats(params: ToggleOverlayParams): void {
        params.trafficStats = [];
        for (let index = 0; index < NetGameInit.NODE_NAMES.length; index++) {
            params.trafficStats[index] = new Array(NetGameInit.NODE_NAMES.length).fill(0);
        }
        params.sentStats = new Array(NetGameInit.NODE_NAMES.length).fill(0);
        params.destinedStats = new Array(NetGameInit.NODE_NAMES.length).fill(0);
        params.arrivedStats = new Array(NetGameInit.NODE_NAMES.length).fill(0);
        params.sentAndArrivedStats = new Array(NetGameInit.NODE_NAMES.length).fill(0);

        let oldNode = -1;
        for (const logs of NetGame.getNetGame().messageLogs) {
            const msgLog = logs.msgLog;
            for (let i = 0; i < msgLog.length; i++) {
                const msg: NetGameMsg = msgLog[i];
                if (i == 0) {
                    if (!msg.isACKorNAK()) {
                        params.sentStats[msg.actNode]++; // record source and
                        params.destinedStats[msg.message.msgFields[Fields.TO]]++; // destination node
                    }
                } else if (i == msgLog.length - 1) { // last entry ?
                    // check if msg has reached its destination
                    if (!msg.isACKorNAK() && msg.hasArrived()) {
                        params.arrivedStats[msg.toNode]++;
                        params.sentAndArrivedStats[msg.message.msgFields[Fields.FROM]]++;
                    }
                } else {
                    if (msg.actNode != oldNode) {
                        params.trafficStats[oldNode][msg.actNode]++; // record traffic between nodes
                    }
                }
                oldNode = msg.actNode;
            }
        }
    }
  
}