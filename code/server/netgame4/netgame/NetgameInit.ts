/**
 * Constants used for NetGame initialisation
 */
export class NetGameInit {

    public static readonly NODE_NAMES: string[] = [
        "Charly",
        "Lucy",
        "Schroeder",
        "Linus",
        "Snoopy",
        "Woodstock",
        "Sally",
        "Franklin",
        "Marcie"
    ];

    public static readonly LAYER_NAMES: string[] = [
        "Physical Layer (PHL)",
        "Data Link Layer (DLL)",
        "Network Layer (NL)",
        "Application Layer (AL)"
    ];

    public static readonly LAYER_SHORT_NAMES: string[] = [
        "PHL",
        "DLL",
        "NL",
        "AL" // application layer
    ];

    //--- ROUTING

    /** initial connectivity matrix for 9 nodes/stations (defines connections between nodes) */ 
    public static readonly baseConnectivityMatrix: boolean[][] = [
        //  1      2     3      4      5     6      7      8      9
        [false, true, false, false, true, false, false, false, false], // 1
        [true, false, true, false, true, false, false, false, false],  // 2
        [false, true, false, false, false, true, false, false, false], // 3
        [false, false, false, false, true, false, true, true, false],  // 4
        [true, true, false, true, false, true, false, false, true],    // 5
        [false, false, true, false, true, false, false, false, true],  // 6
        [false, false, false, true, false, false, false, true, false], // 7
        [false, false, false, true, false, false, true, false, true],  // 8
        [false, false, false, false, true, true, false, true, false]   // 9
    ];

    /** defines two messages sets, each node sends and receives two messages */
    public static readonly firstSets :[number, number][][] = [
        [ 
            [ 0, 4 ], [ 0, 7 ], [ 1, 2 ], [ 1, 3 ], [ 2, 0 ], [ 2, 4 ], 
            [ 3, 1 ], [ 3, 0 ], [ 4, 2 ], [ 4, 3 ], [ 5, 6 ], [ 5, 8 ], 
            [ 6, 8 ], [ 6, 1 ], [ 7, 6 ], [ 7, 5 ], [ 8, 5 ], [ 8, 7 ] 
        ],
        [ 
            [ 0, 3 ], [ 0, 6 ], [ 1, 5 ], [ 1, 8 ], [ 2, 4 ], [ 2, 0 ],
            [ 3, 0 ], [ 3, 1 ], [ 4, 2 ], [ 4, 7 ], [ 5, 1 ], [ 5, 7 ], 
            [ 6, 4 ], [ 6, 8 ], [ 7, 5 ], [ 7, 6 ], [ 8, 2 ], [ 8, 3 ] 
        ] 
    ];

    public static readonly secondSets :[number, number] [][] = [
        [
            [ 0, 7 ], [ 0, 8 ], [ 1, 6 ], [ 1, 4 ], [ 2, 6 ], [ 2, 8 ],
            [ 3, 7 ], [ 3, 2 ], [ 4, 0 ], [ 4, 5 ], [ 5, 1 ], [ 5, 4 ],
            [ 6, 2 ], [ 6, 3 ], [ 7, 1 ], [ 7, 0 ], [ 8, 3 ], [ 8, 5 ]
        ],
        [
            [ 0, 2 ], [ 0, 5 ], [ 1, 3 ], [ 1, 7 ], [ 2, 4 ], [ 2, 8 ],
            [ 3, 1 ], [ 3, 2 ], [ 4, 7 ], [ 4, 8 ], [ 5, 1 ], [ 5, 6 ],
            [ 6, 4 ], [ 6, 0 ], [ 7, 0 ], [ 7, 5 ], [ 8, 3 ], [ 8, 6 ]
        ],
        [
            [ 0, 6 ], [ 0, 3 ], [ 1, 7 ], [ 1, 3 ], [ 2, 7 ], [ 2, 4 ],
            [ 3, 2 ], [ 3, 8 ], [ 4, 5 ], [ 4, 6 ], [ 5, 0 ], [ 5, 8 ],
            [ 6, 1 ], [ 6, 5 ], [ 7, 4 ], [ 7, 2 ], [ 8, 1 ], [ 8, 0 ]
        ]
    ];


    // Peanuts quotes used for the two message sets
    public static peanutsQuotes :string[][] = [
        [ 
        "Hi, howdo?", 
        "Playing the Red Baron?", 
        "Do you like Chopin?",
        "You lost your towel!", 
        "Just listen, please", 
        "Do you like it?",
        "Don't bother me", 
        "How about Lucy?", 
        "Yes, I like it", 
        "Woof?",
        ".. (hi)", 
        ".. ... (see you)", 
        "What's your name?", 
        "Get of my feet!",
        "Ever met Harry?", 
        "Howdy?", 
        "What's up, birdie?", 
        "Are you famous?" 
        ],
        [
        "The book of life has no answers in the back",
        "You like peanut butter?",
        "Take a note, please",
        "Happiness is a warm puppy",
        "Dance for me!",  
        "Hi looser",
        "I love mankind, it's people I can't stand",
        "Do they still make wooden Christmas Trees?", 
        "To live is to dance, to dance is to live",  
        "I hate good-byes",
        "... (shut up)", 
        ".... (howdy?)",
        "Stupid dog, you!", 
        "Linus is so cute",
        "What's up, birdie?", 
        "Franklin meets Sally",
        "Play it again, Sam!", 
        "Lost your towel?" 
        ]                   
    ];

    // General proverbs used for message sets
    public static readonly proverbs: string[] = [
        "When the cat's away, the mice will play",
        "Without justice, courage is weak",
        "To err is human, to forgive divine",
        "Two wrongs do not make a right",
        "Procrastination is the thief of time",
        "Never too late to learn",
        "No time like the present",
        "Necessity is the mother of invention",
        "Fish and guests smell after three days",
        "Bad news travels fast",
        "Absolute power corrupts absolutely",
        "Men never remember, but women never forget",
        "Great talkers are little doers",
        "Fools grow without watering",
        "Computers are thiefs of time",
        "Opinions differ",
        "Too many clicks spoil the browse",
        "A chat has nine lives",
        "Home is where you hang your @",
        "What boots up must come down",
        "Never assume the computer assumes anything",
        "1f y0u c4n r34d 7h15, y0u r34||y n33d 70 637 |41d",
        "don't loose your mind - do backup!",
        "Hey! It compiles! Ship it!",
        "Smash forehead on keyboard to continue",
        "I get mail, therefore I am",
        "Computer security - an oxymoron?",
        "Windows isn't a virus, viruses do something",
        "Programmers convert caffeine into code",
        "Never let a computer know you're in a hurry",
        "ASCII stupid question, get a stupid ANSI!",
        "Computers do everything on purpose",
        "Everything is easy, finding it out is hard",
        "A rumour goes in one ear and out many mouths",
        "The purpose of life is a life of purpose",
        "All wiyht. Rho sritched mg kegtops awound?",
        "Hit any user to continue...",
        "Upgrade: old bugs out, new ones in",
        "Knowledge comes, but wisdom lingers",
        "Where there is love there is life",
        "Cheer up, the worst is yet to come",
        "Common sense ain't common anymore",
        "A prudent question is one half of wisdom",
        "A witty saying proves nothing",
        "Black holes are where God divided by zero",
        "Failure is success if we learn from it",
        "A goal without a plan is just a wish",
        "Chess is mental torture",
        "Errare humanum est",
        "A joke is a very serious thing",
        "Brevity is the soul of wit",
        "Fortune favors the brave",
        "Advertising is legalized lying",
        "Love is the beauty of the soul",
        "In teaching others we teach ourselves",
        "Don't worry, be happy",
        "Mistakes are the portals of discovery",
        "They know enough who know how to learn",
        "Think twice, code once",
        "By viewing the old we learn the new",
        "Do you need space? Join NASA!",
        "When nothing goes right, go left.",
        "If you fail to prepare, then prepare to fail",
        "Curiosity killed the cat",
        "Do as I say, and not as I do",
        "Nothing is certain but death and taxes",
        "Nostalgia is a seductive liar",
        "Never steal. The government hates competition",
        "As I said before, I never repeat myself",
        "Some people are so poor, all they have is money",
        "Technology is teaching us to be human again",
        "Science is a cemetery of dead ideas",
        "Knowledge isn't free, you have to pay attention",
        "Lessons of life are free, but cost a lot"
/* following proverbs are too long : 
        "Everybody wishes they could go to heaven but no one wants to die.",
        "Doesn't expecting the unexpected make the unexpected expected?",
        "It is easier to ask for forgiveness than it is to ask for permission.",
        "My mind is like lighting, one brilliant flash, then its gone.",
        "Whenever I find the key to success, someone changes the lock.",
        "I'm not a complete idiot. Some pieces are missing",
        "Rlaely it deson't mttaer waht I wirte you'll sitll uanrtednsnd it",
        "The slowest traffic of the day is called 'rush hour'",
        "Who says nothing is impossible. I've been doing nothing for years...",
        "Of all the things I've lost, I miss my mind the most",
        "Video games ruined my life. At least I have 2 left.",
        "I just got lost in thought. It was unfamiliar territory.",
        "Why is there no egg in eggplant and no ham in hamburger?",
        "The device will work much better, if you turn it on." */
    ];

    static setSize : number = NetGameInit.firstSets[0].length;
    static shuffle : number[] = [];
    static setIndex : number = 0;
    static setIndex2 : number = 0;


    public static doShuffle(): void {
        let i: number;
        for(i = 0; i < NetGameInit.setSize; i++) {
            NetGameInit.shuffle[i] = i;
        }

        for(i = 0; i < NetGameInit.shuffle.length; ++i) {
            const randomPosition = Math.floor(Math.random()*NetGameInit.shuffle.length)
            const temp = NetGameInit.shuffle[i];
            NetGameInit.shuffle[i] = NetGameInit.shuffle[randomPosition];
            NetGameInit.shuffle[randomPosition] = temp;
        }

    }
}