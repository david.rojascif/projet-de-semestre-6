// NetGame core
import { NetGameInit } from "./NetgameInit";
import { NetGameMsg } from "./messages/NetGameMsg";
import { MessageLog } from "./messages/MessageLog";
import { Node } from "./network/Node";
import { Player } from "./network/Player";
import { ConnectivityMatrix } from "./network/ConnectivityMatrix";
import { AutoProcess } from "./network/processing/AutoProcess";
import { AutoProcessTimer } from "./network/processing/AutoProcessTimer";
import { RankingStats } from "./stats/RankingStats";
// sockets
import { MonitorServer } from "../sockets/MonitorServer";
import { PlayerServer } from "../sockets/PlayerServer";
// messages
import { GameState } from "../sockets/messages/common/GameState";
import { CRGCodes } from "../sockets/messages/player/From/ClearRegister";
import { ProcessMsgParam } from "../sockets/messages/player/From/ProcessMsg";
import { RegCodes } from '../sockets/messages/player/RegisterResult';
import { logger } from "../log";

/**
 * Main class of the game
 */
export class NetGame {
    
    /** holds singleton instance */
    private static netgame: NetGame;

    /** 
     * Get the Netgame instance, creates singleton instance if needed 
     */
    public static getNetGame(): NetGame {
        return NetGame.netgame = NetGame.netgame ?? new NetGame();
    }

    /** Nodes/Stations in the game */
    private _nodes: Node[] = [];

    /** true if registration is open */
    private _registrationOpen: boolean = false;

    /** true if game is currently running */
    private _gameRunning = true;

    /** Connectivity matrix of the game */
    private _connectivity: ConnectivityMatrix;

    /** Logs of the message since the start of the game */
    private _messageLogs: MessageLog[] = [];

    /** Informations about the autoprocessing of the messages  */
    private _autoProcess: AutoProcess;

    /** Timer used to automatically trigger processing of the messages */
    private _autoTimer: AutoProcessTimer;

    /** Timestamp (milliseconds) of game start */
    private _timeStart : number;

    /** Timestamp (milliseconds) when game was last suspended */
    private _timeStop : number;

    /**
     * Initialise the netgame with default values
     */
    private constructor() {
        logger.info("NetGame v4.1.0 (Typescript/EJS)");
        logger.info("Game Initialisation");
        this._timeStart = Date.now();
        this._connectivity = new ConnectivityMatrix();
        this._autoProcess = new AutoProcess();
        this._autoTimer = new AutoProcessTimer(45000);
        this.reset();
        NetGameInit.doShuffle();
    }

    /**
     * Resets the game to initial state
     */
    public reset(): void {
         // Reset values
        this._nodes = [];
        this._messageLogs = []
        this._timeStart = Date.now();
        this.AutoProcess.reset();
        this.ProcessTimer.stop();

        // new values
        NetGameInit.NODE_NAMES.forEach((_, idx) => this._nodes.push(new Node(idx)));
        logger.info("Game was reset");
    }

    /** returns true if the registration is open */
    get isRegistrationOpen(): boolean {
        return this._registrationOpen;
    }

    /** allows to change the registration state */
    set isRegistrationOpen(value: boolean) {
        this._registrationOpen = value;
        MonitorServer.broadcast(new GameState());
        PlayerServer.broadcast(new GameState());
    }

    /** Returns true if the game is running */
    get gameRunning(): boolean {
        return this._gameRunning;
    }

    /** Changes the state of the game and handles timestamps */
    set gameRunning(newRunning: boolean) {
        if (this._gameRunning == newRunning) return;
        if (this._gameRunning && !newRunning) { // RUN -> SUSPENDED
            // remember when game was suspended
            this._timeStop = Date.now();
        } else if (!this._gameRunning && newRunning) { // SUSPENDED -> RUN
            // add time while game was suspended
            const timeDelta = Date.now() - this._timeStop;
            this._timeStart += timeDelta;
            this.updateMessageTimestamps(timeDelta);
        }
        logger.silly("Game state changed to " + (newRunning ? "RUNNING" : "SUSPENDED"));
        this._gameRunning = newRunning;
        MonitorServer.broadcast(new GameState());
        PlayerServer.broadcast(new GameState());
    }

    /** gets the array of nodes (stations) */
    public get Nodes(): Node[] {
        return this._nodes;
    }

    /** sets the array of nodes (stations) */
    public set Nodes(nodes: Node[])  {
        this._nodes = nodes;
    }

    /** returns the connectivity matrix of the game */
    public get connectivityMatrix(): boolean[][] {
        return this._connectivity.connectivityMatrix;
    }

    /** returns the connectivity matrix of the game */
    public get connectivity(): ConnectivityMatrix {
        return this._connectivity;
    }

    /** sets the connectivityMatrix */
    public set connectivity(matrix : ConnectivityMatrix) {
        this._connectivity = matrix;
    }

    /** returns a 2D array of the aliases of the connected players */
    public get aliases(): string[][] {
        return this._nodes.map(n => n.aliases);
    }

    /** Get a 1D array containing the number of messages in buffers */
    public get bufferedMessagesCounter(): number[] {
        return this._nodes.map(n => n.bufferedCount);
    }

    /** Get a 2D array containing the number of messages in queues */
    public get queuedMessagesCounter(): number[][] {
        return this._nodes.map(n => n.queuedCount);
    }

    /** returns the message log for all the messages */
    public get messageLogs(): MessageLog[] {
        return this._messageLogs;
    }

    /** sets the message log for all the messages */
    public set messageLogs(logs: MessageLog[]) {
        this._messageLogs = logs;
    }

    /** returns the informations about autoprocessing */
    public get AutoProcess(): AutoProcess {
        return this._autoProcess;
    }

    /** returns the timer used to autoprocess messages */
    public get ProcessTimer(): AutoProcessTimer {
        return this._autoTimer;
    }

    /** Get all the ranking stats for every layer of every node */
    public get RankingStats(): RankingStats[][] {
        return this._nodes.map(n => n.RankingStats);
    }

    public get timeStart(): number {
        return this._timeStart;
    }

    public set timeStart(timestamp: number) {
        this._timeStart = timestamp;
    }

    //--- REGISTRATION

    /**
     * Register a player to a selected node / layer
     * @param nodeId : node selected by player
     * @param layerId : layer selected by player
     * @param force : if this player can register even if registration is closed
     * @param player : the player to register to the node / layer
     * @returns : code (string) to indicate success or error
     */
    public registerPlayer(nodeId: number, layerId: number, force: boolean, player: Player): RegCodes {
        // Netgame is currently suspended
        if (!this._gameRunning) {
            logger.warn("Game is currently suspended, no registrations allowed");
            return RegCodes.REG01;
        }

        // Registration for NetGame is currently disabled (overriden if force==true)
        if (!this._registrationOpen && !force) {
            logger.warn("Registration is closed, no registrations allowed");
            return RegCodes.REG02;
        }

        // Invalid parameter NodeId
        if (nodeId >= this._nodes.length) {
            logger.warn("Invalid nodeId : " + nodeId);
            return RegCodes.REG03;
        }

        return this._nodes[nodeId].register(player, layerId);
    }

    /**
     * removes the player from the node / layer
     * @param nodeId : the current node the player is registered to
     * @param layerId : the current layer the player is registered to
     * @returns : code (string) if there is an error
     */
    public clearPlayerRegistration(nodeId: number, layerId: number): CRGCodes {
        // Invalid parameter NodeId
        if (nodeId >= this._nodes.length) {
            logger.warn("Invalid nodeId : " + nodeId);
            return CRGCodes.CRG02;
        } 

        return this._nodes[nodeId].removePlayerFromLayer(layerId);
    }

    //--- Message processing
    /**
     * process the message for the player, with given parameters
     * @param sender : the player that has processed the message
     * @param param : the information to process the message
     */
    public processMessage(sender: Player, param: ProcessMsgParam) :void {
        this._nodes[sender.node].processMsg(sender, param);
    }

    /**
     * add a new message to the game, give an ID and add a new Log for this message
     * @param msg : the new message to add to the game
     */
    public addNewMsg(msg: NetGameMsg): void {
        msg.message.ID = this._messageLogs.length;
        this._messageLogs.push(new MessageLog(msg));
        logger.silly("Added new Message :", msg);
    }

    /**
     * Add a new message instance to the corespondant message log
     * @param msg : the message to add to the log
     */
    public addMsgToLog(msg: NetGameMsg): void {
        // TODO why not directly use array index?
        this._messageLogs.find(msgLog => msgLog.ID == msg.message.ID).msgLog.push(msg);
    }

    /**
     * Retrieve current message instance of a specific message from log
     * @param msgID the ID of the message to look for
     */
    public getCurrentMsgFromLog (msgID: number) : NetGameMsg {
        return this._messageLogs.find(msgLog => msgLog.ID == msgID).getCurrent();
    }

    /**
     * Updates timestamps in all messages
     * @param timeDelta 
     */
    public updateMessageTimestamps(timeDelta: number): void {
        this._messageLogs.forEach(log => {
            log.msgLog.forEach(msg => {
                msg.timestamp += timeDelta;
            });
        });
    }
}