import { logger } from "../../log";
import { Layers } from "../../netgame/enums/Layers";
import { RankingStats } from "../../netgame/stats/RankingStats";
import { CRGCodes } from "../../sockets/messages/player/From/ClearRegister";
import { ProcessMsgParam } from "../../sockets/messages/player/From/ProcessMsg";
import { RegCodes } from "../../sockets/messages/player/RegisterResult";
import { WsMessage, WsParam } from "../../sockets/messages/WsMessage";
import { NetGameInit } from "../NetgameInit";
import { Layer } from "./Layer";
import { Player } from "./Player";

export class Node {
    
    /** Layers for this node */
    public readonly Layers: Layer[] = [];

    /**
     * Create a new node with all the layers inside
     */
    public constructor(nodeId?: number) {
        if (nodeId !== undefined)
            NetGameInit.LAYER_NAMES.forEach((_, index) => this.Layers.push(new Layer(nodeId, index)));
    }

    public annotateClass() : Node {
        return Object.assign({}, this, { $class: "Node" });
    }        

    /** return the list of aliases for this node */
    public get aliases(): string[] {
        return this.Layers.map(l => l.player ? l.player.alias : null).slice(0,-1); // without AL
    }

    /** returns the number of messages in the buffer of this node */
    public get bufferedCount(): number {
        return this.Layers[Layers.DLL].buffered.length;
    }

    /** returns the number of message in each queues of the layers */
    public get queuedCount(): number[] {
        return this.Layers.map(l => l.queue.length).slice(0,-1); // without AL;
    }

    /** Returns all the ranking stats for this node */
    public get RankingStats(): RankingStats[] {
        return this.Layers.map(l => l.player ? l.player.ranking : new RankingStats());
    }

    /**
     * Will try to add the player in the layer fo this node
     * @param player : the player that wants to register
     * @param layerId : the layer the player wants to register in
     */
    public register(player: Player, layerId: number): RegCodes {
        // Invalid parameter LayerId
        if (layerId >= this.Layers.length) {
            logger.warn("Invalid layerId : " + layerId);
            return RegCodes.REG03;
        }

        if (this.Layers[layerId].player == null
            || this.Layers[layerId].player.socket == null) { // socket is null (loaded game state?)
            this.Layers[layerId].player = player;
            return RegCodes.SUCC;
        }

        // Node/Layer combination already assigned
        logger.warn("Node/Layer combination already assigned");
        return RegCodes.REG04;
    }

    /**
     * Will remove the player from the layer
     * @param layerId : the layer we want to remove the player from
     */
    public removePlayerFromLayer(layerId: number): CRGCodes {
        // Invalid paramter LayerId
        if (layerId >= this.Layers.length) {
            logger.error("Invalid layerId : " + layerId);
            return CRGCodes.CRG01;
        }

        if (this.Layers[layerId].player == null) {
            logger.error("no player registered to this node / layer combination");
            return CRGCodes.CRG02;
        }

        this.Layers[layerId].player.reset();
        this.Layers[layerId].player = null;
        return CRGCodes.SUCC;
    }

    /**
     * Will process the message for the sender
     * @param sender : the player that wants to process the message
     * @param msg : the informations to process the message
     */
    public processMsg(sender: Player, msg: ProcessMsgParam): void {
        this.Layers[sender.layer].processMsg(sender, msg);
    }

    
    /**
     * will send a message to all the players in this node
     * @param message : the messsage to broadcast
     */
    public broadcast(message: WsMessage<WsParam>): void {
        this.Layers.forEach(layer => layer.player?.sendMessage(message));
    }

    /**
     * Will send a message to the specific layer
     * @param message : the message to send
     * @param layerId : the layer to send the message to
     */
    public unicast(message: WsMessage<WsParam>, layerId: number): void {
        this.Layers[layerId].player?.sendMessage(message);
    }
}