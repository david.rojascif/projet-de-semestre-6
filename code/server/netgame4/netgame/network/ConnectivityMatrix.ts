import { logger } from "../../log";
import { NetGameInit } from "../NetgameInit";
import { MonitorServer } from "../../sockets/MonitorServer";
import { ConnectivityUpdate } from "../../sockets/messages/monitor/ConnectivityUpdate";
import { Connectivity } from "../../sockets/messages/player/Connectivity";
import { NetGame } from "../Netgame";


export class ConnectivityMatrix {

    /** the connectivity matrix */
    private readonly _connectivityMatrix: boolean[][] = [];

    /** Shortest path first matrix, used for automatic processing */
    private _SPFMatrix: number[][][] = [];

    /** returns the connectivity matrix */
    public get connectivityMatrix(): boolean[][] {
        return this._connectivityMatrix;
    }

    /**
     * returns the matrix of SPF
     */
    public get SPFMatrix(): number[][][] {
        return this._SPFMatrix;
    }

    /**
     * Creates new Connectivity from the default matrix
     */
    public constructor() {
        // clone the base matrix
        NetGameInit.baseConnectivityMatrix.forEach(value => this._connectivityMatrix.push([...value]));
        this.calculateSPFRouting();
    }

    /**
     * Changes the connectivity of the 2 given node to value
     * Return true if the change was allowed
     * @param node1 : Id of the node1 
     * @param node2 : Id of the node2
     * @param value : true if connected, false if not
     */
    public UpdateConnectivity(node1: number, node2: number, value?: boolean): boolean {
        if (typeof value === "undefined") {
            value = !this._connectivityMatrix[node1][node2];
        }
        this._connectivityMatrix[node1][node2] = value;
        this._connectivityMatrix[node2][node1] = value;
        const oldSPFMatrix = this._SPFMatrix;
        this.calculateSPFRouting()
        if (ConnectivityMatrix.isSPFMatrixOk(this.SPFMatrix, node1)) {
            MonitorServer.broadcast(new ConnectivityUpdate());
            this.updateConnectivityForNode(node1);
            this.updateConnectivityForNode(node2);
            return true;
        }
        else {
            this._SPFMatrix = oldSPFMatrix;
            this._connectivityMatrix[node1][node2] = !value;
            this._connectivityMatrix[node2][node1] = !value;
            return false;
        }
    }

    public updateConnectivityForNode(node : number): void {
        const msg = new Connectivity();
        msg.param.connectivity = this._connectivityMatrix[node];
        NetGame.getNetGame().Nodes[node].broadcast(msg);
    }

    public annotateClass() : ConnectivityMatrix {
        return Object.assign({}, this, { $class: "ConnectivityMatrix" });
    }

    public reset(): void {
        for(let i=0; i<NetGameInit.NODE_NAMES.length; i++) {
            for(let j=0; j<NetGameInit.NODE_NAMES.length; j++) {
                this._connectivityMatrix[i][j]=NetGameInit.baseConnectivityMatrix[i][j];
            }
            this.updateConnectivityForNode(i);
        }
        this.calculateSPFRouting();
        MonitorServer.broadcast(new ConnectivityUpdate());
    }


    /**
     * Returns the next node in the SPF to go to toNode
     * @param fromNode : the node the message is comming from
     * @param toNode : the final node for the message
     */
    public getNextOnRoute(fromNode: number, toNode: number): number {
        const v = this._SPFMatrix[fromNode][toNode];
        if (v == null) {
            logger.error(1, "[NG] ERROR: no SPF entry for source " + fromNode + " and destination " + toNode + "!");
            return toNode;
        }
        const nextNode = v[Math.floor(Math.random() * v.length)];
        if (nextNode == fromNode) {
            logger.error("[NG] ERROR in getNextOnRoute(): " + NetGameInit.NODE_NAMES[fromNode] + "-->" + NetGameInit.NODE_NAMES[toNode] + " loops at " + NetGameInit.NODE_NAMES[fromNode]);
            return toNode; // emergency solution
        }
        return nextNode;
    }

    private calculateSPFRouting(): void {
        logger.info("- recalculating SPF Routing");

        this._SPFMatrix = Array.from(Array(NetGameInit.NODE_NAMES.length), () => new Array(NetGameInit.NODE_NAMES.length))

        for (let i = 0; i < NetGameInit.NODE_NAMES.length; i++) {

            for (let j = 0; j < NetGameInit.NODE_NAMES.length; j++) {
                let minDepth = 9999;

                if (this._connectivityMatrix[i][j]) { // direct?
                    this._SPFMatrix[i][j] = new Array<number>();
                    this._SPFMatrix[i][j].push(j);
                }
                else if (i != j) { // orig<>dest
                    for (let k = 0; k < NetGameInit.NODE_NAMES.length; k++) {
                        if (!this._connectivityMatrix[i][k])
                            continue;

                        const s = this.traverse(k, j, "/" + i + "/" + k + "/");
                        if (s && s.length <= minDepth) {
                            if (s.length < minDepth) {
                                minDepth = s.length;
                                this._SPFMatrix[i][j] = [];
                            }
                            this._SPFMatrix[i][j].push(k);
                        }
                    }
                }
            }
        }
    }

    private static isSPFMatrixOk(matrix: number[][][], nodeID: number) {
        let cnt = 0;
        for(let i = 0; i < matrix[nodeID].length; ++i) {
            if (matrix[nodeID][i] != null) {
                ++cnt;
            }
        }
        return cnt == NetGameInit.NODE_NAMES.length - 1;
    }

    private traverse(node: number, dest: number, path: string): string {
        let minDepth = 9999;
        let minPath: string = null;

        for (let i = 0; i < NetGameInit.NODE_NAMES.length; i++) {
            if (this._connectivityMatrix[node][i]) {
                if (i == dest) {
                    return path + i + "/";
                }
                if (path.includes("/" + i + "/")) {
                    continue;
                }
                const s = this.traverse(i, dest, path + i + "/");
                if (s && (s.length < minDepth)) {
                    minDepth = s.length;
                    minPath = s;
                }
            }
        }
        return minPath;
    }
}