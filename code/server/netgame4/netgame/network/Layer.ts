import { logger } from "../../log";
import { LayerUpdate } from "../../sockets/messages/monitor/LayerUpdate";
import { ProcessMsgParam } from "../../sockets/messages/player/From/ProcessMsg";
import { PlayerState } from "../../sockets/messages/player/PlayerState";
import { MonitorServer } from "../../sockets/MonitorServer";
import { Layers } from "../enums/Layers";
import { Fields } from "../messages/enums/Fields";
import { NetGameMsg } from "../messages/NetGameMsg";
import { NetGame } from "../Netgame";
import { Player } from "./Player";
import { DLLAutoProcess } from "./processing/behavior/DLLAutoProcess";
import { DLLProcess } from "./processing/behavior/DLLProcess";
import { NLAutoProcess } from "./processing/behavior/NLAutoProcess";
import { NLProcess } from "./processing/behavior/NLProcess";
import { PHLAutoProcess } from "./processing/behavior/PHLAutoProcess";
import { PHLProcess } from "./processing/behavior/PHLProcess";

export class Layer {

    /** The messages this layer needs to treat */
    public queue: NetGameMsg[] = [];

    /** DLL Only, contains the messages in the buffer */
    public buffered: NetGameMsg[] = []

    /** The player currently in this node, null if no player */
    player: Player;

    /** the method to process the message, depends of the layer, return a new NetGameMessage */
    private readonly processFunction: (sender: Player, msg: NetGameMsg, param: ProcessMsgParam) => NetGameMsg;

    /** The function used to automatically process the messages */
    private readonly AutoprocessFunction: (nodeId: number, msg: NetGameMsg) => NetGameMsg;

    /** The node this layer is in */
    public readonly _nodeId: number;

    /** The node this layer is in */
    public readonly _layerId: number;

    /**
     * Create a new Layer with the correct processing function according to the id
     * @param layerId : Identifier of the layer, (PHL, DLL, NL, AL), used to set the right processing function
     * @param nodeId : identifier of the node this layer belongs to
     */
    public constructor(nodeId: number, layerId: Layers) {
        this._nodeId = nodeId;
        this._layerId = layerId;
        switch (layerId) {
            case Layers.PHL:
                this.processFunction = PHLProcess;
                this.AutoprocessFunction = PHLAutoProcess;
                break;
            case Layers.DLL:
                this.processFunction = DLLProcess;
                this.AutoprocessFunction = DLLAutoProcess;
                break;
            case Layers.NL:
                this.processFunction = NLProcess;
                this.AutoprocessFunction = NLAutoProcess;
                break;
            case Layers.AL:
                this.processFunction = (_player, msg) => new NetGameMsg(msg);
                this.AutoprocessFunction = (_nodeId, msg) => new NetGameMsg(msg);
        }
    }

    public annotateClass() : Layer {
        return Object.assign({}, this, { $class: "Layer" });
    }

    public createInstance(obj: any) : Layer {
        return Object.assign(new Layer(obj._nodeId, obj._layerId), obj);
    }

    /**
     * Will process a message for this layer
     * @param player : the player that has processed the message
     * @param msg : the value the player has changed to process this message
     */
    public processMsg(player: Player, msg: ProcessMsgParam): void {

        if (!NetGame.getNetGame().gameRunning) {
            logger.warn("PROC01 Game is suspended, no message processing allowed");
            return;
        }

        if ((this.player && this.player.lockedMsgID < 0) && msg.changedValue != -1) {
            logger.warn("PROC02 Message not locked, probably already processed");
            return;
        }

        const oldMsg = this.queue.find(x => x.message.ID == msg.MsgId);

        // some elementary tests to prevent loops
        if (oldMsg.message.msgFields[Fields.SRC] >= 0) {
            // is value set?
            if (oldMsg.message.msgFields[Fields.SRC] == oldMsg.message.msgFields[Fields.DEST]) {
                logger.error("[NG] ERROR in wsProcessMessage: DEST==SRC");
                logger.error("PROC03 Forwarding loop (DEST==SRC) ! [" + msg.MsgId + "]");
                return;
            }
            else if (oldMsg.message.msgFields[Fields.SRC] == oldMsg.message.msgFields[Fields.NEXT]) {
                logger.error("[NG] ERROR in wsProcessMessage: NEXT==SRC");
                logger.error("PROC04 Forwarding loop(NEXT == SRC)![" + msg.MsgId + "]");
                return;
            }
        }

        let newMsg;
        if (msg.changedValue == -1) {
            // layer specific automatic treatement
            newMsg = this.AutoprocessFunction(this._nodeId, oldMsg)
        }
        else {
            // layer specific treatement
            newMsg = this.processFunction(player, oldMsg, msg);

            // update error stats
            player?.updateStats(msg.errorCodes || "", msg.processingTime);
        }
        
        // put msg on new node/layer
        if ((newMsg.toNode >= 0) && (newMsg.toLayer >= 0)) {
            NetGame.getNetGame().Nodes[newMsg.toNode].Layers[newMsg.toLayer].addToQueue(newMsg);
        }

        if (!player) {
            oldMsg.auto=true;
        }

        // remove msg from actual node/layer
        this.queue = this.queue.filter(x => x.message.ID != msg.MsgId);

        NetGame.getNetGame().addMsgToLog(newMsg); // store new msg instance
    }

    /**
     * Add a message in the queue and notify the player and monitors
     * @param message : the messag to add in the queue
     */
    public addToQueue(message: NetGameMsg): void {
        // Add message to the queue
        this.queue.push(message);

        // Update player and monitors
        this.player?.sendMessage(new PlayerState(message.toNode, message.toLayer, this.player.ranking.scoreAvg));
        MonitorServer.broadcast(new LayerUpdate(message.toNode, message.toLayer));
    }
}