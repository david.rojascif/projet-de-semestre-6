import WebSocket from "ws";
import { logger } from "../../log";
import { WsMessage, WsParam } from "../../sockets/messages/WsMessage";
import { ErrorStats } from "../stats/ErrorStats";
import { RankingStats } from "../stats/RankingStats";

export class Player {

    /** the websocket for this player, will not change  */
    private _socket: WebSocket = null;

    /** the alias for this player */
    public alias: string;

    /** the ip of the player */
    public ipAddress: string;

    /** the id of the player */
    public id: number = -1;

    /** the message Id the player is currently processing */
    public lockedMsgID: number = -1;

    /** the layer the player is in */
    public layer: number = -1;

    /** the node the player is in */
    public node: number = -1;

    /** The score of this player */
    public ranking: RankingStats = new RankingStats();

    /** The errors this player has made */
    public errors: ErrorStats = new ErrorStats();

    /**
     * Creates a new player with the given socket
     * @param socket : socket used by this player
     */
    public constructor(socket: WebSocket) {
        this._socket = socket;
    }

    public get socket(): WebSocket {
        return this._socket;
    }

    public annotateClass() : Player {
        this._socket = null; // don't write socket to JSON
        return Object.assign({}, this, { $class: "Player" });
    }

    public hasValidSocket() : boolean {
        return this._socket != null && this._socket.readyState < 2;
    }

    /**
     * send a message to the player
     * @param message : the message to send to the player
     */
    public sendMessage(message: WsMessage<WsParam>): void {
        if (!this.hasValidSocket()) return; // player was reset before (websocket lost)
        this._socket.send(JSON.stringify(message), err => { if (err) logger.error(err) });
    }

    /**
     * reset the player with default values
     */
    public reset(): void {
        this.layer = -1;
        this.node = -1;
        this.lockedMsgID = -1;
        this.alias = null;
    }

    /**
     * Updates the statistics of this player, score,error, ranking
     */
    public updateStats(errorCodesCSV: string, processingTime: number): void {
        let errorCount = 0;
        if (errorCodesCSV.length > 0) {
            const errorCodes = errorCodesCSV.split(",");
            errorCount = errorCodes.length;

            for (const code of errorCodes) {
                this.errors.AddError(code);
            }
        }
        //update scores
        this.ranking.updateScore(this.layer, errorCount, processingTime);
    }
}