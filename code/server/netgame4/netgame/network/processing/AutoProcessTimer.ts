import { LayerUpdate } from "../../../sockets/messages/monitor/LayerUpdate";
import { ProcessMsgParam } from "../../../sockets/messages/player/From/ProcessMsg";
import { MonitorServer } from "../../../sockets/MonitorServer";
import { NetGame } from "../../Netgame";

export class AutoProcessTimer {

    /** the interval for this timer in ms */
    private _interval: number;

    /** the handle to the timer, used to start and stop */
    private _timerID: number;

    /** if the timer is currently running */
    private _isRunning = false;

    /** true if the timer is running, else false*/
    public get IsRunning(): boolean {
        return this._isRunning;
    }

    /**
     * Create a new AutoProcessTimer, the timer is stopped
     * @param interval, the interval in ms, at which auto processs runs
     */
    public constructor(interval: number) {
        this._interval = interval;
    }

    /**
     * stops the timer and the autoprocessing
     */
    public stop(): void {
        this._isRunning = false;
        clearInterval(this._timerID);

    }

    /**
     * starts the timer and the autoprocessing
     */
    public start(): void {
        this._isRunning = true;
        this._timerID = setInterval(this.processMessages, this._interval);
    }

    /**
     * Change the interval to a new value
     * @param interval the new value
     */
    public newInterval(interval: number): void {
        this._interval = interval;
        clearInterval(this._timerID);
        this.start();
    }

    /**
     * Process all messages in layers excepts AL and updates the layer
     * return either 0 if no message was processed or 10000 * number of agents + 1 * number of messages
     */
    public processMessages(repeated : boolean): number {
        let repeat = true;
        let count = 0;
        while(repeat) {
            let wasAMessageProcessed = 0;
            NetGame.getNetGame().Nodes.forEach((node, id) => {
                // Prevent to process AL
                for (let i = 0; i < node.Layers.length - 1; i++) {
                    const layer = node.Layers[i];
                    if (layer.queue.length > 0 && layer.player == null) {
                        layer.queue.forEach(message => {
                            const mid = message.message.ID
                            const param = new ProcessMsgParam();
                            param.MsgId = mid;
                            param.changedValue = -1;
                            layer.processMsg(null, param);
                            MonitorServer.broadcast(new LayerUpdate(id, i));
                            wasAMessageProcessed++;
                        });
                        wasAMessageProcessed += 10000
                    }
                }
            });
            count+=wasAMessageProcessed;
            if (!wasAMessageProcessed || !repeated) {
                repeat = false;
            }
        }
        return count;
    }

    /**
     * Check if the interval is the same as one number, only if the time isn't paused.
     * @param m time to compare with in ms
     */
    public isInterval(m: number): boolean {
        return m === this._interval && this.IsRunning === true;
    }
}