import { InsertError } from "../../enums/InsertError";
import { Fields } from "../../messages/enums/Fields";
import { NetGameMsg } from "../../messages/NetGameMsg";
import { NetGame } from "../../Netgame";

export class AutoProcess {

    /** Offset, to prevent collisions with the ids set by the players */
    private readonly _baseId = 700;

    /** Number of message that have been auto processed */
    private AutoProcessCount = 0;

    /** get the next id of the autoprocessed message */
    public get Id(): number {
        return this._baseId + (++this.AutoProcessCount);
    }

    /** probability of an error to happen, out of 100 */
    public errorProbability: number = 10;

    /** Number of errors decision that have been taken */
    private _errorDecisionCount: number = 0;

    /** Number of erros that have been made */
    private _errorCount: number = 0;

    private introduceError: InsertError = InsertError.Random;

    public get ErrorCount(): number {
        return this._errorCount
    }

    public get ErrorDecisionCount(): number {
        return this._errorDecisionCount;
    }

    public reset(): void {
        this.errorProbability = 10;
        this._errorCount = 0;
        this._errorDecisionCount = 0;
    }

    public IntroduceError(msg: NetGameMsg): boolean {
        if (this.introduceError == InsertError.Error)
            return true;

        let val = false;
        if (this.introduceError == InsertError.Random && this.allowTransmissionError(msg)) {
            val = (Math.random() * 100 < this.errorProbability);
            this._errorDecisionCount++;
            this._errorCount += val ? 1 : 0;
        }
        return val;
    }

    private allowTransmissionError(msg: NetGameMsg): boolean {
        // no errors allowed for ACK or NAK message
        if (msg.isACKorNAK())
            return false;

        const toNode = msg.message.msgFields[Fields.SRC]; // check on NAK with
        const fromNode = msg.message.msgFields[Fields.DEST]; // reverse direction
        const log = NetGame.getNetGame().messageLogs.find(msgLog => msgLog.ID == msg.message.ID); // TODO why not directly use array index?
        const cnt = log.msgLog.length;
        for (let index = cnt - 1; index >= 0; index--) {
            const tmpmsg = log.msgLog[index];
            if (tmpmsg.isNAK() && tmpmsg.message.msgFields[Fields.SRC] == fromNode
                && tmpmsg.message.msgFields[Fields.DEST] == toNode) {
                return false;
            }
        }
        return true;
    }
}