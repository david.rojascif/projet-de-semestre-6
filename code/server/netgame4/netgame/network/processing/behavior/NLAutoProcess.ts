import { NetGame } from "../../../Netgame";
import { Layers } from "../../../enums/Layers";
import { Fields } from "../../../messages/enums/Fields";
import { Status } from "../../../messages/enums/Status";
import { NetGameMsg } from "../../../messages/NetGameMsg";

/**
 * Will automatically process the message for the NL layer
 * @param nodeID : The node that will process the message
 * @param oldMsg : The message we want to process
 */
export function NLAutoProcess(nodeID: number, oldMsg: NetGameMsg): NetGameMsg {

    const newMsg = new NetGameMsg(oldMsg);
    const oldDirection = oldMsg.message.direction;

    // coming up from DLL of same node
    if (oldDirection == 1) {
        // msg arrived at its final destination?
        if (newMsg.message.msgFields[Fields.FINAL] == nodeID) {
            newMsg.status = Status.ARRIVED;
            newMsg.shiftLocationInfo(nodeID, Layers.AL);
        } else {
            newMsg.message.msgFields[Fields.NEXT] = NetGame.getNetGame().connectivity.getNextOnRoute(nodeID, newMsg.message.msgFields[Fields.TO]);
            newMsg.shiftLocationInfo(nodeID, Layers.DLL);
            newMsg.clearDLLFields();
        }
    }
    // coming down from AL of same node
    else {
        newMsg.message.msgFields[Fields.NEXT] = NetGame.getNetGame().connectivity.getNextOnRoute(nodeID, newMsg.message.msgFields[Fields.TO]);
        newMsg.message.msgFields[Fields.FINAL] = newMsg.message.msgFields[Fields.TO];
        newMsg.shiftLocationInfo(nodeID, Layers.DLL);
        newMsg.clearDLLFields();
    }

    return newMsg;
}