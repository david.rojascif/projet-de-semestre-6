import { ProcessMsgParam } from "../../../../sockets/messages/player/From/ProcessMsg";
import { Layers } from "../../../enums/Layers";
import { Fields } from "../../../messages/enums/Fields";
import { NetGameMsg } from "../../../messages/NetGameMsg";
import { Player } from "../../Player";

/**
 * Will process the message for the PHL layer
 * @param player : the player that has treated the message
 * @param oldMsg : the messsage the player has treated
 * @param params : the parameter used by the player to treat this message
 * @returns : the new NetGameMsg
 */
export function PHLProcess(player: Player, oldMsg: NetGameMsg, params: ProcessMsgParam): NetGameMsg {

    const newMsg = new NetGameMsg(oldMsg);
    const oldDirection = oldMsg.message.direction;

    // received from PHL of an other node
    if (oldDirection == 0) {
        // shift location info
        newMsg.shiftLocationInfo(player.node, Layers.DLL);

        if (!newMsg.isACKorNAK()) {
            newMsg.message.msgFields[Fields.FCS] = newMsg.retrieveFCSinRaw();
        }

        // erase raw msg, DLL fields already filled in
        newMsg.message.rawMsg = null;
    }
    else {
        // coming down from DLL of same node
        newMsg.message.msgFields[Fields.FCS] = params.changedValue;
        newMsg.constructRawMsg();
        newMsg.shiftLocationInfo(oldMsg.message.msgFields[Fields.DEST], Layers.PHL);
    }

    return newMsg;
}