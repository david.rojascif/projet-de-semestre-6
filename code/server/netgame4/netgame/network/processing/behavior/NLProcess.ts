import { ProcessMsgParam } from "../../../../sockets/messages/player/From/ProcessMsg";
import { Layers } from "../../../enums/Layers";
import { Fields } from "../../../messages/enums/Fields";
import { Status } from "../../../messages/enums/Status";
import { NetGameMsg } from "../../../messages/NetGameMsg";
import { Player } from "../../Player";

/**
 * Will process the message for the NL layer
 * @param player : the player that has treated the message
 * @param oldMsg : the messsage the player has treated
 * @param params : the parameter used by the player to treat this message
 * @returns : the new NetGameMsg
 */
export function NLProcess(player: Player, oldMsg: NetGameMsg, params: ProcessMsgParam): NetGameMsg {

    const newMsg = new NetGameMsg(oldMsg);
    const oldDirection = oldMsg.message.direction;

    // coming up from DLL of same node
    if (oldDirection == 1) {
        // msg arrived at its final destination?
        if (newMsg.message.msgFields[Fields.FINAL] == player.node) {
            newMsg.status = Status.ARRIVED;
            newMsg.shiftLocationInfo(player.node, Layers.AL);
        }
        else {
            newMsg.message.msgFields[Fields.NEXT] = params.changedValue;
            newMsg.shiftLocationInfo(player.node, Layers.DLL);
            newMsg.clearDLLFields();
        }
    }
    // coming down from AL of same node
    else {
        if (player.node == params.changedValue) {
            params.changedValue = Fields.FINAL; // emergency solution
        }
        newMsg.message.msgFields[Fields.NEXT] = params.changedValue;
        newMsg.message.msgFields[Fields.FINAL] = newMsg.message.msgFields[Fields.TO];
        newMsg.shiftLocationInfo(player.node, Layers.DLL);
        newMsg.clearDLLFields();
    }

    return newMsg;
}