import { NetGame } from "../../../Netgame";
import { ProcessMsgParam } from "../../../../sockets/messages/player/From/ProcessMsg";
import { Layers } from "../../../enums/Layers";
import { Fields } from "../../../messages/enums/Fields";
import { Status } from "../../../messages/enums/Status";
import { Type } from "../../../messages/enums/Type";
import { NetGameMsg } from "../../../messages/NetGameMsg";
import { Player } from "../../Player";

/**
 * Will process the message for the DLL layer
 * @param player : the player that has treated the message
 * @param oldMsg : the messsage the player has treated
 * @param params : the parameter used by the player to treat this message
 * @returns : the new NetGameMsg
 */
export function DLLProcess(player: Player, oldMsg: NetGameMsg, params: ProcessMsgParam): NetGameMsg {

    let newMsg = new NetGameMsg(oldMsg);
    const oldDirection = oldMsg.message.direction;

    // coming up from PHL of same node
    if (oldDirection == 1) {
        // DAT: check FCS and return ACK/NAK, delivery just if FCS ok
        if (newMsg.message.msgFields[Fields.TYPE] == Type.DAT) {
            // if FCS ok then deliver msg and return ACK
            if (newMsg.checkFCS()) {
                const ackMsg = new NetGameMsg();
                ackMsg.related = newMsg.message.ID;

                ackMsg.initLocationInfo(player.node, Layers.PHL, player.node, Layers.DLL, player.node, Layers.PHL);
                ackMsg.message.msgFields[Fields.SRC] = player.node;
                ackMsg.message.msgFields[Fields.DEST] = oldMsg.message.msgFields[Fields.SRC];
                ackMsg.message.msgFields[Fields.TYPE] = Type.ACK;
                ackMsg.message.msgFields[Fields.SEQNO] = oldMsg.message.msgFields[Fields.SEQNO];
                ackMsg.message.msgFields[Fields.FCS] = 0;

                NetGame.getNetGame().addNewMsg(ackMsg);
                NetGame.getNetGame().Nodes[player.node].Layers[Layers.PHL].addToQueue(ackMsg);

                newMsg.shiftLocationInfo(player.node, Layers.NL);
                newMsg.related = ackMsg.message.ID;
            }
            else {
                // FCS not ok, thus return msg as NAK
                newMsg.prepareFieldsForNAK();
                newMsg.shiftLocationInfo(player.node, Layers.PHL);
            }
        }
        // ACK: check which buffered msg is acknowledged by the ACK and remove it
        else if (newMsg.message.msgFields[Fields.TYPE] == Type.ACK) {
            // Remove message with same seqNo from buffer
            const layer = NetGame.getNetGame().Nodes[player.node].Layers[Layers.DLL];
            layer.buffered = layer.buffered.filter(x => x.message.msgFields[3] !== oldMsg.message.msgFields[3]);

            newMsg.shiftLocationInfo(-1, -1);
            newMsg.status = Status.ARRIVED;
        }
        // NAK: check which buffered msg has to be retransmitted
        else if (newMsg.message.msgFields[Fields.TYPE] == Type.NAK) {
            const layer = NetGame.getNetGame().Nodes[player.node].Layers[Layers.DLL];
            const msg = layer.buffered.find(x => x.message.msgFields[3] == oldMsg.message.msgFields[3]);
            newMsg = msg;
        }
    }
    // coming down from NL of same node
    else {
        newMsg.message.msgFields[Fields.SRC] = player.node;
        newMsg.message.msgFields[Fields.DEST] = newMsg.message.msgFields[Fields.NEXT];
        newMsg.message.msgFields[Fields.TYPE] = Type.DAT;
        newMsg.message.msgFields[Fields.SEQNO] = params.changedValue;

        if (newMsg.message.msgFields[Fields.TYPE] == Type.DAT) {
            newMsg.message.msgFields[Fields.FCS] = newMsg.calculateFCS();
        }

        // save msg in buffer
        newMsg.shiftLocationInfo(player.node, Layers.PHL);
        NetGame.getNetGame().Nodes[newMsg.toNode].Layers[Layers.DLL].buffered.push(newMsg);
    }

    return newMsg;
}