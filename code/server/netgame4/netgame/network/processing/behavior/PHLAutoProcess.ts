import { NetGame } from "../../../Netgame";
import { Layers } from "../../../enums/Layers";
import { Fields } from "../../../messages/enums/Fields";
import { NetGameMsg } from "../../../messages/NetGameMsg";

/**
 * Will automatically process the message for the PHL layer
 * @param nodeID : The node that will process the message
 * @param oldMsg : The message we want to process
 */
export function PHLAutoProcess(nodeID: number, oldMsg: NetGameMsg): NetGameMsg {

    const newMsg = new NetGameMsg(oldMsg);
    const oldDirection = oldMsg.message.direction;

    // received from PHL of an other node
    if (oldDirection == 0) {
        // shift location info
        newMsg.shiftLocationInfo(nodeID, Layers.DLL);

        if (!newMsg.isACKorNAK()) {
            newMsg.message.msgFields[Fields.FCS] = newMsg.retrieveFCSinRaw();
        }

        // erase raw msg, DLL fields already filled in
        newMsg.message.rawMsg = null;
    }
    // coming down from DLL of same node
    else {
        if (NetGame.getNetGame().AutoProcess.IntroduceError(oldMsg)) {
            newMsg.message.msgFields[Fields.FCS]++;
        }

        newMsg.constructRawMsg();
        newMsg.shiftLocationInfo(oldMsg.message.msgFields[Fields.DEST], Layers.PHL);
    }

    return newMsg;
}