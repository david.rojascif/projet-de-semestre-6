import WebSocket from "ws";
import { WsMessage, WsParam } from "../../sockets/messages/WsMessage";
import { logger } from "../../log";


export class Admin {

    /** the websocket for this admin, will not change  */
    private _socket: WebSocket = null;

    /** the ip of the admin */
    public ipAddress: string;

    /** the id of the admin */
    public id: number = -1;

    /**
     * Creates a new player with the given socket
     * @param socket : socket used by this admin
     */
    public constructor(socket: WebSocket) {
        this._socket = socket;
    }

    public get socket(): WebSocket {
        return this._socket;
    }

    public annotateClass() : Admin {
        this._socket = null; // don't write socket to JSON
        return Object.assign({}, this, { $class: "Admin" });
    }

    public hasValidSocket() : boolean {
        return this._socket != null && this._socket.readyState < 2;
    }

    /**
    * send a message to the admin
    * @param message : the message to send to the admin
    */
    public sendMessage(message: WsMessage<WsParam>): void {
        this._socket.send(JSON.stringify(message), err => { if(err) logger.error(err) });
    }
}