import { IncomingMessage } from 'http';
import { Socket } from 'net';
import url from 'url';
import { MonitorServer } from './sockets/MonitorServer';
import { PlayerServer } from './sockets/PlayerServer';
import { AdminServer } from './sockets/AdminServer';

export default function wsRouter(req: IncomingMessage, socket: Socket, head: Buffer): void {
    // Send the request to the right route, if no routes socket is destroyed
    switch (url.parse(req.url).pathname) {
        case '/websocket/playing':
            PlayerServer.server.handleUpgrade(req, socket, head, (ws) => PlayerServer.server.emit('connection', ws, req));
            break;
        case '/websocket/monitor':
            MonitorServer.server.handleUpgrade(req, socket, head, (ws) => MonitorServer.server.emit('connection', ws, req));
            break;
        case '/websocket/admin':
            AdminServer.server.handleUpgrade(req, socket, head, (ws) => AdminServer.server.emit('connection', ws, req));
            break;
        default:
            socket.destroy();
            break;
    }
}
