/**
 * Extend type Express.User (as used in Express.Request) by attribute 'role'
 * (needed for TypeScript compiler)
 * 
 * similar to https://dev.to/kwabenberko/extend-express-s-request-object-with-typescript-declaration-merging-1nn5
 */

import ''; // dummy import to make this a module

declare global {
    namespace Express {
        interface User {
            role: string;
            id: number;
        }
    }
}

