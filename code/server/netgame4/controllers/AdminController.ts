import { Request } from "express";
// NetGame core
import { NetGame } from "../netgame/Netgame";
import { NetGameInit } from "../netgame/NetgameInit";
import { NetGameMsg } from "../netgame/messages/NetGameMsg";
import { Fields } from "../netgame/messages/enums/Fields";
import { Status } from "../netgame/messages/enums/Status";
import { MessageHistory } from "../netgame/stats/MessageHistory";
import { RoutingStats } from "../netgame/stats/RoutingStats"
import { Player } from "../netgame/network/Player";
import { Monitor } from "../sockets/Monitor";
import { Layers } from "../netgame/enums/Layers";
import { Snapshot } from "../netgame/json/Snapshot";
// sockets
import { MonitorServer } from "../sockets/MonitorServer";
import { PlayerServer } from "../sockets/PlayerServer";
// common message
import { GameReset } from "../sockets/messages/common/GameReset";
// monitor messages
import { AliasAllReset } from "../sockets/messages/monitor/AliasAllReset";
import { AliasReset } from "../sockets/messages/monitor/AliasReset";
import { AliasUpdate } from "../sockets/messages/monitor/AliasUpdate";
import { LayerUpdate } from "../sockets/messages/monitor/LayerUpdate";
import { MonitorInit } from "../sockets/messages/monitor/MonitorInit";
import { MsgInit } from "../netgame/messages/MsgInit";
import { ToggleOverlayParams } from '../sockets/messages/monitor/ToggleOverlay';
// player messages
import { Notification } from "../sockets/messages/player/Notification";
import { LaunchMessage } from "../sockets/messages/player/LaunchMessage";
import { PlayerState } from "../sockets/messages/player/PlayerState";
import { ProcessMsgParam } from "../sockets/messages/player/From/ProcessMsg";
import { RegisterAllow } from "../sockets/messages/player/RegisterAllow";
import { RegistrationReset } from "../sockets/messages/player/RegistrationReset";
// logger
import { logger } from "../log";
// date utility
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
dayjs.extend(duration);
// file system
import fs = require('fs');


/**
 * Controls communication between admin interface and game.
 * Dispatches and handles requests from admin interface (via admin routes).
 */
export class AdminController {

    public static expertMode = false;

    // snapshot information
    public static readonly snapshotVersion = "1.0"
    public static readonly classList = ["ISC-1a", "ISC-1b", "ISC-1d", "Frühling", "Herbst"];
    public static readonly courseList = ["HEIA-FR Téléinfo", "HTA-FR Teleinfo", "BFH CAS-ITP"];

    /**
     * Function called every time the admin page is loaded. It choses the correct command treatment
     * and shows the correct view depending on which command was selected.
     * Allows to do some preprocessing and error control before generating the view with cmd.ejs
     * @param req string situated after admin/
     * @return view used by cmd.ejs to show the proper part of the page depending on selected command.
     */
    public static pickFunction(req: Request) {
        let cmd = req.params.cmd;
        const cmdSplit = cmd.split('|');
        const viewParms = { view: "", success: null as string, error: null as string };
        switch (cmdSplit[0]) {  // TODO replace switch by lookup of anonymous functions ?

            //=== MAIN PANEL ===
            //--- Game State
            case "run":
                NetGame.getNetGame().gameRunning = !NetGame.getNetGame().gameRunning;
                viewParms.success = NetGame.getNetGame().gameRunning ? "Game is now RUNNING" : "Game is now SUSPENDED";
                break;
            case "reset":
                AdminController.resetGame();
                viewParms.success = "Full game reset done !";
                break;
            case "snapshotRead":
            case "snapshotWrite":
                viewParms.view = cmd;
                break;

            //--- Registration
            case "reg":
                NetGame.getNetGame().isRegistrationOpen = !NetGame.getNetGame().isRegistrationOpen;
                viewParms.success = "Registration is now " + (NetGame.getNetGame().isRegistrationOpen ? "OPEN" : "CLOSED");
                break;
            case "regReset":
                AdminController.regReset();
                viewParms.success="Registration of all players was reset !"
                break;

            //--- Messages
            case "init":
                AdminController.initFirstSet();
                viewParms.success = "Set 1 of initial messages has been sent."
                break;
            case "init2":
                AdminController.initSecondSet();
                viewParms.success = "Set 2 of initial messages has been sent."
                break;
            case "launch":
                const from = Number(req.body["msgfield"+Fields.FROM]);
                const to = Number(req.body["msgfield"+Fields.TO]);
                if (from >= 0 && from < NetGameInit.NODE_NAMES.length && to >= 0 && to < NetGameInit.NODE_NAMES.length) {
                    if (from == to) {
                        viewParms.error = "Source and destination are identical";
                    } else if (!req.body["msgfield"+Fields.MSG]) {
                        viewParms.error = "Missing message text"
                    } else {
                        AdminController.launchMessage(from, to, req.body["msgfield"+Fields.MSG]);
                        viewParms.success = "Message launch successful !";
                    }
                } else {
                    viewParms.error = "Launch: invalid from/to parameter value";
                }
                break;
            case "showAll":
                viewParms.view = cmd;
                break;

            //--- Players
            case "notes":
                if (req.body.message === "") {
                    viewParms.error = "Empty notification text, so nothing was sent.";
                } else if (!AdminController.getPlayer(Number(req.body.node), Number(req.body.layer))&&Number(req.body.node)>=0&&Number(req.body.layer)>=0) {
                    viewParms.error = "This layer is played by an agent, can't send a notification.";
                } else {
                    viewParms.success = AdminController.sendNotes(Number(req.body.node), Number(req.body.layer), req.body.message);
                }
                break;
            case "statsError":
            case "statsScore":
                viewParms.view = cmd;
                break;
            case "websocket":
                viewParms.view = cmd;
                break;

            //--- Monitor
            case "ranking":
                if (MonitorServer.isOverlayDisplayed())
                    MonitorServer.hideOverlay();
                viewParms.error=MonitorServer.showOverlay("ranking");
                break;
            case "routing":
                if (MonitorServer.isOverlayDisplayed())
                    MonitorServer.hideOverlay();
                const params = new ToggleOverlayParams('routing');
                RoutingStats.fillRoutingStats(params);
                viewParms.error=MonitorServer.showRoutingOverlay(params);
                break;
            case "hideOverlay":
                if (typeof cmdSplit[1] != "undefined") {
                    if (cmdSplit[1] == "history") {
                        viewParms.view = "inspect|"+cmdSplit[2]+"|"+cmdSplit[3];
                    }
                }
                MonitorServer.hideOverlay();
                break;

            //--- Routing
            case "changeRouting":
                if (cmdSplit.length === 3 || cmdSplit.length === 4) {
                    if (!NetGame.getNetGame().connectivity.UpdateConnectivity(Number(cmdSplit[1]), Number(cmdSplit[2]))) {
                        viewParms.error = "Removal of this link is not allowed, this would either isolate a node or divide the network.";
                    }
                    cmd = "changeRouting"
                    if (cmdSplit.length === 4) {
                        cmd += "|" +cmdSplit[3];
                    }
                }
                viewParms.view = cmd
                break;
            case "statsRouting":
                viewParms.view = cmd;
                break;

            //--- Agents
            case "triggerOnce":
            case "triggerRepeated":
                if (!NetGame.getNetGame().gameRunning) {
                    viewParms.error = "Agents can't be triggered, the game is currently suspended !";
                } else {
                    const repeated = (cmdSplit[0] != "triggerOnce");
                    const count = NetGame.getNetGame().ProcessTimer.processMessages(repeated);
                    viewParms.success = (repeated ? "REPEATED" : "ONCE") + ": " + Math.floor(count/10000) + " agent(s) activated, " + (count % 10000) + " message(s) processed.";
                }
                break;
            case "periodic":
                viewParms.success = AdminController.changeInterval(Number(req.body.period));
                break;

            //--- Misc
            case "errorProb":
                const n = Number(req.body.errorProbability);
                if (n>=0 && n<=100) {
                    NetGame.getNetGame().AutoProcess.errorProbability = n;
                    viewParms.success = "Error probability set to " + n + " %";
                }
                else {
                    viewParms.error = "Could not set error probability (invalid number)";
                }
                break;
            case "toggleMode":
                AdminController.expertMode = !AdminController.expertMode;
                viewParms.success = "View set to " + (AdminController.expertMode ? "EXPERT" : "NORMAL") + " mode";
                break;
            case "logLevel":
                const level = Number(req.body.logLevel);
                if (level>=0 && level<=6) {
                    AdminController.setLoggerLevel(level);
                    viewParms.success = "Logger level set to "+logger.settings.minLevel;
                } else {
                    viewParms.error = level + " isn't a valid logger level";
                }
                break;

            //=== SNAPSHOT
            case "read":
                viewParms.success = AdminController.readSnapshot(cmdSplit[1]);
                break;
            case "write":
                if (req.body.class && req.body.round && req.body.course) {
                    const ok = AdminController.writeSnapshot(req.body.course, req.body.class, req.body.round, req.body.comment);
                    if (ok) viewParms.success = "Game state snapshot successfully written to file";
                    else viewParms.error = "ERROR while writing snapshot file";
                } else {
                    viewParms.error = "Can't write the snapshot, there are missing arguments";
                }
                break;
            case "deleteSnapshot":
                if (cmdSplit.length != 2) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    const filename = cmdSplit[1] + '.json';
                    if (fs.existsSync("./dumps/" + filename)) {
                        fs.unlinkSync("./dumps/" + filename);
                    } else {
                        viewParms.error = "File does not exist ?!";
                    }
                }
                viewParms.view = 'snapshotRead'
                break;

            //=== MESSAGE LIST
            case "history":
                if (MonitorServer.isOverlayDisplayed()) {
                    MonitorServer.hideOverlay();
                }
                viewParms.error = MonitorServer.showOverlay("history");
                if (typeof cmdSplit[2] === "undefined") {
                    cmdSplit[2] = "0";
                }
                viewParms.view = "inspect|"+cmdSplit[1]+"|"+cmdSplit[2];
                break;
            case "deactivate":
            case "reactivate":
                if (cmdSplit.length != 2) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    viewParms.success = AdminController.deactivateMessage(Number(cmdSplit[1]))
                }
                break;

            //=== ROUTING PANEL
            case "resetRouting":
                NetGame.getNetGame().connectivity.reset();
                viewParms.success = "The connectivity has been reset."
                viewParms.view = "changeRouting";
                if (cmdSplit.length === 2) {
                    viewParms.view += "|" + cmdSplit[1];
                }
                break;

            //=== STATUS PANEL
            case "showStatus":
                if (cmdSplit.length != 3)
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                else
                    viewParms.view = cmd;
                break;
            case "enterNote":
            case "showLaunch":
            case "inspect":
                viewParms.view = cmd;
                break;
            case "clearReg":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    AdminController.clearReg(Number(cmdSplit[1]), Number(cmdSplit[2]));
                    viewParms.success = "Registration for this node/layer cleared!";
                }
                break;
            case "changeAlias":
                AdminController.changeAlias(Number(req.body.node), Number(req.body.layer), req.body.alias);
                viewParms.success = "Player alias changed";
                break;
            case "inviteLaunch":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    NetGame.getNetGame().Nodes[Number(cmdSplit[1])].unicast(new LaunchMessage(), Layers.NL);
                    viewParms.success = "Player has been invited to launch a message";
                }
                break;
            case "regAllowNodeLayer":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    viewParms.success = AdminController.regAllowNodeLayer(Number(cmdSplit[1]), Number(cmdSplit[2]));
                }
                break;
            case "processMessage":
                if (cmdSplit.length != 4) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else if (!NetGame.getNetGame().gameRunning) {
                    viewParms.error = "Agents can't be triggered, the game is currently suspended !";
                }
                else {
                    viewParms.success = AdminController.processMessage(Number(cmdSplit[1]), Number(cmdSplit[2]), Number(cmdSplit[3]));
                }
                viewParms.view = "showStatus|"+cmdSplit[1]+"|"+cmdSplit[2];
                break;
            case "lifesaver":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                }
                else {
                    const count = AdminController.lifeSaver(Number(cmdSplit[1]), Number(cmdSplit[2]));
                    if (count == 0) {
                        viewParms.error = "Could not process any messages !?!";
                    } else  {
                        viewParms.success = "Helped layer by processing " + count + " messages";
                    }
                }
                viewParms.view = "showStatus|"+cmdSplit[1]+"|"+cmdSplit[2];
                break;
            case "bufdel":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    AdminController.bufdel(Number(cmdSplit[1]), Number(cmdSplit[2]));
                    viewParms.view = "showStatus|"+cmdSplit[1]+"|"+Layers.DLL;
                }
                break;
            case "clearLock":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    AdminController.getPlayer(Number(cmdSplit[1]), Number(cmdSplit[2])).lockedMsgID = -1;
                    viewParms.view = "showStatus|"+cmdSplit[1]+"|"+cmdSplit[2];
                }
                break;

            //=== WEBSOCKET PANEL
            case "regAllowID":
                if (cmdSplit.length != 2) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    AdminController.regAllowID(Number(cmdSplit[1]));
                    viewParms.view = "websocket";
                    viewParms.success = "Permission to do registration sent to player"+ cmdSplit[1] + ".";
                }
                break;
            case "closePlayerWS":
                if (cmdSplit.length != 2) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    PlayerServer.closeWS(Number(cmdSplit[1]));
                    viewParms.view = "websocket";
                    viewParms.success = "Player Websocket with id=" + cmdSplit[1] + " closed.";
                }
                break;
            case "CloseMonitorWS":
                if (cmdSplit.length != 2) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    MonitorServer.closeWS(Number(cmdSplit[1]));
                    viewParms.view = "websocket";
                    viewParms.success = "Monitor Websocket with id=" + cmdSplit[1] + " closed."
                }
                break;

            //=== Message History Pannel
            case "showEditHistory":
                viewParms.view = cmd;
                break;
            case "edit":
                if (cmdSplit.length != 3) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    AdminController.getMessageInstance(Number(cmdSplit[1]), Number(cmdSplit[2])).editValues(req);
                    viewParms.view = "inspect|"+cmdSplit[1]+"|1";
                    viewParms.success = "Message edit done."
                }
                break;
            case "delete":
                if (cmdSplit.length != 2) {
                    viewParms.error = "Wrong parameter count in \"" + cmd + "\" !";
                } else {
                    AdminController.deleteMsg(Number(cmdSplit[1]));
                    viewParms.view = "inspect|"+cmdSplit[1]+"|1";
                    viewParms.success = "Message history entry deleted.";
                }
                break;
            default:
                viewParms.error = "Unknown command : " + cmd;
        }
        return viewParms;
    }

    //--- FUNCTIONS SELECTED BY CLICKING ON A BUTTON

    /**
     * Send one of the "first message sets" (defined in NetGameInit) to the players.
     * Adds 2 messages on network layer of each node, message text is randomly set
     * There are two predefined sets available.
     */
    private static initFirstSet(): void {
        // Choose random set
        const set = NetGameInit.setIndex++ % NetGameInit.firstSets.length;
        // Choose random starting quote
        const startquotes = Math.floor(Math.random() * NetGameInit.firstSets[set].length);

        // Add the messages in the node[from] in layer NL
        NetGameInit.firstSets[set].forEach(([from, to], idx) => {
            const newMsg = new NetGameMsg();
            newMsg.initLocationInfo(-1, -1, from, Layers.AL, from, Layers.NL);
            newMsg.message.msgFields[Fields.FROM] = from;
            newMsg.message.msgFields[Fields.TO] = to;
            newMsg.message.msgString = NetGameInit.peanutsQuotes[set][(startquotes + idx) % NetGameInit.firstSets[set].length];

            NetGame.getNetGame().addNewMsg(newMsg);
            NetGame.getNetGame().Nodes[newMsg.toNode].Layers[newMsg.toLayer].addToQueue(newMsg);
        });
    }

    /**
     * Send one of the "second message sets" (defined in NetGameInit) to the players.
     * Similar to "first message sets", but with other randomly selected quotes.
     */
    private static initSecondSet(): void {
        // Choose random set
        const set = NetGameInit.setIndex2 % NetGameInit.secondSets.length;

        // Add the messages in the node[from] in layer NL
        NetGameInit.secondSets[set].forEach(([from, to], idx) => {
            const newMsg = new NetGameMsg();
            newMsg.initLocationInfo(-1, -1, from, Layers.AL, from, Layers.NL);
            newMsg.message.msgFields[Fields.FROM] = from;
            newMsg.message.msgFields[Fields.TO] = to;
            newMsg.message.msgString = NetGameInit.proverbs[NetGameInit.shuffle[idx] + NetGameInit.setIndex2 * NetGameInit.secondSets[set].length % NetGameInit.proverbs.length]

            NetGame.getNetGame().addNewMsg(newMsg);
            NetGame.getNetGame().Nodes[newMsg.toNode].Layers[newMsg.toLayer].addToQueue(newMsg);
        });
        NetGameInit.setIndex2++;
        NetGameInit.doShuffle();
    }

    /**
     * Fully reset the game
     * Monitors and Players get notified and reset is done
     */
    private static resetGame() {
        MonitorServer.broadcast(new GameReset());
        PlayerServer.broadcast(new GameReset());
        NetGame.getNetGame().reset();
        //connectivity can't be included in the main reset to avoid having an infinite loop of new netGame
        NetGame.getNetGame().connectivity.reset();
    }

    /**
     * Reads game state snapshot from JSON file
     * @param filename name of the JSON file
     * @returns 
     */    
    private static readSnapshot(filename : string) : string {
        const game: NetGame = NetGame.getNetGame();
        
        // reset game and report this to monitors
        game.reset();
        MonitorServer.broadcast(new GameReset());

        const snapshot = Snapshot.readFromFile('./dumps/' + filename + '.json');
        
        // restore game state information
        game.gameRunning = snapshot.gameState.gameRunning;
        game.isRegistrationOpen = snapshot.gameState.registration;
        game.messageLogs = snapshot.logs;
        game.Nodes = snapshot.nodes;
        game.connectivity = snapshot.connectivity;

        // adjust timestamps in messages
        const now = Date.now();
        game.timeStart = now - (snapshot.info.time - snapshot.gameState.timeStart);
        game.updateMessageTimestamps(now - snapshot.info.time);
        
        // report new state to monitors
        MonitorServer.broadcast(new MonitorInit());
        return "Snapshot successfully read and loaded";
    }

    /**
     * Writes snapshot of game state to JSON file
     * @param course institution and course name
     * @param className name of class
     * @param round indicates round number (1,2,3)
     * @returns confirmation text
     */
    private static writeSnapshot(course: string, className: string, round: string, comment: string) : boolean {
        const game: NetGame = NetGame.getNetGame();

        // collect game state information
        const info = {
            version : "1.0",
            time: Date.now(),
            course : course,
            class : className,
            round : round,
            comment: comment
        };
        const gameState = {
            timeStart: game.timeStart,
            gameRunning: game.gameRunning,
            registrationOpen: game.isRegistrationOpen
        };
        const snapshot = {
            info: info,
            gameState: gameState,
            logs: game.messageLogs,
            nodes: game.Nodes,
            connectivity: game.connectivity
        };
        const filename = dayjs().format("YYYY-MM-DD_HH-mm")+".json";
        return Snapshot.writeToFile('./dumps/' + filename, snapshot);
    }

    /**
     * Manually launch a message
     * @param to destination nodeID
     * @param from sender nodeID
     * @param message message text to be sent
     */
    private static launchMessage(from: number, to: number, message: string) {
        const newMsg = new NetGameMsg();
        newMsg.initLocationInfo(-1, -1, from, Layers.AL, from, Layers.NL);
        newMsg.message.msgFields[Fields.FROM] = from;
        newMsg.message.msgFields[Fields.TO] = to;
        newMsg.message.msgString = message;
        NetGame.getNetGame().addNewMsg(newMsg);
        NetGame.getNetGame().Nodes[newMsg.toNode].Layers[newMsg.toLayer].addToQueue(newMsg);
    }

    /**
     * Change the interval between the automatic process of the messages
     * @param interval amount of time between process in ms. 0 means the process is turned off.
     */
    private static changeInterval(interval: number) {
        if (interval === 0) {
            NetGame.getNetGame().ProcessTimer.stop();
            return "Timer canceled"
        } else {
            NetGame.getNetGame().ProcessTimer.newInterval(interval);
            return "Timer set to "+interval/1000+" seconds"
        }
    }

    /**
     * Delete a message in the buffer of a node in the layer DLL
     * @param node the node where the message is
     * @param n the placement of the message in the buffer
     */
    private static bufdel(node : number, n : number) {
        const layer = NetGame.getNetGame().Nodes[node].Layers[Layers.DLL];
        layer.buffered.splice(Number(n), 1);
        AdminController.getPlayer(node, Layers.DLL)?.sendMessage(new PlayerState(node, Layers.DLL, AdminController.getPlayer(node, Layers.DLL).ranking.scoreAvg));
        MonitorServer.broadcast(new LayerUpdate(node, Layers.DLL));
    }

    /**
     * Send notes to some players
     * @param node which nodes to send the notes to (-1 for broadcast)
     * @param layer which layers to send the notes to (-1 for broadcast)
     * @param message content of the notification
     */
    private static sendNotes(node: number, layer: number, message: string) {
        let okmsg;
        const notification = new Notification();
        notification.param.message = message;
        if (node === -1 && layer === -1) {
            PlayerServer.broadcast(notification);
            okmsg = "Notification sent to ALL layers of ALL nodes.";
        } else if (layer === -1) {
            NetGame.getNetGame().Nodes[node].broadcast(notification);
            okmsg = "Notification sent to ALL layers of " + NetGameInit.NODE_NAMES[node] + ".";
        }
        else if (node === -1) {
            NetGame.getNetGame().Nodes.forEach(function (node) {
                node.unicast(notification, layer);
            })
            okmsg = "Notification sent to layer " + NetGameInit.LAYER_NAMES[layer] + " of ALL nodes.";
        }
        else {
            NetGame.getNetGame().Nodes[node].unicast(notification, layer);
            okmsg = "Notification sent to " + NetGameInit.NODE_NAMES[node] + "/" + NetGameInit.LAYER_NAMES[layer] + ".";
        }
        return okmsg;
    }

    /**
     * Resets registrations : removes registration of all players
     */
    private static regReset() {
        MonitorServer.broadcast(new AliasAllReset());
        PlayerServer.broadcast(new RegistrationReset());
        for (let i = 0; i < NetGameInit.NODE_NAMES.length; i++) {
            for (let j = 0; j < NetGameInit.LAYER_NAMES.length-1; j++) {
                if (NetGame.getNetGame().Nodes[i].Layers[j].player != null) {
                    NetGame.getNetGame().clearPlayerRegistration(i,j);
                }
            }
        }
    }


    /**
     * Clears the registration of a player of a given node/layer
     * @param node
     * @param layer
     */
    private static clearReg(node : number, layer : number) {
        // Monitor Alias Reset
        MonitorServer.broadcast(new AliasReset(node, layer));
        // Player RegistrationReset
        NetGame.getNetGame().Nodes[node].unicast(new RegistrationReset(), layer);
        NetGame.getNetGame().clearPlayerRegistration(node, layer);
    }

    /**
     * Deactivates the message with given ID (not deleted).
     * It will be removed from the queue of the current layer.
     * If called on an already deactivated message, it will be reactivated.
     * @param id the message ID
     */
    private static deactivateMessage(id : number) : string {
        let confirmationMsg = "";
        const msg = AdminController.getMessagewithID(id);

        if (msg.toNode < 0) { // msg obviously alread arrived -> deactivation is useless
            logger.error("trying to (de-)activate arrived DAT/ACK");
            return "Could not change message activation state (invalid";
        }

        const layer = NetGame.getNetGame().Nodes[msg.toNode].Layers[msg.toLayer];
        if (msg.status != Status.BANNED) {
            msg.status = Status.BANNED;
            layer.queue = layer.queue.filter((x: { message: { ID: number; }; }) => x.message.ID != msg.message.ID);
            confirmationMsg = "Message was DEactivated";
        } else {
            msg.status = (layer._layerId === Layers.AL ? Status.ARRIVED : Status.OK);
            layer.queue.push(msg);
            confirmationMsg = "Message was REactivated";
        }
        MonitorServer.broadcast(new LayerUpdate(msg.toNode, msg.toLayer));
        const player = layer.player;
        if (player) {
            player.sendMessage(new PlayerState(msg.toNode, msg.toLayer, player.ranking.scoreAvg));
        }
        return confirmationMsg;
    }

    /**
     * Changes the alias of a player of a given node/layer
     * @param node
     * @param layer
     * @param alias the new alias
     */
    private static changeAlias(node: number, layer: number, alias: string) {
        NetGame.getNetGame().Nodes[node].Layers[layer].player.alias=alias;
        MonitorServer.broadcast(new AliasUpdate(node, layer, alias));
    }

    /**
     * Removes half of the message from a specific layer. Used to help a player that is too behind.
     * @param node
     * @param layer
     */
    private static lifeSaver(node : number, layer : number) {
        logger.info("[NG] Lifesaver: automatic processing for " + NetGameInit.NODE_NAMES[node] + "/"
            + NetGameInit.LAYER_SHORT_NAMES[layer]);
        const lockedMsgID = AdminController.getPlayer(node, layer).lockedMsgID;
        const queueSize = NetGame.getNetGame().Nodes[node].Layers[layer].queue.length;
        if (queueSize < 2) return 0;

        let cntMsgsToProcess = queueSize/2;
        const cnt = cntMsgsToProcess;
        let index = 0;
        while (cntMsgsToProcess > 0) {
            const tmp : NetGameMsg = AdminController.getQueuedMessage(node,layer,index)
            if (tmp.message.ID === lockedMsgID) {
                index++;
            } else {
                AdminController.processMessage(node, layer, tmp.message.ID);
                cntMsgsToProcess--;
            }
        }
        return cnt;
    }

    /**
     * Processes a message once and update monitor and player server.
     * @param node
     * @param layer
     * @param id
     */
    private static processMessage(node : number, layer : number, id : number) {
        const param = new ProcessMsgParam();
        param.MsgId = id;
        param.changedValue = -1;
        NetGame.getNetGame().Nodes[node].Layers[layer].processMsg(null, param);
        MonitorServer.broadcast(new LayerUpdate(node, layer));
        return "Message with ID=" + id + " of " + NetGameInit.NODE_NAMES[node] + "/" +
            NetGameInit.LAYER_SHORT_NAMES[layer] + " has been processed";
    }

    /**
     * Unregisters a player and allow him to register to another node/layer
     * @param node
     * @param layer
     */
    private static regAllowNodeLayer(node: number, layer: number) {
        const message = new RegisterAllow();
        message.param.regState = NetGame.getNetGame().isRegistrationOpen;
        AdminController.getPlayer(node, layer).sendMessage(message);
        return "Request to renew registration sent to "
            + NetGameInit.NODE_NAMES[node] + "/"
            + NetGameInit.LAYER_SHORT_NAMES[layer];
    }

    /**
     * Allows a player to register with his ID
     * @param id
     */
    private static regAllowID(id : number) {
        const message = new RegisterAllow();
        message.param.regState = NetGame.getNetGame().isRegistrationOpen
        PlayerServer.unicast(id, message);
    }

    /**
     * Removes the lastest entry of a msgLog
     * @param msgID
     */
    private static deleteMsg(msgID : number) {
        const messageLog = NetGame.getNetGame().messageLogs[msgID];
        if (messageLog.msgLog.length>1) {
            const oldmsg = messageLog.msgLog[messageLog.msgLog.length-1];
            const oldnode = oldmsg.toNode;
            const oldlayer = oldmsg.toLayer;
            messageLog.msgLog.splice(messageLog.msgLog.length-1, 1);
            if (oldnode>=0||oldlayer>=0) {
                NetGame.getNetGame().Nodes[oldnode].Layers[oldlayer].queue = NetGame.getNetGame().Nodes[oldnode].Layers[oldlayer].queue.filter(x => x.message.ID != msgID);
                MonitorServer.broadcast(new LayerUpdate(oldnode, oldlayer));
                AdminController.getPlayer(oldnode, oldlayer)?.sendMessage(new PlayerState(oldnode, oldlayer, AdminController.getPlayer(oldnode, oldlayer).ranking.scoreAvg));
                const msg = messageLog.msgLog[messageLog.msgLog.length - 1];
                const node = msg.toNode;
                const layer = msg.toLayer;
                NetGame.getNetGame().Nodes[node].Layers[layer].addToQueue(msg);
            }
        }
    }

    /**
     * set the Logger to the proper level, the bigger the level, the less logs are shown.
     * @param level
     */
    public static setLoggerLevel(level : number): void {
        // levels: 0: silly, 1: trace, 2: debug, 3: info, 4: warn, 5: error, 6: fatal
        logger.settings.minLevel = level;
//        const logLevels : TLogLevelName[] = ['silly', 'trace', 'debug', 'info', 'warn', 'error', 'fatal'];
//        logger.setSettings({minLevel:logLevels[level]})
    }

    //--- FUNCTION USED TO GENERATE VIEWS

    /**
     * Generate the different option of a select
     * @param title the name attribut of the select
     * @param list A list of each option
     * @param selected the default option
     * @param defaultValue The string showned when nothing is selected
     * @return the select generated
     */
    public static writeSelect(title : string, list: string[], selected?: number, defaultValue?: string): string {
        if (typeof selected === "undefined") {
            selected = -1;
        }
        if (typeof defaultValue === "undefined") {
            defaultValue = "";
        }
        let i = 0;
        let s = "";
        s += "<select name="+title+"><option "+ (selected === -1 ? "selected " : "") +"value=\"-1\">"+ defaultValue +"</option>\n"
        list.forEach(function (element) {
            if (selected === i) {
                s += "<option selected value=\"" + i + "\">" + element + "</option>\n";
            }
            else {
                s += "<option value=\"" + i + "\">" + element + "</option>\n";
            }
            i++;
        })
        s+="</select>"
        return s;
    }

    /**
     * return the amount of time in the format hh:mm:ss between a timestamp and now
     */
    public static durationString(): string {
        const delta : number = Date.now() - NetGame.getNetGame().timeStart;
        return delta < 3600000 ? dayjs.duration(delta).format('mm:ss') : dayjs.duration(delta).format('HH:mm:ss');
    }

    /**
     * Return the player register at a specific layer, null if no player.
     * @param node
     * @param layer
     */
    public static getPlayer(node : number, layer : number): Player {
        if (node<0||node>NetGameInit.NODE_NAMES.length-1||layer<0||layer>NetGameInit.LAYER_NAMES.length-2) {
            return null;
        }
        return NetGame.getNetGame().Nodes[node].Layers[layer].player ?? null;
    }

    /**
     * Returns an array with all connected Players
     */
    public static getConnectedPlayer(): Player[] {
        return Array.from(PlayerServer.players.values());
    }

    /**
     * Returns an array with all connected Monitors
     */
    public static getConnectedMonitor(): Monitor[] {
        return Array.from(MonitorServer.monitors.values());
    }

    /**
     * Returns a specific message in the queue of a layer
     * @param node
     * @param layer
     * @param ndx
     */
    public static getQueuedMessage(node : number, layer : number, ndx : number): NetGameMsg {
        return NetGame.getNetGame().Nodes[node].Layers[layer].queue[ndx];
    }

    /**
     * Returns the messages in the buffer of a layer
     * @param node
     * @param ndx
     */
    public static getBufferedMessage(node : number, ndx : number): NetGameMsg {
        return NetGame.getNetGame().Nodes[node].Layers[Layers.DLL].buffered[ndx];
    }

    /**
     * return one instance of a message in the log
     * @param msgID
     * @param msgInstance
     */
    public static getMessageInstance(msgID : number, msgInstance : number): NetGameMsg {
        return NetGame.getNetGame().messageLogs[msgID].msgLog[msgInstance];
    }

    /**
     * Returns the Probability of error done by agents
     */
    public static getErrorProb(): number {
        return NetGame.getNetGame().AutoProcess.errorProbability;
    }

    /**
     * returns the SPF (shortest path first) matrix
     */
    public static getSPFMatrix(): number[][][] {
        return NetGame.getNetGame().connectivity.SPFMatrix;
    }

    /**
     * Returns the errors of a player, if node is played by an agent, return null
     * @param node
     * @param layer
     */
    public static getErrorStat(node : number, layer : number) {
        if (!AdminController.getPlayer(node,layer)) {
            return null;
        }
        return AdminController.getPlayer(node,layer).errors.Errors;
    }

    /**
     * Returns the Field Enum for usage in ejs files
     */
    public static getFieldsEnum() {
        return Fields;
    }

    /**
     * Returns the Layer Enum for usage in ejs files
     */
    public static getLayersEnum() {
        return Layers;
    }

    /**
     * Returns the Status Enum for usage in ejs files
     */
    public static getStatusEnum() {
        return Status;
    }

    /**
     * return the current message of a specific ID
     * @param msgID
     */
    public static getMessagewithID(msgID : number): NetGameMsg {
        return AdminController.getMessageInstance(msgID, NetGame.getNetGame().messageLogs[msgID].msgLog.length - 1);
    }

    /**
     * Used by cmd-msgall.esj to determine if a message must be shown in a table
     * @param msgID
     * @param table
     */
    public static checkIfMessageMustBeShowed(msgID : number, table : number): boolean {
        const msg = AdminController.getMessagewithID(msgID);
        return ((table==1) && (msg.status === Status.OK && !msg.isACKorNAK())) ||
            ((table==2) && (!msg.isACKorNAK() && msg.status !== Status.OK)) ||
            ((table==3) && (msg.isACKorNAK() && msg.status === Status.OK)) ||
            ((table==4) && (msg.isACKorNAK() && msg.status !== Status.OK));
    }

    public static getPHLErrorStatistics(): string {
        const errorCount = NetGame.getNetGame().AutoProcess.ErrorCount;
        const errorDecisionCount = NetGame.getNetGame().AutoProcess.ErrorDecisionCount;
        return (errorDecisionCount > 0 ? "" + Math.round(errorCount * 100 / errorDecisionCount) : "0")
            + "% (" + errorCount + "/" + errorDecisionCount + ")";
    }

    /**
     * return an array with every attribute from a player required to fill up an entry of cmd-websocket.ejs
     * @param player
     */
    public static getPlayerInfo(player : Player): any {
        const playerInfo: any = {node: '-', layer: '-'};
        if (player.layer >= 0 && player.node >= 0) {
            playerInfo.node = player.node;
            playerInfo.layer = player.layer;
        }
        playerInfo.alias = (player.alias == null ? "-" : player.alias);
        playerInfo.id = (player.id == -1 ? "-" : player.id);
        playerInfo.ip = (player.ipAddress == null ? "-" : player.ipAddress);
        return playerInfo;
    }

    /**
     * return an array with every attribute from a monitor to fill up an entry of cmd-websocket.ejs
     * @param monitor
     */
    public static getMonitorInfo(monitor : Monitor): any {
        return {
            id: (monitor.id == -1 ? "-" : monitor.id),
            role: (monitor.role == null ? "-" : monitor.role),
            ip: (monitor.ipAddress == null ? "-" : monitor.ipAddress)
        };
    }

    /**
     * Returns either an input or a select with proper values for launch and edit messages
     * @param index Which Field must it return
     * @param msgID For launch always -1, for edit the ID of the message edited
     * @param editField Boolean on whether each field can be edited
     * @param msgN For edit only, the instance of the message
     */
    public static writeHTMLInputField(index: number, msgID: number, editField: boolean[], msgN?: number): string {
        let msg;
        if (msgID<0) {
            msg = new NetGameMsg();
        } else {
            if (typeof msgN == "undefined") {
                msg = AdminController.getMessagewithID(msgID)
            } else {
                msg = AdminController.getMessageInstance(msgID, msgN);
            }
        }
        // field may be edited
        if (editField[index]) {
            // field value has to be entered manually
            if (MsgInit.msgFieldValues[index] == null) {
                return '<INPUT TYPE="text" SIZE="' + MsgInit.msgFieldSizes[index]
                    + '" NAME="msgfield'+index+'" VALUE="'
                    + msg.getHTMLFieldValue(index) + '">'
            }
            // field value may be selected from list
            else {
                return AdminController.writeSelect("msgfield"+index, MsgInit.msgFieldValues[index],
                    (msg.message.msgFields[index] === -1 ? null : msg.message.msgFields[index]));
            }
        }
        else {
            return "";
        }
    }

    /**
     * Write one of the entry of the SPF matrix for cmd-routing.ejs
     * @param arr the content of the entry
     */
    public static writeSPFEntry(arr : number[]): string {
        let out = "[";
        for (let i=0; i < arr.length; i++) {
            out += arr[i]+1;
            if (i+1 < arr.length)
                out += ",";
        }
        return out + "]";
    }

    /**
     * Determine the message path across the game network (to be display when inspecting a message)
     * @param msgID ID of the message we want to see the path
     * @returns an object holding path information and list of traversed nodes
     */
    public static getMessageHistory(msgID : number): any {
        return MessageHistory.getMessageHistory(msgID);
    }

    /**
     * Returns the MsgInit class for usage inside ejs files
     */
    public static getMsgInit(): MsgInit {
        return MsgInit;
    }

    /**
     * Returns a list of snapshot available
     */
    public static getSnapshotList() : string[] {
        const snapshotArray : string[] = [];
        if (!fs.existsSync("./dumps")) {
            fs.mkdirSync("./dumps")
        }
        fs.readdirSync("./dumps").forEach((filename: any) => {
            if (filename.slice(-5) == '.json')
                snapshotArray.push(filename.slice(0, -5));
        })
        return snapshotArray;
    }

    /**
     * Returns an object with the infos about a snapshot.
     * Infos available are course, class, and round
     * @param filename snapshot file to extract information
     * @returns object containing extracted snapshot information
     */
    public static getSnapshotInfos(filename: string): any {
        const snapshot = JSON.parse(fs.readFileSync("./dumps/" + filename + '.json').toString());
        return snapshot.info;
    }

    /**
     * Returns HTML select tag for course/class selection (snapshot info)
     * @param name which one of the select is written
     */
    public static writeSnapshotSelection(name : string) : string {
        let list: string[] = [];
        if (name === "class") {
            list = AdminController.classList;
        }
        else if (name === "course") {
            list = AdminController.courseList;
        }
        let string = '<select id="snapshot-' + name + '" name="' + name + '">\n';
        list.forEach(function(element) {
            string += '<option value="' + element + '">' + element + '</option>\n';
        })
        string += (name !=='course' ? '<option disabled selected value="0">-</option>\n' : '')+'</select>';
        return string;
    }

    /**
     * return the proper format to show in snapshot read date field
     * @param date the date in ms to format;
     */
    public static getDateFormated(date: number): string {
        return dayjs(date).format("YYYY-MM-DD_HH-mm");
    }

    /**
     * return the number corresponding to the current level of the logger, the higher the level (max fatal 6) the less logs shown
     */
    public static getLoggerLevel(): number {
        return logger.settings.minLevel;
    }
}