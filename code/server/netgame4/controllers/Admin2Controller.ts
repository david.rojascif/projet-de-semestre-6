// NetGame core
import { NetGame } from "../netgame/Netgame";
import { Admin } from "../netgame/network/Admin";
import { NetGameInit } from "../netgame/NetgameInit";
import { NetGameMsg } from "../netgame/messages/NetGameMsg";
import { Fields } from "../netgame/messages/enums/Fields";
import { Layers } from "../netgame/enums/Layers";
import { Player } from "../netgame/network/Player";

// logger
import { logger } from "../log";

// sockets
import { MonitorServer } from "../sockets/MonitorServer";
import { PlayerServer } from "../sockets/PlayerServer";
import { AdminServer } from "../sockets/AdminServer";

// admin messages
import { WsMessage, WsParam } from "../sockets/messages/WsMessage";
import { ChangeGameState } from "../sockets/messages/admin/ChangeGameState";
import { ChangeRegState } from "../sockets/messages/admin/ChangeRegState";
import { RegReset } from "../sockets/messages/admin/RegReset";
import { LaunchInitSet } from "../sockets/messages/admin/LaunchInitSet";
import { AgentsTrigger } from "../sockets/messages/admin/AgentsTrigger";
import { AutoProcessMsg } from "../sockets/messages/admin/AutoProcessMsg";


// monitor messages
import { AliasAllReset } from "../sockets/messages/monitor/AliasAllReset";
import { LayerUpdate } from "../sockets/messages/monitor/LayerUpdate";

// player messages
import { RegistrationReset } from "../sockets/messages/player/RegistrationReset";
import { ProcessMsgParam } from "../sockets/messages/player/From/ProcessMsg";

export class Admin2Controller {
    /**
         * Treat the different types of messages sent by the admins
         * @param sender : the admin that sent the message
         * @param message : the message that the admin has sent
         */
    public static dispatch(sender: Admin, message: WsMessage<WsParam>): void {
        let param;
        //logger.info(message.type);
        switch (message.type) {
            case ChangeGameState.name:
                param = (message as ChangeGameState).param;
                NetGame.getNetGame().gameRunning = param.gameRunning;
                param = (NetGame.getNetGame().gameRunning ? 'running' : 'suspended')
                logger.info("Game state changed to " + param);
                break;
            case ChangeRegState.name:
                param = (message as ChangeRegState).param;
                logger.info(param.regOpen);
                NetGame.getNetGame().isRegistrationOpen = param.regOpen;
                param = (NetGame.getNetGame().isRegistrationOpen ? 'reg open' : 'reg closed')
                logger.info("Reg state changed to " + param);
                break;
            case RegReset.name:
                param = (message as RegReset).param;
                if (param.all == true && typeof (param.node) == 'undefined' && typeof (param.layer) == 'undefined') {
                    Admin2Controller.regResetAll();
                    logger.info("Registration of all players was reset !");
                }
                break;
            case LaunchInitSet.name:
                param = (message as LaunchInitSet).param;
                Admin2Controller.launchInitSet(param.initSet);
                logger.info('Set ' + param.initSet + ' of initial messages has been sent.');
                break;
            case AgentsTrigger.name:
                //check if the game is running
                if (!NetGame.getNetGame().gameRunning) {
                    logger.error("Agents can't be triggered, the game is currently suspended !");
                    break;
                }
                param = (message as AgentsTrigger).param;

                if (param.repeated == false) {
                    const count = NetGame.getNetGame().ProcessTimer.processMessages(false);
                    logger.info("Trigger once: " + Math.floor(count / 10000) + " agent(s) activated, " + (count % 10000) + " message(s) processed.");
                }
                else {
                    Admin2Controller.changeInterval(param.time * 1000);
                }
                break;
            case AutoProcessMsg.name:
                if (!NetGame.getNetGame().gameRunning) {
                    logger.error("Agents can't be triggered, the game is currently suspended !");
                    break;
                }
                param = (message as AutoProcessMsg).param;
                logger.info('AutoProcessMsg arrivato: ' + param.msgID);

                // check if the message is called with msgID or nodeID and layerID
                if (param.msgID != null && param.nodeID == null && param.LayerID == null) {
                    for (let index = 0; index < param.msgID.length; index++) {
                        // retrieve the current node and layer of the message
                        let messageLog = NetGame.getNetGame().messageLogs[param.msgID[index]];
                        let lastMessageLog = messageLog.msgLog[messageLog.msgLog.length - 1];
                        let lastNode = lastMessageLog.toNode;
                        let lastLayer = lastMessageLog.toLayer;
                        Admin2Controller.processMessage(lastNode, lastLayer, param.msgID[index]);
                    }
                }
                // life saver
                else {
                    /*
                    TODO in the future
                    Admin2Controller.lifeSaver(param.nodeID, param.LayerID);
                    */
                }

                break;
            default:
                logger.error("Unknown message type : " + message);
            /* TODO in the future
            else if(typeof(param.all) == 'undefined' && typeof(param.node) != 'undefined' && typeof(param.layer) != 'undefined'){
                //Admin2Controller.clearReg(node,layer);
            }
            */
        }
    }

    /**
     * Resets registrations : removes registration of all players
     */
    private static regResetAll() {
        MonitorServer.broadcast(new AliasAllReset());
        PlayerServer.broadcast(new RegistrationReset());
        for (let i = 0; i < NetGameInit.NODE_NAMES.length; i++) {
            for (let j = 0; j < NetGameInit.LAYER_NAMES.length - 1; j++) {
                if (NetGame.getNetGame().Nodes[i].Layers[j].player != null) {
                    NetGame.getNetGame().clearPlayerRegistration(i, j);
                }
            }
        }
    }

    /**
     * Send one of the init sets (defined in NetGameInit) to the players.
     * Adds 2 messages on network layer of each node, message text is randomly set
     * There are two predefined sets available.
     */
    private static launchInitSet(type: number) {
        let set = 0;
        switch (type) {
            case 1:
                // Choose random set
                set = NetGameInit.setIndex++ % NetGameInit.firstSets.length;
                // Choose random starting quote
                const startquotes = Math.floor(Math.random() * NetGameInit.firstSets[set].length);

                // Add the messages in the node[from] in layer NL
                NetGameInit.firstSets[set].forEach(([from, to], idx) => {
                    const newMsg = new NetGameMsg();
                    newMsg.initLocationInfo(-1, -1, from, Layers.AL, from, Layers.NL);
                    newMsg.message.msgFields[Fields.FROM] = from;
                    newMsg.message.msgFields[Fields.TO] = to;
                    newMsg.message.msgString = NetGameInit.peanutsQuotes[set][(startquotes + idx) % NetGameInit.firstSets[set].length];

                    NetGame.getNetGame().addNewMsg(newMsg);
                    NetGame.getNetGame().Nodes[newMsg.toNode].Layers[newMsg.toLayer].addToQueue(newMsg);
                });
                break;
            case 2:
                // Choose random set
                set = NetGameInit.setIndex2 % NetGameInit.secondSets.length;

                // Add the messages in the node[from] in layer NL
                NetGameInit.secondSets[set].forEach(([from, to], idx) => {
                    const newMsg = new NetGameMsg();
                    newMsg.initLocationInfo(-1, -1, from, Layers.AL, from, Layers.NL);
                    newMsg.message.msgFields[Fields.FROM] = from;
                    newMsg.message.msgFields[Fields.TO] = to;
                    newMsg.message.msgString = NetGameInit.proverbs[NetGameInit.shuffle[idx] + NetGameInit.setIndex2 * NetGameInit.secondSets[set].length % NetGameInit.proverbs.length]

                    NetGame.getNetGame().addNewMsg(newMsg);
                    NetGame.getNetGame().Nodes[newMsg.toNode].Layers[newMsg.toLayer].addToQueue(newMsg);
                });
                NetGameInit.setIndex2++;
                NetGameInit.doShuffle();
                break;
            default:
                logger.error("Unknown init set type : " + type);
        }
    }

    /**
     * Change the interval between the automatic process of the messages
     * @param interval amount of time between process in ms. 0 means the process is turned off.
     */
    private static changeInterval(interval: number) {
        if (interval === 0) {
            NetGame.getNetGame().ProcessTimer.stop();
            logger.info("Timer canceled");
        }
        else {
            NetGame.getNetGame().ProcessTimer.newInterval(interval);
            logger.info("Agents trigger set every " + interval + " seconds");

        }
    }

    /**
     * Return the player register at a specific layer, null if no player.
     * @param node
     * @param layer
     */
    public static getPlayer(node: number, layer: number): Player {
        if (node < 0 || node > NetGameInit.NODE_NAMES.length - 1 || layer < 0 || layer > NetGameInit.LAYER_NAMES.length - 2) {
            return null;
        }
        return NetGame.getNetGame().Nodes[node].Layers[layer].player ?? null;
    }

    /**
         * Processes a message once and update server.
         * @param node
         * @param layer
         * @param id
         */
    private static processMessage(node: number, layer: number, id: number) {
        const param = new ProcessMsgParam();
        param.MsgId = id;
        param.changedValue = -1;
        NetGame.getNetGame().Nodes[node].Layers[layer].processMsg(null, param);
        MonitorServer.broadcast(new LayerUpdate(node, layer));
        logger.info("Message with ID=" + id + " of " + NetGameInit.NODE_NAMES[node] + "/" +
            NetGameInit.LAYER_SHORT_NAMES[layer] + " has been processed");
    }

    /**
     * Returns a specific message in the queue of a layer
     * @param node
     * @param layer
     * @param ndx
     */
    public static getQueuedMessage(node: number, layer: number, ndx: number): NetGameMsg {
        return NetGame.getNetGame().Nodes[node].Layers[layer].queue[ndx];
    }

    /**
     * Removes half of the message from a specific layer. Used to help a player that is too behind.
     * @param node
     * @param layer
     */
    /*
    private static lifeSaver(node: number, layer: number) {
        logger.info("Lifesaver: automatic processing for " + NetGameInit.NODE_NAMES[node] + "/"
            + NetGameInit.LAYER_SHORT_NAMES[layer]);
        const lockedMsgID = Admin2Controller.getPlayer(node, layer).lockedMsgID;
        const queueSize = NetGame.getNetGame().Nodes[node].Layers[layer].queue.length;
        if (queueSize < 2) return 0;

        let cntMsgsToProcess = queueSize / 2;
        const cnt = cntMsgsToProcess;
        let index = 0;
        while (cntMsgsToProcess > 0) {
            const tmp: NetGameMsg = Admin2Controller.getQueuedMessage(node, layer, index)
            if (tmp.message.ID === lockedMsgID) {
                index++;
            } else {
                Admin2Controller.processMessage(node, layer, tmp.message.ID);
                cntMsgsToProcess--;
            }
        }
        return cnt;
    }
    */

}