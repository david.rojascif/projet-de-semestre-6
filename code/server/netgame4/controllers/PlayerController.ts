// NetGame core
import { NetGame } from "../netgame/Netgame";
import { Layers } from "../netgame/enums/Layers";
import { Player } from "../netgame/network/Player";
// Monitor
import { MonitorServer } from "../sockets/MonitorServer";
// Messages
import { WsMessage, WsParam } from "../sockets/messages/WsMessage";
import { AliasReset } from "../sockets/messages/monitor/AliasReset";
import { AliasUpdate } from "../sockets/messages/monitor/AliasUpdate";
import { ClearRegister } from "../sockets/messages/player/From/ClearRegister";
import { LaunchMsg } from "../sockets/messages/player/From/LaunchMsg";
import { LayerUpdate } from "../sockets/messages/monitor/LayerUpdate";
import { Lock, LockParam } from "../sockets/messages/player/From/Lock";
import { PlayerState } from "../sockets/messages/player/PlayerState";
import { ProcessMsg } from "../sockets/messages/player/From/ProcessMsg";
import { Register } from "../sockets/messages/player/From/Register";
import { RegisterResult } from "../sockets/messages/player/RegisterResult";
import { RegistrationReset } from "../sockets/messages/player/RegistrationReset";
import { Unlock } from "../sockets/messages/player/From/Unlock";
// Logger
import { logger } from "../log";

/**
 * Controls communication between players and game.
 * Dispatches and handles websocket messages received from players.
 */
export class PlayerController {

    /**
     * Treat the different types of messages sent by the players
     * @param sender : the player that sent the message
     * @param message : the message that the player has sent
     */
    public static dispatch(sender: Player, message: WsMessage<WsParam>): void {
        switch (message.type) {
            case Register.name:
                const param = (message as Register).param;

                const res = NetGame.getNetGame().registerPlayer(param.Node, param.Layer, param.Forced, sender);

                const response = new RegisterResult(res);

                if (response.param.success) {
                    // Monitor AliasUpdate
                    const update = new AliasUpdate(param.Node, param.Layer, param.Alias);

                    sender.alias = param.Alias;
                    sender.node = param.Node;
                    sender.layer = param.Layer;

                    MonitorServer.broadcast(update);

                    if (param.Layer == Layers.NL) {
                        response.param.connectivity = NetGame.getNetGame().connectivityMatrix[param.Node];
                    }

                    // Player StateUpdate
                    sender.sendMessage(new PlayerState(param.Node, param.Layer, sender.ranking.scoreAvg));

                    // Monitor LayerUpdate
                    MonitorServer.broadcast(new LayerUpdate(param.Node, param.Layer));
                }

                // Player RegisterResult
                sender.sendMessage(response);
                break;
            case ClearRegister.name:
                if (sender.node == -1 || sender.layer == -1) return;

                // Save player position, because after clear will be (-1, -1)
                const oldnode = sender.node;
                const oldlayer = sender.layer;

                NetGame.getNetGame().clearPlayerRegistration(sender.node, sender.layer);

                // Monitor AliasReset
                MonitorServer.broadcast(new AliasReset(oldnode, oldlayer));

                // Player RegistrationReset
                sender.sendMessage(new RegistrationReset());
                break;

            case Lock.name:
                // No return message
                sender.lockedMsgID = (message.param as LockParam).MsgId;
                break;

            case Unlock.name:
                // No return message
                sender.lockedMsgID = -1;
                break;

            case ProcessMsg.name:
                // process the message
                NetGame.getNetGame().processMessage(sender, (message as ProcessMsg).param);

                // Monitor LayerUpdate
                MonitorServer.broadcast(new LayerUpdate(sender.node, sender.layer));

                // Player StateUpdate
                sender.sendMessage(new PlayerState(sender.node, sender.layer, sender.ranking.scoreAvg));
                break;

            case LaunchMsg.name: // TODO not implemented ??
                // Player StateUpdate
                // Monitor LayerUpdate
                //sender.sendMessage(new PlayerState());
                break;

            default:
                logger.error("Unknown message type : " + message);
        }
    }
}