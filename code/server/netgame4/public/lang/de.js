var lang = {
  gameVersion : "NetGame v4.0",
  gameInfo : "<b>NetGame - ein didaktisches Lernspiel<br>entwickelt von Dr. Rudolf Scheurer</b><br>Hochschule für Technik und Architektur, <br>Freiburg, Schweiz<br><br>Version Node.js:<br>Julien Torrent (HTA-FR),<br>Florian Chassot (HTA-FR)",
  regTitle : "<strong>Bitte registrieren Sie sich</strong>",
  regChoose : "Bitte wählen Sie die zu spielende <b>Station</b> und <b>Schicht</b> und ein <b>Alias</b>, welches am Netzwerk-Monitor angezeigt wird:",
  regStation : "Station",
  regLayer : "Schicht",
  regAlias : "Alias",
  regAliasPlaceholder : "Alias eingeben",
  regEnterAlias : "Bitte Alias eingeben!",
  regRegister : "<strong>Registrieren</strong>",
  regClosed : "<b>Registrierung momentan nicht möglich, bitte warten.</b>",
  regNoNewReg : "<b>Ändern der Registration ist im Moment nicht erlaubt!</b>",
  regClear : "<strong>Registrierung löschen</strong>",
  regCleared : "Ihre Registration ist vom Spielleiter gelöscht worden.",
  regRenew : "<br><b>Sie können sich jetzt neu registrieren.</b>",
  regError : "Registration scheiterte mit Fehlercode [{1}]:<br>",
  regErrorREG00 : "<b>NetGame läuft noch nicht! Bitte versuchen Sie es später erneut.</b>",
  regErrorREG01 : "<b>NetGame ist momentant unterbrochen! Bitte versuchen Sie es später erneut.</b>",
  regErrorREG02 : "<b>NetGame läuft, es zur Zeit keine Registration möglich.</b>",
  regErrorREG03 : "<b>Ungültige Kombination Station/Schicht.</b>",
  regErrorREG04 : "<b>Diese Kombination Station/Schicht ist bereits belegt.</b>",
  regSuccess : "<b>Ihre Registration war erfolgreich!</b><br>Warten Sie nun auf eingehende Rahmen ...",
  regPlaying : "Sie spielen <b>{1}</b> auf <b>{2}</b>",
  inputQueue : "Warteschlange",
  queueNoItems : "<p>Keine Einträge.</p>",
  bufferedMsgs : "Gespeicherte Rahmen",
  bufferNoItems : "<p>Keine Einträge.</p>",
  formTreatment : "Bearbeiten des Rahmens [{1}]",
  formDelivery : "Weitergabe-Optionen",
  formDlvrUp : "nach <b>OBEN</b> weiterleiten",
  formDlvrDiscard : "kein Weiterleiten (Rahmen wegwerfen)",
  formDlvrDown : "nach <b>UNTEN</b> weiterleiten ",
  formDlvrPHL : "seitwärts zur <b>PHL der nächsten Station</b>",
  formProcessing : "Verarbeitungs-Optionen",
  formProcNone : "Keine.",
  formProcSave : "eine Kopie des Rahmens speichern",
  formProcRemove : "den bestätigten Rahmen aus Speicher entfernen",
  formProcRetransmit : "entsprechenden gespeicherten Rahmen nochmals senden",
  formProcReply : "mit ACK oder NAK antworten",
  formRawMsg : "Rahmen als Rohdaten:",
  formValidate : "Validieren",
  formValSucc : "Validierung war erfolgreich, Sie können die Verarbeitung nun ausführen lassen ...",
  formSubmit : "Ausführen",
  formCancel : "Abbrechen",
  notifTitle : "Mitteilung",
  dispTitle : "Mitteilung",
  launchTitle : "Eine Nachricht absenden",
  launchIntro : "Von Zeit zu Zeit sind Sie eingeladen, selber eine Nachricht abzusenden:",
  launchError1 : "Bitte alle Felder ausfüllen",
  launchError2 : "Start und Ziel sollten nicht identisch sein",
  launchError3 : "Bitte nicht mehr als 4 Schrägstriche verwenden",
  launchError4 : "Die Nachricht darf nicht mehr als 50 Zeichen enthalten",
  dispSuspended : "Das Spiel wurde unterbrochen!<br>Bitte warten Sie auf Instruktionen ...",
  errorNoWS : "ERROR: Dieser Browser unterstützt leider keine WebSockets !",
  connLost : "Die Verbindung zum Server ging verloren!<br>Bitte laden Sie die Seite neu um wieder zu verbinden.",
  alertLeave : "Wollen Sie wirklich das Spiel verlassen?",
  gameReset : "<b>Das Spiel wurde neu gestartet!</b><br>Sie müssen sich neu registrieren ..."
}

function getMsgAndTip(errCode, argument) {
  intErrCode = parseInt(errCode);
  msg = "Interner Fehler: unbekannter Fehlercode [" + errCode + "] !";
  tip = "<b>Bitte informieren Sie den Spielleiter über dieses Problem!</b>";
  layer = Math.floor(intErrCode / 10000);
//  console.log(intErrCode);

  // error code system: "LDNxx" with L=layer(0=any), D=olddirection, N=newdirection, xx=seq.no.
  
  if (layer == 0) {
    // section = "General";
    switch (intErrCode) {
      case 00001:
        msg = "Sie sollten diesen Rahmen unbedingt weitergeben.";
        tip = "Bitte entscheiden Sie, wohin dieser Rahmen weitergegeben werden soll, er sollte nicht bei Ihnen enden.";
        break;
      case 00003:
        msg = "Bitte füllen Sie alle editierbaren Felder aus.";
        tip = "Scheinbar haben Sie mindestens ein Feld leer gelassen (siehe rote Markierung).";
        break;
      case 00004:
        msg = "Bitte füllen Sie den ACK/NAK-Rahmen vollständig aus.";
        tip = "Es macht keinen Sinn, mit einem unvollständigen ACK/NAK-Rahmen zu antworten.";
        break;
      case 99:
        msg = "Interner System-Fehler!";
        tip = "Bitte informieren Sie den Spielleiter über diesen Vorfall. ";
        break;
    }
  }

  else if (layer == 1) {
    // section = "Physical Layer (PHL)";
    switch (intErrCode) {
      case 11201:
        msg = "Dieser Rahmen sollte nicht an die Sicherungsschicht (DLL) zurückgeben werden.";
        tip = "Der Rahmen wurde eben von der Sicherungsschicht Ihrer Station an Sie übergeben, und jetzt<br>"
            + "sollten Sie diesen verarbeiten und an die physikalische Schicht der nächsten Station weitergeben.";
        break;
      case 11203:
        msg = "Die Rohdaten sind fehlerhaft, die Fehlerposition ist markiert:<br>" + argument;
        tip = "Offenbar haben Sie beim Kopieren der Felder in die Rohdatenform einen Fehler gemacht...";
        break;
      case 12301:
        msg = "Es macht keinen Sinn, den Rahmen mit den Rohdaten gleich wieder dahin zurückzusenden, woher Sie ihn bekommen haben!";
        tip = "Sie haben von der physikalischen Schicht (PHL) einer anderen Station einen Rahmen in Rohdaten-Form erhalten. "
            + "Statt ihn zurückzusenden sollten Sie erwägen, den Rahmen zu verarbeiten und ihn an Ihre Sicherungsschicht (DLL) "
            + "weiterzugeben.";
        break;
      case 12303:
        msg = "Der ausgefüllte Rahmen entspricht nicht den Rohdaten (Fehlerposition markiert):<br>" + argument;
        tip = "Einer der ausgefüllten Feldwerte stimmt nicht mit den entsprechenden Rohdaten überein.<br>"
            + "Bitte überprüfen Sie das angegebene Feld.";
        break;
    }
  }

  else if (layer == 2) {
    // section = " Data Link Layer (DLL)";
    switch (intErrCode) {
      case 21111:
        msg = "Dieser Rahmen sollte an die physikalische Schicht (PHL) weitergeben werden!";
        tip = "Der von Ihnen bearbeitete Rahmen kam von Ihrer Netzwerkschicht (NL) zu Ihnen.<br>"
            + "Sie sollten ihn also nach der Bearbeitung an Ihre Bitübertragungsschicht (PHL) weitergeben.";
        break;
      case 21112:
        msg = "Sie sollten eine Kopie dieses Rahmens speichern, für den Fall einer Sendewiederholung!";
        tip = "Sie sind dabei, eine DAT-Rahmen abzusenden. Falls dieser Rahmen fehlerhaft ankommt, wird "
            + "von Ihnen (mit einem NAK-Rahmen) eine Sendewiederholung verlangt werden. Also "
            + "sollten Sie von allen DAT-Rahmen eine Kopie behalten.";
        break;
      case 21113:
        msg = "Der Wert im Feld 'FCS' ist nicht korrekt!";
        tip = "Der Wert des Feldes 'FCS' gibt die Anzahl der sichtbaren Zeichen im Nachrichtentext an. "
            + "Der von Ihnen eingegebene Wert stimmt nicht mit dem vom NetGame-Programm berechnetet überein. "
            + "Vermutlich liegt der Fehler auf Ihrer Seite ...";
        break;
      case 21115:
        msg = "Gemäss den Aufzeichnungen sollten Sie jetzt die Sequenznummer <b>"
            + argument + "</b> benutzen!";
        tip = "Die allererste Sequenznummer können Sie zwar frei bestimmen, danach müssen Sie aber "
            + "konsequent mit den Sequenznummern weiterfahren (unabhängig von der Zielstation).";
        break;
      case 21116:
        msg = "Sollte dies nicht ein DAT-Rahmen sein? Bitte überprüfen Sie das Feld 'Type'.";
        tip = "Der Rahmen kam von Ihrer Vermittlungsschicht (NL). Also ist es ein Daten-Rahmen, und deshalb "
            + "sollten Sie den Rahmentyp auf <i>DAT</i> setzen.";
        break;
      case 21117:
        msg = "Überprüfen Sie den Wert im Feld 'Destination'!";
        tip = "Senden Sie diesen Rahmen an die nächste Station (auf dem Weg zur Zielstation). Deshalb sollte die "
            + "MAC-Adresse im Feld <i>'Destination'</i> der IP-Adresse im Feld <i>'Next Node'</i> entsprechen.";
        break;
      case 21118:
        msg = "Überprüfen Sie den Wert im Feld 'Source') ?";
        tip = "Da Sie der Absender des Rahmens sind, sollten Sie auch Ihre eigene MAC-Adresse im Feld "
            + "<i>'Source'</i> angeben.";
        break;
      case 21119:
        msg = "Es sollten (neben dem Speichern einer Kopie) keine anderen Verarbeitungsoptionen ausgewählt werden.";
        tip = "Es gibt keinen bestätigten Rahmen zu entfernen, keinen Rahmen zu wiederholen und keine Bestätigungen zu senden.";
        break;
      case 21120:
        msg = "Die Sequenznummer ist ungültig, bitte beginnen Sie mit einer Zahl zwischen 0 und 100";
        tip = "Die Start-Sequenznummer muss eine Zahl sein und zwischen 0 und 100.";
        break;
      case 23001:
        msg = "Es ist nicht erlaubt, die DLL-Felder eines erhaltenen Rahmens zu manipulieren.";
        tip = "Die DLL-Felder bleiben unverändert, hingegen sollten Sie je nach DLL-Daten über<br>"
            + "die Weiterleitung und die passenden Verarbeitungsoptionen entscheiden.";
        break;
      case 23011:
        msg = "Bitte erwägen Sie, eine Bestätigung (ACK oder NAK) für diesen Rahmen zu senden!";
        tip = "Sie haben einen Daten-Rahmen von Ihrer Bitübertragungsschicht (PHL) erhalten. Nun sollten Sie entscheiden,"
            + "ob dieser Rahmen korrekt angekommen ist oder nicht (Feld <i>'FCS'</i> überprüfen). "
            + "Bei einem korrekten DAT-Rahmen sollten Sie mit einem ACK antworten, ansonsten mit einem NAK.";
        break;
      case 23012:
        msg = "Sind nicht SIE der Absender dieses ACK/NAK-Rahmens? Überprüfen Sie das Feld 'Source'!";
        tip = "Die MAC-Adresse im Feld <i>'Source'</i> des ACK/NAK-Rahmens sollte Ihre eigene sein.";
        break;
      case 23013:
        msg = "Dieser ACK/NAK-Rahmen sollte an den Absender des DAT-Rahmens gesendet werden!";
        tip = "Das Feld 'Destination' sollte die MAC-Adresse jener Station enthalten, die Ihnen den DAT-Rahmen "
            + "gesendet hat, auf welchen Sie mit diesem ACK/NAK reagieren.";
        break;
      case 23014:
        msg = "Bitte überprüfen Sie den ausgewählten Rahmentyp (siehe Feld 'Type') für Ihre Antwort!";
        tip = "Da es sich um eine positive oder negative Bestätigung handelt, solle der Rahmentyp ACK oder NAK sein.";
        break;
      case 23015:
        msg = "Die Sequenznummer im ACK/NAK-Rahmen ist falsch!";
        tip = "Diese Sequenznummer sollte jener im DAT-Rahmen entsprechen, den Sie (negativ) bestätigen möchten.";
        break;
      case 23016:
        msg = "Es sollten (neben dem Senden eines ACK/NAK) keine anderen Verarbeitungsoptionen ausgewählt werden";
        tip = "Sie müssen den DAT-Rahmen nur bestätigen (positiv oder negativ). Es braucht keine andere Verarbeitungsoptionen.";
        break;
      case 23017:
        msg = "Ein Bestätigungs-Rahmen (ACK oder NAK) sollte nicht wieder nach unten weitergeleitet werden.";
        tip = "Auf positive/negative Bestätigung muss reagiert werden, aber der Bestätigungs-Rahmen selber endet hier.";
        break;
      case 23022:
        msg = "Bestätigungs-Rahmen (ACK/NAK) sollten nicht an die Bitübertragungsschicht (PHL) retourniert werden";
        tip = "Auf positive/negative Bestätigung muss reagiert werden, aber der Bestätigungs-Rahmen selber endet hier.";
        break;
      case 23211:
        msg = "Wenn Sie einen NAK-Rahmen senden wollen, dann sollten Sie den DAT-Rahmen nicht weitergeben!";
        tip = "Wenn der empfangene DAT-Rahmen fehlerhaft ist, dann ist das Senden eines NAK-Rahmens richtig, "
            + "aber der DAT-Rahmen sollte in diesem Fall nicht weiter gegeben werden.";
        break;
      case 23212:
        msg = "Antworten Sie mit einem NAK-Rahmen auf fehlerhafte DAT-Rahmen (falscher FCS-Wert)!";
        tip = "Sie haben einen DAT-Rahmen mit einem falschen FCS-Wert erhalten, also gilt der Rahmen als fehlerhaft. " +
          "Folglich sollten Sie eine negative Bestätigung (NAK-Rahmen) an den Absender des DAT-Rahmens senden.";
        break;
      case 23221:
        msg = "Der bestätigte Rahmen sollte aus dem Zwischenspeicher entfernt werden!";
        tip = "Sie haben einen Bestätigungsrahmen (ACK) erhalten. Das heisst, ein von Ihnen versendeter Datenrahmen ist "
            + "korrekt bei der nächsten Station angekommen. Sie können nun also die gespeicherten Kopie des Datenrahmens "
            + "wieder löschen.";
        break;
      case 23222:
        msg = "Rahmen, die keine Nutzdaten transportieren, werden nicht an die Vermittlungsschicht (NL) weitergegeben!";
        tip = "Auf positive/negative Bestätigung muss reagiert werden, aber der Bestätigungs-Rahmen selber endet hier.";
        break;
      case 23223:
        msg = "Sie brauchen nur die gespeicherte Kopie des bestätigten Datenrahmens zu löschen!";
        tip = "Es sind keine weiteren Verarbeitungsoptionen nötig.";
        break;
      case 23224:
        msg = "Sie brauchen nur den negativ bestätigten Datenrahmen nochmals zu senden!";
        tip = "Es sind keine weiteren Verarbeitungsoptionen nötig.";
        break;
      case 23231:
        msg = "Überlegen Sie sich, den entsprechenden Datenrahmen nochmals zu senden!";
        tip = "Sie haben einen NAK-Rahmen erhalten, d.h. ein von Ihnen versendeter Datenrahmen ist fehlerhaft angekommen. " +
          "Deshalb sollten Sie den entsprechenden Datenrahmen wiederholen, eine Kopie des Rahmens sollte ja noch verfügbar sein.";
        break;
      // case 23232:
      // msg = "Since it is not a DAT frame consider discarding it!";
      // tip = "Only DAT frames should be passed up to the NL. ACK and NAK frames are for "
      // + "DLL use only, thus they end on DLL level.";
      // break;
      case 23311:
        msg = "Der empfangene Datenrahmen ist korrekt und sollte deshalb bestätigt werden!";
        tip = "Der von Ihnen empfangene Datenrahmen hat einen korrekten FCS-Wert, d.h. er ist korrekt übertragen worden. "
            + "Deshalb sollten Sie einen ACK-Rahmen an den Absender zurücksenden, um den korrekten Empfang zu bestätigen";
        break;
      case 23312:
        msg = "Wenn Sie den Rahmen schon bestätigen, dann sollten Sie ihn auch an die Vermittlungsschicht (NL) weitergeben!";
        tip = "Sie haben einen Datenrahmen empfangen, welcher fehlerfrei übertragen wurde. Sie haben entschieden, eine "
            + "Bestätigung (ACK) zu schicken. Folglich sollten Sie den Datenrahmen an die Vermittlungsschicht weitergeben.";
        break;
      case 23313:
        msg = "Ändern Sie nichts am Rahmen, wenn Sie ihn an die Vermittlungsschicht (NL) weitergeben wollen!";
        tip = "Sie haben einen korrekten Datenrahmen empfangen. Wenn Sie diesen an die Vermittlungsschicht weitergeben wollen, " +
          "dann dürfen Sie keine Änderungen daran vornehmen.";
        break;
    }
  }

  else if (layer == 3) {
    // section = "Network Layer (NL)";
    switch (intErrCode) {
      case 30111:
        msg = "Bitte überprüfen Sie Ihre Routing-Entscheidung (Wahl des <i>'Next Node'</i>) !";
        tip = "Von Ihrer Station führt keine direkte Verbindung zu der von Ihnen angegeben nächsten Station ('Next Node'). "
            + "Bitte wählen Sie eine andere nächste Station ('Next Node') auf dem Weg zur endgültigen Zielstation ('Final Node'). ";
        break;
      case 31111:
        msg = "Senden Sie diese Paket nicht zurück an die Anwendungsschicht <i>('AL')</i>!";
        tip = "Das von Ihnen bearbeitete Paket kam von der Anwendungsschicht.<br>Es macht keinen Sinn, es direkt dorthin zurückzugeben";
        break;
      // case 31112:
      // msg = "Don't be lazy, please fill in both fields of your layer!";
      // tip = "There's not much to do on your layer. But still you have to do it ...<br>"
      // + "Check the network topology and decide about the routing. In the field "
      // + "<i>final node</i> you have to fill in the NL address of the destination, "
      // + "in the field <i>next node</i> you have to fill in the next node on the "
      // + "route to the final destination.";
      // break;
      case 31113:
        msg = "Das Feld <i>'Next Node'</i> deutet auf ihre eigene Station !?! Komisches Routing ...";
        tip = "Entscheiden Sie anhand des Netzplans, welches die nächste Station auf dem Weg zur Zielstation "
            + "sein soll und geben Sie diese Adresse (nicht Ihre eigene) im Feld <i>Next Node</i> an.";
        break;
      case 31114:
        msg = "Bitte überprüfen Sie die Eingabe für das Feld <i>'Final Node'</i>.";
        tip = "Sie sollten in diesem Feld die Adresse der Zielstation eingeben<br>(vergleichen Sie mit dem Feld <i>'To'</i>).";
        break;
      // case 33011:
      // msg = "Das 'Next Node' indicates your own node (funny routing ...)!";
      // tip = "Check the network topology and decide about the next node on the route "
      // + "to the final destination. Put the NL address of this node in the field "
      // + "<i>next node</i> (but not your own address!).";
      // break;
      case 33012:
        msg = "Bitte überprüfen Sie die Eingabe für das Feld <i>'Next Node'</i>.";
        tip = "Wollen Sie dieses Paket wirklich an jene Station weiterleiten, von welcher Sie es<br>"
            + "eben bekommen haben? Das ist keine gute Routing-Strategie ... ;-)";
        break;
      case 33013:
        msg = "Warum möchten Sie den Wert im Feld <i>'Final Node'</i> ändern?";
        tip = "Eigentlich sollte Ihre Routing-Entscheidung nur das Feld <i>'Next Node'</i> betreffen, "
            + "nicht aber das Feld <i>'Final Node'</i> (muss unverändert bleiben)!";
        break;
      case 33014:
        msg = "Warum den Wert im Feld <i>'Next Node'</i> ändern?";
        tip = "Hat das Paket seine Zielstation erreicht, dann sollte das Feld <i>'Next Node'</i> unverändert bleiben.";
        break;
      case 33111:
        msg = "Bitte erwägen Sie ein erneutes Routing dieses Paketes (Zielstation ist noch nicht erreicht) !";
        tip = "Da die Zielstation noch nicht erreicht ist, sollten Sie dieses Paket neu routen,<br>"
            + "d.h. an die nächste Station (<i>'Next Node'</i>) auf dem Weg zur Zielstation routen und das " +
              "Paket an die DLL-Schicht übergeben.";
        break;
      case 33311:
        msg = "Erwägen Sie die Übergabe an die Anwendungsschicht (<i>'AL'</i>) !";
        tip = "Die Adresse im Feld <i>'Final Node'</i> ist die Ihre. Also ist dieses Paket an seinem "
            + "Ziel angekommen und sollte an die Anwendungsschicht übergeben werden.";
        break;
      case 33312:
        msg = "Ändern Sie keine Felder der Netzwerkschicht <i>('NL')</i>, wenn Sie Daten-Pakete an die Anwendungsschicht <i>('AL')</i> weitergeben!";
        tip = "Daten-Pakete, die an der Zielstation angekommen sind, sollten unverändert an die Anwendungsschicht weitergegeben werden!";
        break;
    }
  }
  return msg; //[msg, tip];
}

