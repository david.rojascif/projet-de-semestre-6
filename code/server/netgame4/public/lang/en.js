var lang = {
  gameVersion : "NetGame v4.0",
  gameInfo : "<b>NetGame - a didactic game developed<br> by Dr. Rudolf Scheurer</b><br>University of Applied Sciences,<br>Fribourg, Switzerland<br><br>Version Node.js:<br>Julien Torrent (HEIA-FR),<br>Florian Chassot (HEIA-FR)",
  regTitle : "<strong>Please register</strong>",
  regChoose : "Please choose <b>station</b> and <b>layer</b> to play and an <b>alias</b> to be displayed on the network monitor:",
  regStation : "Station",
  regLayer : "Layer",
  regAlias : "Alias",
  regAliasPlaceholder : "Enter your alias",
  regEnterAlias : "Please enter your alias!",
  regRegister : "<strong>Register</strong>",
  regClosed : "<b>Registration is not (yet) possible at the moment, please wait.</b>",
  regNoNewReg : "<b>You may not change your registration at the moment!</b>",
  regClear : "<strong>Clear Registration</strong>",
  regCleared : "Registration was cleared by game master.",
  regRenew : "<br><b>You may now make a new registration.</b>",
  regError : "Registration failed with code [{1}]:<br>",
  regErrorREG00 : "<b>NetGame is not running yet! Please try again later.</b>",
  regErrorREG01 : "<b>NetGame is suspended! Please try again later.</b>",
  regErrorREG02 : "<b>NetGame is running, registration is not possible at the moment.</b>",
  regErrorREG03 : "<b>Invalid node/layer values.</b>",
  regErrorREG04 : "<b>This node/layer combination is already in use.</b>",
  regSuccess : "<b>Registration succeeded!</b><br>Now wait for incoming frames to treat ...",
  regPlaying : "Playing <b>{1}</b> at <b>{2}</b>",
  inputQueue : "Input Queue",
  queueNoItems : "<p>No items.</p>",
  bufferedMsgs : "Buffered Frames",
  bufferNoItems : "<p>No items.</p>",
  formTreatment : "Treatment of frame [{1}]",
  formDelivery : "Delivery Options",
  formDlvrUp : " deliver <b>UP</b>",
  formDlvrDiscard : "no delivery (discard frame)",
  formDlvrDown : "deliver <b>DOWN</b>",
  formDlvrPHL : "deliver to <b>PHL of next node</b>",
  formProcessing : "Processing Options",
  formProcNone : "None.",
  formProcSave : "save a copy of this frame",
  formProcRemove : "remove confirmed frame from buffer",
  formProcRetransmit : "retransmit requested frame",
  formProcReply : "reply with ACK or NAK",
  formRawMsg : "Raw Frame:",
  formValidate : "Validate",
  formValSucc : "Validation succeeded, you may now press submit ...",
  formSubmit : "Submit",
  formCancel : "Cancel",
  notifTitle : "Notification",
  dispTitle : "Notification",
  launchTitle : "Launch a message",
  launchIntro : "Every now and then you are invited to launch your own message:",
  launchError1 : "Please fill in all fields",
  launchError2 : "Source and destination must differ",
  launchError3 : "Please don't use more than 4 slashes",
  launchError4 : "The message may not exceed 50 charachters",
  dispSuspended :"Game has been suspended!<br>Please wait on instructions ...",
  errorNoWS : "ERROR: WebSockets are not supported by this browser!",
  connLost : "Connection to server has been lost!<br>Please reload page if you want to reconnect.",
  alertLeave : "Are you sure you want to leave the game?",
  gameReset : "<b>The game has been restarted!</b><br>You have start over with a new registration ..."
}

function getMsgAndTip(errCode, argument) {
  intErrCode = parseInt(errCode);
  msg = "Internal Error: unknown error code [" + errCode + "] !";
  tip = "<b>Please contact the game master and tell him about this problem!</b>";
  layer = Math.floor(intErrCode / 10000);
//  console.log(intErrCode);

  // error code system: "LDNxx" with L=layer(0=any), D=olddirection, N=newdirection, xx=seq.no.

  if (layer == 0) {
    // section = "General";
    switch (intErrCode) {
      case 00001:
        msg = "You should not discard this frame.";
        tip = "You should decide about where to pass the frame you are processing. Select an appropriate delivery option.";
        break;
      case 00003:
        msg = "Please fill in all editable fields.";
        tip = "There seems to be at least one empty field (see red markup).";
        break;
      case 00004:
        msg = "Please fill in the ACK/NAK frame.";
        tip = "It does not make sense to return an empty ACK/NAK frame.";
        break;
      case 99:
        msg = "Internal error!";
        tip = "Please tell the game supervisor about this. ";
        break;
    }
  }

  else if (layer == 1) {
    // section = "Physical Layer (PHL)";
    switch (intErrCode) {
      case 11201:
        msg = "Why do you want to return the frame to your DLL?";
        tip = "Since you received the frame from your station's DLL you are supposed to process it and hand it "
            + "over to the destination's PHL.";
        break;
      case 11203:
        msg = "Raw frame contains errors, error position is indicated below:<br>" + argument;
        tip = "It seems you made a mistake copying the field values of the frame form into the raw frame form.";
        break;
      case 12301:
        msg = "Does it make sense to send back the raw frame to where you've got it from?";
        tip = "You received a frame in raw format from another station's PHL. Instead of sending it back you  should consider to process the frame, ie. to fill it in the appropriate fields of the frame form and to hand it over to your station's DLL.";
        break;
      case 12303:
        msg = "The filled form does not correspond to the raw frame you received (error position marked):<br>"+ argument;
        tip = "Some field values do no match the corresponding ones in the raw frame. Please verify each field.<br>Maybe you made a mistake related to the <i>inverse character stuffing algorithm</i>.";
        break;
    }
  }

  else if (layer == 2) {
    // section = " Data Link Layer (DLL)";
    switch (intErrCode) {
      case 21111:
        msg = "Consider passing this frame down to the PHL!";
        tip = "The frame you are processing came down from your node's NL. Therefore you should consider passing "
            + "this frame to your PHL after you made your processing work.";
        break;
      case 21112:
        msg = "Consider saving a copy of this frame for later retransmission requests!";
        tip = "Your are about to send a frame via the PHL to a destination DLL. If the frame "
            + "happens to arrive with an error, the destination DLL will return a NAK frame telling you "
            + "to resend the corresponding frame. In order to be able to do that you should therefore "
            + "save a copy of all DAT frames you send out via your PHL.";
        break;
      case 21113:
        msg = "Reconsider the value of the 'FCS' field (be sure to ignore spaces when counting characters) !";
        tip = "The FCS reflects the number of characters in the message string, but without counting "
            + "blanks or spaces. The FCS you entered does not correspond to the one computed by "
            + "the NetGame program. Most probably the mistake is on your side ...";
        break;
      case 21115:
        msg = "According to my notes you should use the seqence number <b>" + argument
            + "</b> now!";
        tip = "Since the last sequence number you used has been <i>N</i> you are expected to use "
            + "<i>N+1</i> for the current data frame now.";
        break;
      case 21116:
        msg = "Isn't it a data frame that you want to send (verify field 'Type')?";
        tip = "The message you are treating came from your NL. Thus it is a data frame, and therefore you should set the type of the frame to <i>DAT</i>.";
        break;
      case 21117:
        msg = "Verify the settings of the field 'Destination'!";
        tip = "The <i>destination</i> field, i.e. the MAC address indicated there, should correspond "
            + "to the IP address of the field <i>next node</i>.";
        break;
      case 21118:
        msg = "I'm a little bit confused: aren't you the sender of this frame (verify field 'Source') ?";
        tip = "Since it's you who is sending out this frame you should fill in your MAC address in "
            + "the <i>source</i> field.";
        break;
      case 21119:
        msg = "You should not select any other processing option (besides \"save a copy ...\")";
        tip = "There's no confirmed frame to remove, no frame to retransmit, and no ACK or NAK to send.";
        break;
      case 21120:
        msg = "The sequence number is not valid, please start with a number between 0 and 100.";
        tip = "Start with a sequence number between 0 and 100.";
        break;
      case 23001:
        msg = "It is not allowed to change any DLL field of incoming frames.";
        tip = "DLL fields must remain unmodified, however you should decide about<br>"
            + "the correct delivery and processing options.";
        break;
      case 23011:
        msg = "Consider sending an acknowledge (ACK or NAK) for this frame!";
        tip = "A data frame has been handed over from your node's PHL. Now you should verify if this frame is correct or not by checking the <i>FCS</i> field. If the value seems ok, then you should return an acknowledgement frame (ACK), otherwise a negative acknowledgement (NAK).";
        break;
      case 23012:
        msg = "YOU are the source of this ACK/NAK frame (reconsider the value of the field 'Source')!";
        tip = "The address in the field <i>Source</i> should be yours.";
        break;
      case 23013:
        msg = "This ACK/NAK frame should be returned to the sender of the DAT frame!";
        tip = "You are the sender of this ACK/NAK frame, and the destination is the sender of the "
            + "DAT frame you are reacting to with this ACK/NAK.";
        break;
      case 23014:
        msg = "Please set the appropriate frame type (verify field 'Type') for your reply!";
        tip = "Choose either ACK or NAK as the frame type.";
        break;
      case 23015:
        msg = "You should use the correct sequence number for your ACK/NAK frame!";
        tip = "The sequence number in your ACK/NAK frame should be the same as the one of the DAT "
            + "frame you want to (negatively) acknowledge.";
        break;
      case 23016:
        msg = "You should not select other processing options (besides \"reply with ACK/NAK\"!";
        tip = "You have to reply with an ACK or NAK frame, depending on the correctness of the FCS field. "
            + "No other processing option is needed.";
        break;
      case 23017:
        msg = "Confirmation frames (ACK or NAK) should not be delivered back down.";
        tip = "Positive/negative confirmations demand an action, but the conirmation frame itself should end here.";
        break;
      case 23022:
        msg = "You received an ACK or NAK frame, you should not return it back to PHL!";
        tip = "You received an ACK or NAK frame. Independently of your reaction (removing "
            + "the acknowledged frame of resending the negatively acknowledged frame), "
            + "the incoming frame should be discarded now.";
        break;
      case 23211:
        msg = "If you decided to reply with a NAK, consider discarding the DAT frame!";
        tip = "A data frame has been handed over from your node's PHL. You have detected an "
            + "error and are intending to return a NAK frame. Thats ok so far. But, what's happening "
            + "with the data frame you received? Since it is not correct, consider discarding it (it "
            + "has to be resent by the sender).";
        break;
      case 23212:
        msg = "You should return a NAK frame for DAT frames with a transmission error (wrong FCS value)!";
        tip = "A data frame has been handed over from your node's PHL. The FCS value does not "
            + "correspond to the number of non-space characters in the message string. Thus "
            + "this data frame should be considered to be erroneous. You should return a negative "
            + "acknowledgement (NAK frame) to the sender of the DAT frame.";
        break;
      case 23221:
        msg = "Consider removing the acknowledged frame from your buffer!";
        tip = "An acknowledgement frame has been handed over from your node's PHL. This "
            + "indicates that the data frame you sent has successfully reached its destination. "
            + "Thus you can remove the original data frame from the buffer, you won't use "
            + "it anymore.";
        break;
      case 23222:
        msg = "This it is not a DAT frame, thus consider discarding it!";
        tip = "You received an ACK or NAK frame. Independently of your reaction (removing "
            + "the acknowledged frame of resending the negatively acknowledged frame), "
            + "the incoming frame should be discarded now.";
        break;
      case 23223:
        msg = "You just have to remove the saved copy of the frame that has been acknowledged!";
        tip = "You don't need to select any other processing option.";
        break;
      case 23224:
        msg = "You just have to retransmit the frame that has been negatively acknowledged!";
        tip = "You don't need to select any other processing option.";
        break;
      case 23231:
        msg = "Consider resending the original data frame (a copy of it should be in your buffer) !";
        tip = "You received a NAK frame indicating that the data frame with the given sequence number "
            + "did not successfully reach its destination. Thus you should resend the original "
            + "data frame using the copy you saved in your buffer.";
        break;
      case 23311:
        msg = "Return an ACK frame for received DAT frames that are free of errors (correct FCS value)!";
        tip = "The data frame you received seems to be error-free (the FCS is correct). Thus "
            + "you should reply with an ACK frame to acknowledge/confirm the receipt of the "
            + "data frame to the sending DLL.";
        break;
      case 23312:
        msg = "If you decided to reply with a ACK, consider passing the DAT frame to the NL!";
        tip = "You received a data frame that seems to be error-free. You decided to confirm "
            + "the receipt with an ACK frame. Since the data frame is ok, you should hand it "
            + "over to your node's NL for further processing.";
        break;
      case 23313:
        msg = "Do not change any DLL fields if passing the DAT frame to the NL!";
        tip = "You received an error-free data frame. When handing it over to the NL "
            + "you should not modify any field values!";
        break;
    }
  }

  else if (layer == 3) {
    // section = "Network Layer (NL)";
    switch (intErrCode) {
      case 30111:
        msg = "Please reconsider your routing decision (choice of <i>next node</i>) !";
        tip = "From your node there is no direct route to the <i>next node</i> you specified.<br> "
            + "Please choose another node as the <i>next node</i> on the route to the final destination. ";
        break;
      case 31111:
        msg = "Don't return the message to your AL layer!";
        tip = "The message you are processing came from the AL layer.<br>"
            + "Thus it makes no sense to send it back. ";
        break;
      case 31113:
        msg = "Field 'Next Node' indicates your own node !?!";
        tip = "Check the network topology and decide about the next node on the route "
            + "to the final destination. Put the NL address of this node in the field "
            + "<i>next node</i> (but not your own address!).";
        break;
      case 31114:
        msg = "Please verify your settings in the field 'Final Node'.";
        tip = "The value of the field <i>final node</i> should be set to the "
            + "NL address of the final destination (refer to the <i>To</i> field).";
        break;
      case 33012:
        msg = "Please verify the settings in the field 'Next Node'!";
        tip = "You specified the next node to be the one you got the message from, "
            + "ie. you are returning the message back to the previous node. This is not a good routing "
            + "practice ... ;-)";
        break;
      case 33013:
        msg = "Why do you want to change the value in field 'Final Node'?";
        tip = "It seems that you changed to value of the field <i>Final Node</i>, but "
            + "there is no reason to do that. The routing decision you have to take is "
            + "related to the field <i>Next Node</i> only!";
        break;
      case 33014:
        msg = "Why did you change the value of <i>'Next Node'</i> ?";
        tip = "If the final node has been reached, then the value of <i>'Next Node'</i> should not be changed.";
        break;
      case 33111:
        msg = "Consider rerouting this packet (final node not yet reached) !";
        tip = "Since the final node has not yet been reached, you should decide "
            + "about the next node on the route to the final destination.";
        break;
      case 33311:
        msg = "Consider delivery to AL (final node seems to be reached) !";
        tip = "The address in the field <i>Final Node</i> indicates your node. "
            + "Therefore the final destination for this message has been reached and "
            + "you should pass it over to your AL. ";
        break;
      case 33312:
        msg = "Do not change any NL fields if passing the DAT frame to the AL!";
        tip = "You received a data frame that is addressed to you. When handing it over to the AL "
            + "you should not modify any field values!";
        break;
    }
  }
  return msg; //[msg, tip];
}

