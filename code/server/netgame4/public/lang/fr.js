var lang = {
  gameVersion : "NetGame v4",
  gameInfo : "<b>NetGame - un jeu didactique développé<br>par Dr. Rudolf Scheurer</b><br>Haute école d'ingénierie et d'architecture, <br>Fribourg, Suisse<br><br>Version Node.js:<br>Julien Torrent (HEIA-FR),<br>Florian Chassot (HEIA-FR)",
  regTitle : "<strong>Prière de vous enregistrer</strong>",
  regChoose : "Choisissez une <b>station</b> et une <b>couche</b> pour jouer, et un <b>alias</b> à afficher sur le moniteur:",
  regStation : "Station",
  regLayer : "Couche",
  regAlias : "Alias",
  regAliasPlaceholder : "Saisir un alias",
  regEnterAlias : "Saisir un alias s.v.p.!",
  regRegister : "<strong>Enregistrement</strong>",
  regClosed : "<b>Pas d'enregistrement possible pour le moment, prière d'attendre.</b>",
  regNoNewReg : "<b>Un changement de l'enregistrement n'est pas possible pour le moment !</b>",
  regClear : "<strong>Annuler l'enregistrement</strong>",
  regCleared : "Votre enregistrement à été annulé par l'administrateur.",
  regRenew : "<br><b>Vous pouvez maintenant refaire votre enregistrement.</b>",
  regError : "L'enregistrement a échoué avec le code [{1}]:<br>",
  regErrorREG00 : "<b>NetGame ne tourne pas encore! Veuillez réessayer plus tard s.v.p.</b>",
  regErrorREG01 : "<b>NetGame est suspendu! Veuillez réessayer plus tard s.v.p.</b>",
  regErrorREG02 : "<b>NetGame est en cours, les enregistrement ne sont pas possible.</b>",
  regErrorREG03 : "<b>Combinaison station/couche invalide.</b>",
  regErrorREG04 : "<b>Cette combinaison station/couche est déjà prise.</b>",
  regSuccess : "<b>Votre enregistrement a réussi!</b><br>Veuillez attendre des trames entrant ...",
  regPlaying : "Vous jouez <b>{1}</b> de la station <b>{2}</b>",
  inputQueue : "Queue d'attente",
  queueNoItems : "<p>Aucune entrée.</p>",
  bufferedMsgs : "Trames sauvegardées",
  bufferNoItems : "<p>Aucune entrée.</p>",
  formTreatment : "Traitement de la trame [{1}]",
  formDelivery : "Options de livraison",
  formDlvrUp : "délivrer vers le <b>HAUT</b>",
  formDlvrDiscard : "pas de livraison (éliminer la trame)",
  formDlvrDown : "délivrer vers le <b>BAS</b>",
  formDlvrPHL : "délivrer vers <b>PHL du prochain noeux</b>",
  formProcessing : "Options de traitement",
  formProcNone : "Aucune.",
  formProcSave : "stocker une copie de cette trame",
  formProcRemove : "supprimer la trame confirmée",
  formProcRetransmit : "retransmettre la trame correspondante",
  formProcReply : "répondre avec un ACK ou NAK",
  formRawMsg : "Trame en forme brute:",
  formValidate : "Valider",
  formValSucc : "Validation réussi, vous pouvez alors lancer le traitement ...",
  formSubmit : "Exécuter",
  formCancel : "Annuler",
  notifTitle : "Notification",
  dispTitle : "Notification",
  launchTitle : "Lancer un message",
  launchIntro : "De temps en temps on vous invite à lancer votre propre message :",
  launchError1 : "Saisir tous les champs s.v.p",
  launchError2 : "Source et destination sont identiques",
  launchError3 : "Ne pas utiliser plus que 4 slashs",
  launchError4 : "Le message ne doit pas dépasser 50 caractères",
  dispSuspended : "NetGame est suspendu!<br>Prière d'attendre des instructions ...",
  errorNoWS : "ERROR: ce navigateur ne supporte pas les WebSockets !",
  connLost : "La connexion au serveur a été perdue!<br>Veuillez recharger cette page pour vous reconnecter.",
  alertLeave : "Voulez-vous vraiment quitter le jeu?",
  gameReset : "<b>Le jeu a été redémmarré (reset)!</b><br>Veuillez vous enregistrer à nouveau ..."
}

function getMsgAndTip(errCode, argument) {
  intErrCode = parseInt(errCode);
  msg = "Erreur interne: code d'erreur inconnu [" + errCode + "] !";
  tip = "<b>Veuillez informer l'administrateur du jeu!</b>";
  layer = Math.floor(intErrCode / 10000);
//  console.log(intErrCode);

  // error code system: "LDNxx" with L=layer(0=any), D=olddirection, N=newdirection, xx=seq.no.
  
  if (layer == 0) {
    // section = "General";
    switch (intErrCode) {
      case 00001:
        msg = "Vous devriez absolument delivrer cette trame plus loin.";
        tip = "Délibérez sur le livraision de ce message, il ne devrait pas finir chez vous.";
        break;
      case 00003:
        msg = "Saisissez tous les champs editables, svp.";
        tip = "Il semblerait qu'au moins un des champs a été laissé vide (voir marque rouge).";
        break;
      case 00004:
        msg = "Saisissez la trame ACK/NAK de manière complète.";
        tip = "C'est inutile de repondre avec une trame ACK/NAK incomplet.";
        break;
      case 99:
        msg = "Erreur interne du système!";
        tip = "Informez svp le superviseur du jeu de ce probleme!";
        break;
    }
  }

  else if (layer == 1) {
    // section = "Physical Layer (PHL)";
    switch (intErrCode) {
      case 11201:
        msg = "Cette trame ne devrait pas être redonné à la couche DLL.";
        tip = "Vous venez de recevoir cette trame depuis la couche DLL. Vous devriez<br>"
            + "maintenant la traiter et la délivrer à la couche PHL de la prochaine station.";
        break;
      case 11203:
        msg = "Les données brutes sont erronées (voir position indiquée):<br>" + argument;
        tip = "Apparemment vous avez fait une faute lors de la copie des champs dans leur forme brute.";
        break;
      case 12301:
        msg = "Cela ne fait pas de sens de retourner cette trame vers la couche depuis laquelle vous l'aviez reçu!";
        tip = "Vous avez reçu une trame (dans la forme brute) de la couche PHL d'une autre station. "
            + "Au lieu de la retourner vous devriez considérer de la traiter et de la passer à la couche DLL.";
        break;
      case 12303:
        msg = "Les champs remplis ne correspondent pas aux données brutes (voir position indiquée):<br>" + argument;
        tip = "Un des champs remplis n'est pas équivalent aux données brutes correspondantes.";
        break;
    }
  }

  else if (layer == 2) {
    // section = " Data Link Layer (DLL)";
    switch (intErrCode) {
      case 21111:
        msg = "Cette trame devrait être delivré à la couche physique (PHL) !";
        tip = "Cette trame vous est parvenu depuis votre couche réseau (NL).<br>"
            + "Après traitement, vous devriez donc la passer à la couche physique (PHL) de votre station.";
        break;
      case 21112:
        msg = "Vous devriez sauvegarder une copie de cette trame (en cas de retransmission)!";
        tip = "Vous êtes en train d'envoyer uen trame DAT. Si jamais cette trame arrive avec des erreurs, "
            + "le récepteur va vous retourner une trame NAK pour vous demander de retransmettre votre trame. "
            + "C'est pourquoi vous devriez en stocker une copie dans une mémoire tampon.";
        break;
      case 21113:
        msg = "La valeur du champ 'FCS' n'est pas correcte!";
        tip = "Le champ 'FCS' devrait correspondre au nombre de caractères visibles dans le texte du message, "
            + "mais la valeur actuelle du champ 'FCS' n'est pas identique à celle calculée par le programme."
            + "Probablement la faute est de votre côté ...";
        break;
      case 21115:
        msg = "Selon mes notes vous devriez utiliser le numéro de sequence <b>" + argument
            + "</b> pour cette trame!";
        tip = "Vous êtes libre de choisir le premier numéro de sequence, mais par la suite il faut être conséquent"
            + "dans la numérotation (indépendamment de la destination).";
        break;
      case 21116:
        msg = "Cette trame devrait être du type <i>'DAT'</i>, alors vérifiez svp le champ <i>'Type'</i>.";
        tip = "Cette trame a été reçu depuis la couche réseau (NL). Donc cela doit être une trame "
            + "du type <i>DAT</i>.";
        break;
      case 21117:
        msg = "Vérifier la valeur du champ <i>'Destination'</i>!";
        tip = "Envoyez cette trame à la prochaine station (sur le chemin vers la destination finale). L'adresse MAC "
            + "du champ <i>'Destination'</i> devrait donc correspondre à l'adresse IP du champ <i>'Next Node'</i>.";
        break;
      case 21118:
        msg = "Vérifier la valeur du champ 'Source') ?";
        tip = "Comme vous êtes l'émetteur de cette trame, l'adresse MAC dans le champ <i>'Source'</i> devrait être la vôtre.";
        break;
      case 21119:
        msg = "Seul l'option \"stocker une copie de cette trame\" devrait être choisie, pas les autres.";
        tip = "Il n'y pas de trame confirmée à supprimer, pas de trame à retransmettre, et pas d'acquittement à renvoyer.";
        break;
      case 21120:
        msg = "Le numéro de sequence est non valide, veuillez commencer avec un numéro entre 0 et 100.";
        tip = "Il faut commencer avec un numéro de sequence entre 0 et 100";
        break;
      case 23001:
        msg = "Il est interdit de manipuler les champs DLL de la trame reçue.";
        tip = "Les champs de la DLL restent inchangés. Toutefois, en fonction des données de la DLL,"
            + "vous devez choisir le type de transfert et les options de traitement appropriées.";
        break;
      case 23011:
        msg = "Considérez de répondre avec un acquittement (ACK ou NAK) pour ce message!";
        tip = "Vous avez reçu une trame de données de votre couche physique (PHL). Maintenant c'est à vous de "
            + "determiner, si cette trame est erronée ou pas (vérifiez le champ <i>'FCS'</i>). "
            + "Repondez avec un ACK si la trame est sans erreur, sinon avec un NAK.";
        break;
      case 23012:
        msg = "Ce n'est pas vous la source de cette trame ACK/NAK ? Vérifier le champ 'Source'!";
        tip = "L'adresse MAC dans le champ <i>'Source'</i> de la trame ACK/NAK devrait être la vôtre.";
        break;
      case 23013:
        msg = "Cette trame ACK/NAK devrait être renvoyé à l'expéditeur de la trame DAT!";
        tip = "Le champ 'Destination' de la trame ACK/NAK devrait indiquer l'adresse MAC de la station qui "
            + "vous a envoyé la trame DAT pour lequel vous préparez la trame ACK/NAK.";
        break;
      case 23014:
        msg = "Vérifier le type de trame (voir champ 'Type') choisi pour votre réponse!";
        tip = "Comme il s'agit d'un acquittement positif/négatif, le type de la trame devrait être ACK ou NAK.";
        break;
      case 23015:
        msg = "Le numéro de séquence dans la trame ACK/NAK n'est pas correct!";
        tip = "Ce numéro de séquence devrait correspondre à celui de la trame DAT que vous aimeriez confirmez.";
        break;
      case 23016:
        msg = "Seule l'option \"répondre avec un ACK/NAK\" devrait être choisi, pas les autres.";
        tip = "Il n'y pas de copie à stocker, pas de trame confirmée à supprimer, et pas de trame à retransmettre.";
        break;
      case 23017:
        msg = "Ne pas retourner des trames d'aquittement (ACK or NAK) vers le bas.";
        tip = "Les trames d'aquittement demande une action, mais la trame elle-même devrait finir içi.";
        break;
      case 23022:
        msg = "Les trames reçues de type ACK/NAK ne devraient pas être retournées à la couche physique (PHL) !";
        tip = "Vous avez reçu un message de confirmation ACK ou NAK. Indépendamment des options de traitement choisies "
            + "vous ne devriez pas passer plus loin cet acquittement.";
        break;
      case 23211:
        msg = "Si vous voulez envoyer un message NAK, alors vous ne devriez pas passer le message DAT plus loin!";
        tip = "Si le message reçu se révèle erroné, alors l'envoi d'un message NAK est correct. "
            + "Dans ce cas le message DAT ne devrait alors pas être passé plus loin.";
        break;
      case 23212:
        msg = "Il faut répondre avec un message NAK sur les message DAT erronés (valeur FCS incorrecte)!";
        tip = "Vous avez reçu un message dont le champ FCS n'est pas correct, donc le message est erroné. "
            + "En conséquence il faudrait envoyer une confirmation négative (message NAK) à l'expéditeur du message DAT.";
        break;
      case 23221:
        msg = "Il faudrait supprimer la trame confirmée de la mémoire tampon!";
        tip = "La trame d'acquittement (ACK) indique que un de vos trames envoyées est bien arrivé à la "
            + "prochaine station. Vous pouvez alors supprimer la copie de la trame confirmée de la mémoire tampon.";
        break;
      case 23222:
        msg = "Les trames de type autre que DAT ne devraient pas être passés à la couche réseau (NL) !";
        tip = "Vous avez reçu un message de confirmation ACK ou NAK. Indépendamment des options de traitement choisies "
            + "vous ne devriez pas passer plus loin ce message.";
        break;
      case 23223:
        msg = "Il faut juste choisir l'option pour supprimer la copie stockée du message confirmé!";
        tip = "Vous n'avez pas besoin des autres options de traitement.";
        break;
      case 23224:
        msg = "Il faut juste choisir l'option pour retransmettre le message correspondant à la confirmation négative!";
        tip = "Vous n'avez pas besoin des autres options de traitement.";
        break;
      case 23231:
        msg = "Considerez une retransmission de la trame correspondante à l'acquittement négatif (NAK)!";
        tip = "Vous avez reçu un acquittement négatif (NAK), c.à.d. une des trames émis par vous n'est pas arrivée correctement. Il faudrait alors retransmettre cette trame, une copie devrait être disponible dans la mémoire tampon.";
        break;
      case 23311:
        msg = "La trame DAT reçu est correcte et donc vous devriez l'acquitter!";
        tip = "Vous venez de recevoir une trame DAT avec une valeur FCS correcte, ce qui indique qu'il n'y avait pas d'erreur de transmission. Vous devriez confirmer la bonne réception de cette trame en renvoyant une trame ACK.";
        break;
      case 23312:
        msg = "Si vous confirmez cette trame, il faudrait aussi envisager de la délivrer à la couche réseau (NL) !";
        tip = "Vous venez de recevoir une trame DAT sans erreur. Vous avez décidé de la confirmer avec une trame ACK. Par conséquence vous devriez delivrer cette trame DAT à la couche supérieure (NL).";
        break;
      case 23313:
        msg = "Ne changez rien à la trame si vous la passez plus loin à la couche réseau (NL) !";
        tip = "Vous avez reçu une trame sans erreur. Si vous décidez de la délivrer à la couche supérieure,  "
            + "alors il faut la passer sans aucune modification.";
        break;
    }
  }

  else if (layer == 3) {
    // section = "Network Layer (NL)";
    switch (intErrCode) {
      case 30111:
        msg = "Révisez la décision de routage, c.à.d. le choix du prochaine noeud (champ 'Next Node')!";
        tip = "Depuis votre station il n'y a pas de liaison directe au noeud indiqué par le champ <i>('Next Node')</i>. Choisissez svp un autre prochain noeud <i>('Next Node')</i> sur le chemin vers la station finale <i>('Final Node')</i>. ";
        break;
      case 31111:
        msg = "Ne pas retourner cette trame à la couche 'AL'!";
        tip = "Ce message traité par vous venait de la couche 'AL'.<br>Il est inutile de le lui retourner.";
        break;
      case 31113:
        msg = "Le champ <i>'Next Node'</i> indique votre propre station !?! Drôle de routage ...";
        tip = "Décidez à l'aide du plan du réseau quel devrait être le prochain noeud sur le chemin vers la station finale. Saisir l'adresse de ce prochain noeud (pas votre station) dans le champ <i>'Next Node'</i>.";
        break;
      case 31114:
        msg = "Vérifier la valeur du champ <i>'Final Node'</i>.";
        tip = "Dans ce champ vous devriez saisir l'adresse la station finale<br>(comparez avec le champ <i>'To'</i>).";
        break;
      case 33012:
        msg = "Vérifier la valeur du champ <i>'Next Node'</i>.";
        tip = "Est-ce que vous aimeriez vraiment délivrer cette trame au noeud depuis lequel vous venez de le recevoir? "
            + "Ce n'est pas vraiment une bonne stratégie pour le routage ... ;-)";
        break;
      case 33013:
        msg = "Pourquoi changer la valeur du champ <i>'Final Node'</i>?";
        tip = "En principe la décision de routage ne touche que le champ<i>'Next Node'</i>, "
            + "mais pas le champ <i>'Final Node'</i> (qui doit rester inchangé)!";
        break;
      case 33014:
        msg = "Pourquoi changer la valeur du champ <i>'Next Node'</i> ?";
        tip = "Si la destination finale est atteinte, vous devriez pas modifier le champ <i>'Next Node'</i>.";
        break;
      case 33111:
        msg = "Considérez de faire un re-routage de cette trame (destination finale pas atteint) !";
        tip = "Puisque la station finale n'a pas encore été atteinte, vous devriez re-router cette trame,<br>"
            + "c.-à-d. il faut choisir le prochain noeud (champ <i>'Next Node'</i>) et delivrer la trame à la couche DLL.";
        break;
      case 33311:
        msg = "Considérez de délivrer cette trame à la couche <i>'AL'</i>!";
        tip = "L'adresse du champ <i>'Final Node'</i> est la vôtre. Donc cette trame est arrivée à sa destination finale "
            + "et devrait être delivrée à la couche supérieure";
        break;
      case 33312:
        msg = "Ne pas changer les champs de la couche réseau (<i>'NL'</i>), lorsque vous délivrez des trames à la couche <i>'AL'</i>!";
        tip = "Les trames étant arrivées à leur destination finale doivent être délivré à la couche supérieure sans aucune modification!";
        break;
    }
  }

return msg; //[msg, tip];
}
