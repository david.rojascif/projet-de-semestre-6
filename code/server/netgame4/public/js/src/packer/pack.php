<?php
echo("\nPacking JavaScript files ...\n\n");

if ($argc < 2) {
//  $files = glob("$webroot/media/js/src/db.*.js");
  $files = Array('monitor','player','admin');
  sort($files);
  foreach($files as $filename) {
    $path = realpath('.');
    $shortfilename = "$filename.js";
    $minfilename = "$path/../$filename.min.js";
    $filename = "$path/$filename.js";
    $tm = filemtime($filename);
    $tmmin = filemtime($minfilename);
    echo("----------------------\n");
    if ($tm > $tmmin) { // unpacked file is newer
      echo("$shortfilename needs UPDATE !\n");
      doPack($filename, $minfilename);
    } else {
      echo("$shortfilename: Ok\n");
    }
  }
  echo("----------------------\n");
}

echo("\nAny key to close ...");
$answer = fgets(STDIN);

//---------------------------------
function doPack($fileIn, $fileOut) {
//  echo("OK to pack this file ? : ");
//  $answer = fgets(STDIN);
//  if ($answer[0] != 'y') return;

  require_once 'class.JavaScriptPacker.php';

  $script = utf8_decode(file_get_contents($fileIn));

  $t1 = microtime(true);

  $packer = new JavaScriptPacker($script, 'Normal', true, false);
  $packed = $packer->pack();

  $t2 = microtime(true);
  $time = sprintf('%.4f', ($t2 - $t1) );
  echo "script '", basename($fileIn), "' packed in ", $time, ' s.', "\n";

  file_put_contents($fileOut, utf8_encode($packed));
}

?>
