/****************************************************************************
 * NetGame Player
 * (c) 2015-21 R. Scheurer (HEIA-FR)
 * Last modification: 2021-05-15
 ****************************************************************************/
var playerVersion = '<small><i>NetGame Player v210515</i></small>';
 console.log(playerVersion);

 //--- PRESETs on player HTML page ---
 // var wsPort = "<%= application.getInitParameter("wsPort") %>";
 // var presetNodeLayer = "<%= request.getParameter("nodelayer") %>";

// websocket
var connected = false;
var webSocketID = null;
// registration
var registeredNode = -1;
var registeredLayer = -1;
var registered = false;
var registrationOpen = false;
var registrationForcing = false;
var nodes, layers;
// msg treatment
var imgDirName = "images/";
var msgEmpty = { ID: -1, msgFields: [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1], msgString: "", direction: 0 };
var connectivity; // matrix of available connections
var msgQueue, msgBuffer; // input queue and buffer for sent messages
var recentMsg = msgEmpty;
var nextSeqNo = -1;
var validated = false;
var errorCodes = [];
var processingTimes = {}; // array of processing time per msg
var processingTime = 0; // current sum of processing time
var processStart = 0; // time of processing start
//var inactivityTimer = false;
//---------- CONSTANTS ---------------------
var imgDirectionNames = ['down','horiz','up'];
var layer2Nodes = ["1111", "2222","3333","4444","5555","6666","7777","8888","9999"];
var layer3Nodes = ["1.1.1.1", "2.2.2.2","3.3.3.3","4.4.4.4","5.5.5.5","6.6.6.6","7.7.7.7","8.8.8.8","9.9.9.9"];
var layer2Types = ["DAT","ACK","NAK"];
var LAYER_PHL = 0, LAYER_DLL = 1, LAYER_NL = 2, LAYER_AL = 3;
var DIR_UP = 1, DIR_DISC = 0, DIR_DOWN = -1;
var FIELD_SRC = 0, FIELD_DEST = 1, FIELD_TYPE = 2, FIELD_SEQNO = 3, FIELD_NEXT = 4, FIELD_FINAL = 5,
    FIELD_FROM = 6, FIELD_TO = 7, FIELD_MSG = 8, FIELD_FCS = 9;
var TYPE_DAT = 0, TYPE_ACK = 1, TYPE_NAK = 2;

//---------------------------------------------------------------------

/*
 * Binding events on DOM elements
 */
$(document).ready(function () {
  // info icon
  $('#idIconInfo').click(function() {
    showNotifDialog("<table width=\"100%\"><tr><td>" + lang.gameInfo
      + '<br><br>' + playerVersion +"</td><td>&nbsp;&nbsp;<img src='images/logo.png' width='250px'></td></tr></table>",
      lang.gameVersion);
  });
  // register/unregister buttons
  $('#idRegButtonRegister').click(function(event) {
    register(event);
    return false;
  });
  $('#idRegButtonUnregister').click(function() {
    if (registrationOpen || registrationForcing)
      unregister(false);
    else
      showNotifDialog(lang.regNoNewReg);
    return false;
  });
  // remove alias error state if alias entered
  $('#idRegAliasInput').change(function() {
    if (this.value) {
      $('#idRegAliasFormGrp').removeClass('has-error');
      $('#idRegErrorSpan').html('');
    }
  });
  // msg form
  $('#idProcButtonProcess').click(handleMsgFormProcess);
  $('#idProcButtonCancel').click(handleMsgFormCancel);
  $('input#idCBreplyACK').change(function() {
//    console.log("replyACK clicked");
    $('#idMsgFormTableACK').toggle();
  });
  // launch form
  $('#idLaunchButtonLaunch').click(processLaunchForm);
  
  // insert language specific texts to DOM
  $("[tkey]").each (function (index) {
    $(this).html(lang[$(this).attr('tkey')]);
  });
  $('input#idRegAliasInput').attr('placeholder',lang.regAliasPlaceholder);
  
  // messages in queue
  $('#idQueueMsgsDiv').on("click", ".msg", handleMsgClick);

  // Assure that all modals properly stack their backdrops
  // (needed for suspended over msg treatment dialog)
  $(document).on('show.bs.modal', '.modal', function () {
      var zIndex = 1040 + (10 * $('.modal:visible').length);
      $(this).css('z-index', zIndex);
      setTimeout(function() {
          $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
      }, 0);
  });
  
});

//---------------------------------------------------------------------

/**
 * Connect to the remote websocket.
 */
function connect() {
	var target = "ws://"+window.location.host+wsPort+"/websocket/playing";
  console.log("Websocket " + target);
  // check if browser supports WebSockets
	if ('WebSocket' in window) {
		ws = new WebSocket(target);
	} else if ('MozWebSocket' in window) {
		ws = new MozWebSocket(target);
	} else {
		alert(lang.error-noWS);
		return;
	}
  
	/**
	 * WebSocket connection opened
	 */
	ws.onopen = function () {
		log('Info: WebSocket connection opened.');
		connected = true;
	};
  
	/**
	 * Message received from websocket
	 */
	ws.onmessage = function (event) {
		log('Rcvd: ' + event.data);
		var jsonresult = JSON.parse(event.data);
		var type = jsonresult.type;
		var param = jsonresult.param;
		if (type == "Notification") {
			showNotifDialog(param.message);
    //------------------------------------
		} else if (type == "Connectivity") {
			connectivity = param.connectivity;
    //------------------------------------
		} else if (type == "InitConfig") {
			nodes = param.nodes;
			layers = param.layers;
      nextSeqNo = -1;
			fillRegistrationSelectors(nodes, layers, presetNodeLayer);
      fillMsgFormSelectors();
      registrationOpen = param.regState;
      if (presetNodeLayer != "null") registrationForcing = true;
      webSocketID = param.wsID;
      console.log('WebSocket ID = ' + webSocketID);
      $('#idRegPanelBody').toggleClass('hide', (!registrationOpen && !registrationForcing));
      $('#idRegPanelBodyNoRegister').toggleClass('hide', (registrationOpen || registrationForcing));
//      $('#idRegPanelBody').toggleClass('hide', !registrationOpen);
//      $('#idRegPanelBodyNoRegister').toggleClass('hide', registrationOpen);
    //------------------------------------
		} else if (type == "RegistrationReset"){
			$('#idProcDialog').modal('hide'); // close any message treatment dialog
      unregister(true);
    //------------------------------------
		} else if (type == "RegisterResult") {
			if (param.success) { // registration succeeded
        registeredNode = parseInt(registeredNode);
        registeredLayer = parseInt(registeredLayer);
        registered = true;
        registrationForcing = false;
        connectivity = param.connectivity;
        $('#idLangSelect').hide();
        $('#idRegPanelTitle').html(langify2(lang.regPlaying, layers[registeredLayer],nodes[registeredNode]));
        $('#idRegPanel').removeClass('panel-danger').addClass('panel-success');
        $('#btCollapseReg').removeClass('in');
        $('#idRegPanelBody').addClass('hide');
        $('#idRegPanelBodyUnregister').removeClass('hide');
        $('#idQueuePanel').removeClass('hide');
        if (registeredLayer == 1)
          $('#idBufferPanel').removeClass('hide');
        param.message = lang.regSuccess;
        window.onbeforeunload = onPageLeave;
        errorCodes = [];
        processingTimes = {};
        processingTime = processStart = 0;
        $('#idScore').show();
      } else { // registration error
        console.log("regError"+param.code);
        param.message = langify1(lang.regError, param.code) + lang["regError"+param.code];
      }
			showNotifDialog(param.message, "Registration");
    //------------------------------------
		} else if(type == "PlayerState") {
			msgQueue = param.msgQueue;
			msgBuffer = param.bufferQueue;
			showMsgQueue(msgQueue, msgBuffer);
      if (param.score >= 0)
        $('#idScoreBadge').html(Math.min(param.score,100));
    //------------------------------------
		} else if (type == "GameReset") {
	    if (registered) {
        unregister(true);
        registrationForcing = false;
  			$('#idProcDialog').modal('hide'); // close any message treatment dialog
			  showNotifDialog(lang.gameReset);
      }
    //------------------------------------
    } else if (type == "GameState") {
      $('#idNotifDisplay').modal(param.gameState ? 'hide' : 'show');
      // TODO: stop processing time while game is suspended !
      registrationOpen = param.regState;
      registrationForcing = false;
      if (!registered) {
        $('#idRegPanelBody').toggleClass('hide', !registrationOpen);
        $('#idRegPanelBodyNoRegister').toggleClass('hide', registrationOpen);
      }
    //------------------------------------
		} else if (type == "RegisterAllow") {
      registrationForcing = true;
      if (registered) {
        unregister(false);
  			showNotifDialog(lang.regCleared + lang.regRenew);
      } else {
        $('#idRegPanelBody').toggleClass('hide', false);
        $('#idRegPanelBodyNoRegister').toggleClass('hide', true);
      }
    //------------------------------------
		} else if (type == "LaunchMessage") {
      showLaunchFormDialog();
    }
	};
  
	/**
	 * Websocket connection is closed.
	 */
	ws.onclose = function (event) {
		connected = false;
		log('Info: WebSocket connection closed, Code: ' + event.code + (event.reason == "" ? "" : ", Reason: " + event.reason));
    $('#idNotifDisplayText').html(lang.connLost);
		$('#idNotifDisplay').modal('show');
    window.onbeforeunload = null;
	};

	ws.onerror = function (event) {
		log('Info: WebSocket error, Code: ' + event.code + (event.reason == "" ? "" : ", Reason: " + event.reason));
  };  
};

/**
 * Close websocket
 */
function disconnect() {
	if (ws != null) {
		ws.close();
		ws = null;
	}
};

/**
 * Send a message through websocket
 * @param message : the message String to send to the websocket
 */
function send(message) {
	if (ws != null) {
		log('SEND: ' + message);
		ws.send(message);
	} else {
		alert('ERROR: WebSocket connection not established, please reload page.');
	}
};

/**
 * Logger for debug use only.
 * @param message : debug message to display on debug console
 */
function log(message) {
  console.log(message);
};

/**
 * Called when the user wants to leave the page.
 * @returns {string} the message to show to the user
 */
function onPageLeave(event){
	return lang.alertLeave;
};

//---------------------------------------------------------------------

/**
 * Fill register form selects with nodes and layer names (received from server)
 * @param nodes: nodeslist to update
 * @param layers: layerslist to update
 * @param presetNodeLayer: pre-select a give node/layer
 */
function fillRegistrationSelectors(nodes, layers, presetNodeLayer) {
  fillSelect("idRegSelectNode", nodes, null);
  fillSelect("idRegSelectLayer", layers, null);
  if (presetNodeLayer != "null") {
    document.getElementById("idRegSelectNode").selectedIndex = Math.floor(presetNodeLayer / 10);
    document.getElementById("idRegSelectLayer").selectedIndex = presetNodeLayer % 10
  }
};

/**
 * Fill msg form selects
 */
function fillMsgFormSelectors() {
  fillSelectWithDefault("dllSrc", layer2Nodes);
  fillSelectWithDefault("dllDst", layer2Nodes);
  fillSelectWithDefault("dllType", layer2Types);
  fillSelectWithDefault("nlNextNode", layer3Nodes);
  fillSelectWithDefault("nlFinalNode", layer3Nodes);
  fillSelectWithDefault("alFrom", nodes);
  fillSelectWithDefault("alTo", nodes);
  fillSelectWithDefault("dllAckSrc", layer2Nodes);
  fillSelectWithDefault("dllAckDst", layer2Nodes);
  fillSelectWithDefault("dllAckType", layer2Types);
  fillSelectWithDefault("alFromLaunch", nodes);
  fillSelectWithDefault("alToLaunch", nodes);
};

/**
 * Fill a select with all options
 * @param id : id of select
 * @param values : Tab of values to add to the select
 */
function fillSelectWithDefault(id, values) {
  fillSelect(id, values, "-");
};

/**
 * Fill a specific select with all options and given default value
 * @param id : id of select
 * @param values : values to add to the select
 * @param defVal : default value to add to the select
 */
function fillSelect(id, values, defVal) {
	var select = document.getElementById(id);
	select.innerHTML = ''; // clear it
	if (defVal) { // default value
    var option0 = document.createElement("option");
    option0.value=-1;
    option0.innerHTML = defVal;
    select.appendChild(option0);
  }
  // fill values
	for (var i = 0; i<values.length; i++){
		var option = document.createElement("option");
		option.value=i;
		option.innerHTML = values[i];
		select.appendChild(option);
	}
};

//---------------------------------------------------------------------

/**
 * Register using values in form
 */
function register(event) {
	// if websocket disconnected, try to reconnect
	if (!connected) connect();
	var node = document.getElementById("idRegSelectNode").value;
	var layer = document.getElementById("idRegSelectLayer").value;
	var alias = document.getElementById("idRegAliasInput").value;
  if (event.ctrlKey && event.altKey) {
    node = "0"; layer = "2"; alias = "test";
  }
  if (!alias)
    alias = '';
  else
    alias = sanitize(alias);
  if (!alias.replace(/\s/g, '').length) { // whitespaces only?
    $('#idRegAliasFormGrp').addClass('has-error');
    $('#idRegErrorSpan').html(lang.regEnterAlias);
    document.getElementById("idRegAliasInput").value = '';
    return;    
  }
  $('#idRegAliasFormGrp').removeClass('has-error');
	registeredNode = parseInt(node);
	registeredLayer = parseInt(layer);
	var object = { type:"Register", param:{Node:node, Layer:layer, Alias:alias, Forced:registrationForcing}, error:""};
	var objstring = JSON.stringify(object);
	send(objstring);
};

/**
 * Assure that there's no malicious content in the string
 * @param {string} evil string to sanitize
 * @returns {string} sanitzed string
 */
function sanitize(evil) {
  var div = document.createElement('div');
  div.appendChild(document.createTextNode(evil));
  return div.innerHTML.trim();
};

function htmlEncode(plain) {
//  var elem = document.createElement('textarea');
//  elem.value = plain;
//  return elem.innerHTML;
  return sanitize(plain);
}

/**
 * Clear registration
 * @param reset : true if triggered by Admin
 */
function unregister(reset) {
	if (registered) {
    $('#idLangSelect').show();
		if (!reset) {
			var registermsg = {type:"ClearRegister", param:{}, error:"0"};
			send(JSON.stringify(registermsg));
		} else {
			showNotifDialog(lang.regCleared);
		}
	}
  $('#idRegPanelTitle').html(lang.regTitle);
  $('#idRegAliasInput').val("");
  $('#idRegPanel').removeClass('panel-success').addClass('panel-danger');
  $('#idRegPanelBody').toggleClass('hide', (!registrationOpen && !registrationForcing));
  $('#idRegPanelBodyNoRegister').toggleClass('hide', (registrationOpen || registrationForcing));
  $('#idRegPanelBodyUnregister').addClass('hide');
  $('#idQueuePanel').addClass('hide');
  if (registeredLayer == 1)
    $('#idBufferPanel').addClass('hide');
  $('#btCollapseReg').addClass('in');
  $('#idScore').hide();
	registeredNode = -1;
	registeredLayer = -1;
	registered = false;
  window.onbeforeunload = null;
};

/**
 * Update the list of queued / buffered messages
 * @param msgQueue: message queue to show on user screen
 * @param bufferQueue: buffer queue to show (layer2 only)
 */
function showMsgQueue(msgQueue, bufferQueue) {
	var msgList = document.getElementById("idQueueMsgsDiv");
  $('#idQueueBadge').text(msgQueue.length);
	if (msgQueue.length > 0) {
    msgList.innerHTML = '';
//    startInactivityTimer();
  } else {
    msgList.innerHTML = lang.queueNoItems;
//    stopInactivityTimer();
  }
	for (var i = 0; i<msgQueue.length; i++) {
		var msg = msgQueue[i];
		var msgdiv = document.createElement("div");
		var img = (msg.direction == -1) ? "down" : (msg.direction == 1) ? "up" : "horiz";
		var dllsrc =        (msg.msgFields[0] != -1) ? layer2Nodes[msg.msgFields[0]] :"";
		var dlldst =        (msg.msgFields[1] != -1) ? layer2Nodes[msg.msgFields[1]] :"";
		var dlltype =       (msg.msgFields[2] != -1) ? layer2Types[msg.msgFields[2]] :"";
		var dllseq =        (msg.msgFields[3] != -1) ? msg.msgFields[3] :"";
		var nlnextnode =    (msg.msgFields[4] != -1) ? layer3Nodes[msg.msgFields[4]] :"";
		var nlfinalnode =   (msg.msgFields[5] != -1) ? layer3Nodes[msg.msgFields[5]] :"";
		var alfrom =        (msg.msgFields[6] != -1) ? nodes[msg.msgFields[6]] :"";
		var alto =          (msg.msgFields[7] != -1) ? nodes[msg.msgFields[7]] :"";
		var almessage =     (msg.msgString) ? htmlEncode(msg.msgString) : "";
		var dllfcs =        (msg.msgFields[9] != -1) ? msg.msgFields[9] :"";
		msgdiv.innerHTML = '<table class="msg cursorptr" data="'+i+'" title="Message '+msg.ID+'">'+
		'<tr><td rowspan="3"><img src="images/' + img + '.gif" alt="'+img+'"></td><th colspan="4">Data Link Layer (DLL)</th><th colspan="2">Network Layer</th><th colspan="3">Application Layer</th><th>DLL</th></tr>'+
		'<tr class="fieldTitleRow"><td>Source</td><td>Destination</td><td>Type</td><td>Seq.Nr</td><td>Next Node</td><td>Final Node</td><td>From</td><td>To</td><td>Message</td><td>FCS</td></tr>'+
		'<tr>'+''+
			'<td>'+dllsrc+'</td><td>'+dlldst+'</td><td>'+dlltype+'</td><td>'+dllseq+'</td>'+
			'<td>'+nlnextnode+'</td><td>'+nlfinalnode+'</td>'+
			'<td>'+alfrom+'</td><td>'+alto+'</td><td>'+almessage+'</td>'+
			'<td>'+dllfcs+'</td>'+
		'</tr>'+
		'</table>';
		msgList.appendChild(msgdiv);
  }
  if (registeredLayer != 1) return;
    
  var bufferList = document.getElementById("idBufferMsgsDiv");
  $('#idBufferBadge').text(bufferQueue.length);
  bufferList.innerHTML = lang.bufferNoItems;
  if (bufferQueue.length > 0)
    bufferList.innerHTML = '';
  for (var i = 0; i<bufferQueue.length; i++) {
    var msg = bufferQueue[i];
    var msgdiv = document.createElement("div");
    var dllsrc =        (msg.msgFields[0] != -1) ? layer2Nodes[msg.msgFields[0]] :"";
    var dlldst =        (msg.msgFields[1] != -1) ? layer2Nodes[msg.msgFields[1]] :"";
    var dlltype =       (msg.msgFields[2] != -1) ? layer2Types[msg.msgFields[2]] :"";
    var dllseq =        (msg.msgFields[3] != -1) ? msg.msgFields[3] :"";
    var nlnextnode =    (msg.msgFields[4] != -1) ? layer3Nodes[msg.msgFields[4]] :"";
    var nlfinalnode =   (msg.msgFields[5] != -1) ? layer3Nodes[msg.msgFields[5]] :"";
    var alfrom =        (msg.msgFields[6] != -1) ? nodes[msg.msgFields[6]] :"";
    var alto =          (msg.msgFields[7] != -1) ? nodes[msg.msgFields[7]] :"";
    var almessage =     (msg.msgString) ? htmlEncode(msg.msgString) : "";
    var dllfcs =        (msg.msgFields[9] != -1) ? msg.msgFields[9] :"";
    msgdiv.innerHTML = '<table class="msg">'+
    '<tr><th colspan="4">Data Link Layer (DLL)</th><th colspan="2">Network Layer</th><th colspan="3">Application Layer</th><th>DLL</th></tr>'+
		'<tr class="fieldTitleRow"><td>Source</td><td>Destination</td><td>Type</td><td>Seq.Nr</td><td>Next Node</td><td>Final Node</td><td>From</td><td>To</td><td>Message</td><td>FCS</td></tr>'+
    '<tr>'+''+
      '<td>'+dllsrc+'</td><td>'+dlldst+'</td><td>'+dlltype+'</td><td>'+dllseq+'</td>'+
      '<td>'+nlnextnode+'</td><td>'+nlfinalnode+'</td>'+
      '<td>'+alfrom+'</td><td>'+alto+'</td><td>'+almessage+'</td>'+
      '<td>'+dllfcs+'</td>'+
    '</tr>'+
    '</table>';
    bufferList.appendChild(msgdiv);
  }
};

/**
 * Handle click on Cancel button of msg form dialog
 */
function handleMsgFormCancel() {
  if (processStart > 0) {
    processingTime = Date.now() - processStart;
//    console.log("processing time to add: " + (processingTime/1000) + " seconds");
    processStart = 0;
//    startInactivityTimer();
    if (!processingTimes.hasOwnProperty(recentMsg.ID))
      processingTimes[recentMsg.ID] = 0;
    processingTimes[recentMsg.ID] += processingTime;
//    console.log("processing time total: " + (processingTimes[recentMsg.ID]/1000) + " seconds");
  }
  $('#idMsgFormValidatedDiv').hide();
  unlockMsg();
};

/**
 * Unlock all messages
 */
function unlockMsg() {
  if (!registered) return;
  // unlock msg
  var tosend = {type:"Unlock", param:{}};		
  send(JSON.stringify(tosend));
};

/**
 * Handle click on msg in queue
 * Decide if normal treatment or auto-process
 */
function handleMsgClick(event) {
  var msgIndex = event.currentTarget.attributes.data.value;
  var msg = msgQueue[msgIndex];
  if (event.altKey && event.ctrlKey) { // automatic processing ?
    var processmdgsend = {type:"ProcessMsg", param:{MsgId:msg.ID, changedValue:-1}};
    errorCodes[msg.ID] = [];
    send(JSON.stringify(processmdgsend));
    return;
  } 
  // normal msg treatment
  showMsgFormDialog(msg);
};

/**
 * Validates message form filled in by player
 */
function handleMsgFormProcess(event) {
  if (!isValidUserInput(recentMsg) ) { // user input (form) has errors
    if (validated) { // form changed was valid before, but user has changed it!
      $('#idMsgFormValidatedDiv').slideUp(); // remove any previous submit prompt
      $('#idProcButtonProcess').html(lang.formValidate).removeClass('btn-success').addClass('btn-primary');
    }
    validated = false;
    event.preventDefault();
    event.stopImmediatePropagation();
    return false; 
  }
  if (!validated) { // user input ok, but now prompt to submit
    validated = true;
    processingTime = Date.now() - processStart;
    $('#idMsgFormValidatedDiv').slideDown();
    $('#idProcButtonProcess').html(lang.formSubmit).removeClass('btn-primary').addClass('btn-success');
    event.preventDefault();
    event.stopImmediatePropagation();
    return false; 
  }
  $('#idProcDialog').modal('hide');
  $('#idMsgFormValidatedDiv').hide();
	// determine the one relevant value to send:
	// NL : field nextNode 
	// DLL : field SeqNo (only if msg is going «down»)
	// PHL : field FCS (only if msg is going «down»)
	var relevantValue = 0;
	if (registeredLayer == LAYER_PHL) {
		if (recentMsg.direction == -1) {
			 var phyval = $("#idMsgFormPhyRaw").val();
			 var phytab = phyval.split("/");
			 if (phytab[phytab.length-1])
         relevantValue = parseInt(phytab[phytab.length-1]); // take last field (FCS)
		}
	} else if (registeredLayer == LAYER_DLL) {
		if (recentMsg.direction == -1) {
			relevantValue = $("#dllSeq").val();
      nextSeqNo++;
		}
	} else if (registeredLayer == LAYER_NL) {
		relevantValue = $("#nlNextNode").val();
	}
  if (processingTimes.hasOwnProperty(recentMsg.ID)) {
    processingTime += processingTimes[recentMsg.ID];
    delete processingTimes[recentMsg.ID]; // remove obsolete entry
  }
  console.log("msg processing time: " + (processingTime/1000) + " seconds");
	var processMsg = {
    type:"ProcessMsg",
    param:{MsgId:recentMsg.ID,
      changedValue:relevantValue,
      errorCodes:errorCodes[recentMsg.ID].join(), // send errorCodes as String with comma-separated codes
      processingTime: Math.floor((processingTime+0.5)/1000)
    }
  };
  errorCodes[recentMsg.ID] = [];
	send(JSON.stringify(processMsg));
};

/**
 * Displays a form that allows the player to launch a message
 */
function showLaunchFormDialog() {
  $('#idLaunchForm td').removeClass('has-error');
  $('#idLaunchFormErrorDiv').hide();
  $('#alFromLaunch').val(-1);
  $('#alToLaunch').val(-1);
  $('#alMsgLaunch').val("");
  $('#idLaunchDialog').modal('show');
};

/**
 * Handle the filled form to launch a message
 */
function processLaunchForm(event) {
  var isValid = true;
  var errMsg = "";
  $('#idLaunchForm .form-control').each(function () { // assure fields are not empty
    $(this).val($(this).val().trim());
    if ($(this).val() < 0 || $(this).val() === "") {
      $(this).parent().addClass('has-error');
      $(this).one('change', function() {
        $(this).parent().removeClass('has-error');
      });
      isValid = false;
      errMsg = lang.launchError1;
    }
  });
  var alFrom = $('#alFromLaunch').val();
  var alTo = $('#alToLaunch').val();
  if (isValid && alFrom == alTo) {
    $('#alFromLaunch').parent().addClass('has-error');
    $('#alToLaunch').parent().addClass('has-error');
    isValid = false;
    errMsg = lang.launchError2;
  }
  
//  var alMsg = sanitize($('#alMsgLaunch').val().trim());

  // https://jrgraphix.net/research/unicode_blocks.php
  // allow \x20-\u24F : Basic Latin, Latin-1 Supplement, Latin Extended-A, Latin Extended-Basic
  var alMsg = $('#alMsgLaunch').val().trim().replace(/[\x00-\x1F\u0250-\uFFFF]/g, ''); // remove fancy chars
  if (isValid && countSlashes(alMsg) > 4) { // limit slashes
    $('#alMsgLaunch').parent().addClass('has-error');
    isValid = false;
    errMsg = lang.launchError3;
  }
  if (isValid && alMsg.length > 50) { // limit to 50 char
    $('#alMsgLaunch').parent().addClass('has-error');
    isValid = false;
    errMsg = lang.launchError4;
  }
  if (!isValid) {
    $('#idLaunchFormErrorMsg').html(errMsg);
    $('#idLaunchFormErrorDiv').slideDown();
    event.preventDefault();
    event.stopImmediatePropagation();
    return false; 
  }
  $('#idLaunchFormErrorDiv').hide();
  $('#idLaunchDialog').modal('hide');
  console.log("Launch Message");
	var launchMsg = {type:"LaunchMsg", param:{fromNode:alFrom, toNode: alTo, msgTxt:alMsg}};
	send(JSON.stringify(launchMsg));
};

/**
 * Count the number of slashes ('/') in a given string
 * @param {string} str : string to look for slashes
 * @returns {number} count of slashes found
 */
function countSlashes(str) {
  var cnt=0;
  for(var i=0; i < str.length; i++) {
    cnt += str[i]==='/' ? 1 : 0;
  }
  return cnt;
}

/**
 * Display message treatment form/dialog
  * @param msg : msg to treat
 */
function showMsgFormDialog(msg) {
  if (msg === undefined || msg.ID < 0) { // msg should be a valid one
    console.log("ERROR in 'showMsgFormDialog': parameter 'msg' is not valid");
    return;
  }
  recentMsg = msg;
  processStart = Date.now();
//  stopInactivityTimer();
//  console.log("starting timer");
  validated = false;
  $('#idProcButtonProcess').html(lang.formValidate).removeClass('btn-success').addClass('btn-primary');
	// lock msg
  var tosend = {type:"Lock", param:{MsgId:msg.ID}}; 
	send(JSON.stringify(tosend));
  
  // general preparations
  if (!errorCodes[msg.ID])
    errorCodes[msg.ID] = [];
  $('#idMsgFormErrorDiv').hide();
  // enable/disable fields depending on layer
	$(".phy").prop("disabled", registeredLayer != LAYER_PHL);
  $(".phlonly").toggle(registeredLayer == LAYER_PHL);
  $(".dll").prop("disabled", registeredLayer != LAYER_DLL);
  $(".dllonly").toggle(registeredLayer == LAYER_DLL);
  $('#idProcessingNone').toggle(registeredLayer != LAYER_DLL);
	$(".nl").prop("disabled", registeredLayer != LAYER_NL);
	$(".al").prop("disabled", true);
  $('.msg .form-control').parent().removeClass('has-error'); // remove all has-error classes
//  $('input:radio').prop('checked',false); // clear all radio
  $('#idRadioDiscard').prop('checked',true); // select 'discard' by default

  // layer specific preparations
  if (registeredLayer == LAYER_PHL) {
    $('span[tkey="formDlvrDown"]').html(lang.formDlvrPHL);
		if (msg.direction == -1) {
      fillMsgFormFields(msg);
      $('#idMsgFormPhyRaw').prop("readonly", false).val(joinMsgFields(msg));
		} else if (msg.direction == 0) {
      $(".phy").prop("disabled", false);
      $(".dll").prop("disabled", false);
      $(".nl").prop("disabled", msg.msgFields[FIELD_TYPE] != TYPE_DAT);
      $(".al").prop("disabled", msg.msgFields[FIELD_TYPE] != TYPE_DAT);
      fillMsgFormFields(msgEmpty); // clear form fields with "empty" msg
      // fill raw msg and disable modifs
      $('#idMsgFormPhyRaw').val(msg.rawMsg).prop("readonly", true);
    }
  } else if (registeredLayer == LAYER_DLL) {
    $('span[tkey="formDlvrDown"]').html(lang.formDlvrDown);
    fillMsgFormFields(msg);
    $('input:checkbox').prop('checked', false); // clear all checkboxes
    $('#idMsgFormTableACK select').val("-1");
    $('#dllAckSeq').val("");
    $('#idMsgFormTableACK').hide(); // assure ACK/NAK table is hidden
  } else if (registeredLayer == LAYER_NL) {
    $('span[tkey="formDlvrDown"]').html(lang.formDlvrDown);
    fillMsgFormFields(msg);
  }
  // finally show modal dialog
  $('#idProcDialogTitle').html(langify1(lang.formTreatment, msg.ID));
  $('#idProcDialog').modal('show');
};

/**
 * Join message fields to suggest raw message content
 * @param msg : msg to put in raw format
 */
function joinMsgFields(msg) {
  var raw = layer2Nodes[msg.msgFields[0]]
    + layer2Nodes[msg.msgFields[1]]
		+ layer2Types[msg.msgFields[2]]
		+ msg.msgFields[3];
  if (msg.msgFields[FIELD_TYPE] == TYPE_DAT)
    raw += layer3Nodes[msg.msgFields[4]]
      + layer3Nodes[msg.msgFields[5]]
      + nodes[msg.msgFields[6]]
      + nodes[msg.msgFields[7]]
      + (msg.msgString ? msg.msgString : "");
  raw += msg.msgFields[9];
  return raw;
};

/**
 * Fill in msg form fields with values of given msg
 * @param msg : msg to display
 */
function fillMsgFormFields(msg) {
  var imgSrc = imgDirName + imgDirectionNames[msg.direction+1] + ".gif";
  $('#idMsgDirImg').attr('src',imgSrc);
	$("#dllSrc").val(msg.msgFields[0]); 
	$("#dllDst").val(msg.msgFields[1]); 
	$("#dllType").val(msg.msgFields[2]); 
	$("#dllSeq").val(msg.msgFields[3] < 0 ? '' : msg.msgFields[3]); 
	$("#nlNextNode").val(msg.msgFields[4]); 
	$("#nlFinalNode").val(msg.msgFields[5]); 
	$("#alFrom").val(msg.msgFields[6]); 
	$("#alTo").val(msg.msgFields[7]); 
	$("#alMessage").val(msg.msgString);
	$("#dllFCS").val(msg.msgFields[9] < 0 ? '' : msg.msgFields[9]); 
  // TODO: fill layer names in delivery options description
  // TODO: reset fields in ACK/NACK form
};

/**
 * Display modal notification dialog
 */
function showNotifDialog(message, title) {
  title = title || lang.notifTitle;
  $('#idNotifDialogTitle').html(title);
  $('#idNotifDialogBody').html(message);
  $('#idNotifDialog').modal('show');
};

/*
 * Validate player input befor submitting msg
 */
function isValidUserInput(msg) {
  // check if no visible & enabled form field is empty
  var errorCode = null;
  $('.form-control:visible:enabled').each(function () {
   if ($(this).val() < 0 || $(this).val() === "") {
//     console.log($(this));
     $(this).parent().addClass('has-error');
     $(this).one('change', function() {
       $(this).parent().removeClass('has-error');
     });
     errorCode = "00003"; // fill in all editable fields
   }
  });
  if (errorCode !== null) {
    errorCodes[msg.ID].push(errorCode);
    $('#idMsgFormErrorMsg').html('['+errorCode+'] '+getMsgAndTip(errorCode, errorArg));
    $('#idMsgFormErrorDiv').slideDown();    
    return false;
  }

  // now check logical input errors
//  var changedValue = null;
  var errorArg = null;
  var oldDirection = msg.direction; // down=-1, horiz=0, up=1
  var newDirection = $('input[name="treat"]:checked').val(); // down=-1, nothing=0, up=1
  var deliverToLayer = ((newDirection < 0 && registeredLayer == LAYER_PHL) ? 0 : registeredLayer + newDirection);
  var msgType = msg.msgFields[FIELD_TYPE]; // DAT=0, ACK=1, NAK=2
  console.log("VALIDATION (directions: old=" + oldDirection
      + " / new=" + newDirection + " / type=" + msgType + ")");
  console.log('registeredNode', registeredNode,'registeredLayer=',registeredLayer);

  // --- check PHL -------------------------
  if (registeredLayer == LAYER_PHL) {
    if (newDirection == DIR_DISC) // discard
      errorCode = "00001"; // do not discard msg
    else if (oldDirection == DIR_DISC) { // PHL1->PHL2
      if (newDirection == DIR_UP) { // PHL1->PHL2->DLL
        errorArg = compareRawAndFilledMsg(msg, newDirection, true);
        if (errorArg != null) {
          errorCode = "12303"; // filled msg does not correspond to raw message
        }
      } else { // PHL1->PHL2->PHL1
        errorCode = "12301"; // don't return raw frame to sender
      }
    } else if (oldDirection == DIR_DOWN) { // DLL->PHL
      if (newDirection == DIR_DOWN) { // DLL->PHL1->PHL2
        errorArg = compareRawAndFilledMsg(msg, newDirection, false);
        if (errorArg != null) {
          errorCode = "11203"; // raw message does not correspond to msg fields
        }
      } else {
        errorCode = "11201"; // DLL->PHL->DLL
      }
    } else {
      errorCode = "00099"; // internal error: oldDirection should never be 1
    }
  }
  
  // --- check DLL ----------------------------
  else if (registeredLayer == LAYER_DLL) {
    // determine selected processing options (1000=saveCopy/100=removeConfFrame/10=retransmit/1=replyAckNak)
    var processingOption = 0;
    $('input:checkbox:checked').each(function () {
      processingOption += parseInt($(this).val());
    });
//    console.log("processingOption = " + processingOption);

    // get msg form values
    var dllSrc = parseInt($("#dllSrc").val());
    var dllDst = parseInt($("#dllDst").val());
    var dllType = parseInt($("#dllType").val());
    var dllSeqRaw = $("#dllSeq").val();
    var dllSeq = parseInt(dllSeqRaw);
    if (dllSeqRaw != dllSeq) dllSeq = -1; // non-numeric dllSeq
    var dllFCS = parseInt($("#dllFCS").val());
    
    // now analyze directions
    if (oldDirection == DIR_UP) { //--- COMING UP ---
      if (dllSrc != msg.msgFields[FIELD_SRC] || dllDst != msg.msgFields[FIELD_DEST]
        || dllType != msg.msgFields[FIELD_TYPE] || dllSeq != msg.msgFields[FIELD_SEQNO]
        || dllFCS != msg.msgFields[FIELD_FCS]) {
        errorCode = "23001"; // manipulating any DLL field is not allowed
        // TODO reset field values to original values ?
      }
      else if (msg.msgFields[FIELD_TYPE] == TYPE_DAT) { // a DAT comes UP
        if (processingOption%2 != 1) { //ACK/NAK not checked
          errorCode = "23011"; // please send ACK/NAK
        } else { // ACK/NAK checked
          var dllAckSrc = parseInt($("#dllAckSrc").val());
          var dllAckDst = parseInt($("#dllAckDst").val());
          var dllAckType = parseInt($("#dllAckType").val());
          var dllAckSeq = parseInt($("#dllAckSeq").val());
          if (dllAckType == TYPE_DAT) {
            errorCode = "23014"; // wrong frame type
          } else {
            // validations independent of frame type
            var frametype = layer2Types[dllAckType];
            if (dllAckSrc != registeredNode) {
              errorCode = "23012";
              errorArg = frametype; // wrong source address
            } else if (dllAckDst != msg.msgFields[FIELD_SRC]) {
              errorCode = "23013";
              errorArg = frametype; // ACK/NAK to be returned to sender
            } else if (dllAckSeq != msg.msgFields[FIELD_SEQNO]) {
              errorCode = "23015";
              errorArg = frametype; // wrong sequence number
            }
          }
          // validations dependent of frame type
          if (dllAckType == TYPE_NAK) {
            if (isValidFCS(msg, dllFCS)) {
              errorCode = "23311"; // send ACK !
            } else if (newDirection != DIR_DISC) {
              errorCode = "23211"; // discard DAT if sending NAK
            }
          } else if (dllAckType == TYPE_ACK) {
            if (!isValidFCS(msg, dllFCS)) {
              errorCode = "23212"; // send NAK!
            } else if (newDirection != DIR_UP) {
              errorCode = "23312"; // pass on DAT if sending ACK
            } else if (dllSrc != msg.msgFields[FIELD_SRC] || dllDst != msg.msgFields[FIELD_DEST]
                   || dllType != msg.msgFields[FIELD_TYPE] || dllSeq != msg.msgFields[FIELD_SEQNO]) {
              errorCode = "23313"; // do not change any DLL field
            }
          }
          if (processingOption != 1)
            errorCode = "23016"; // no other processing options besides NAK/ACK
        }
      } else { // an ACK or NAK is coming UP
        if (newDirection == DIR_UP)
          errorCode = "23222"; // discard ACK/NAK
        else if (newDirection == DIR_DOWN)
          errorCode = "23022"; // discard ACK/NAK
        else if (msgType == TYPE_ACK) {
          if (!$('#idCBremove').is(':checked')) {
            errorCode = "23221"; // remove ack'ed frame from buffer
          } else if (processingOption != 100)
            errorCode = "23223"; // no other processing options besides RMV
        } else if (msgType == TYPE_NAK) {
          if (!$('#idCBretransmit').is(':checked')) {
            errorCode = "23231"; // retransmit your frame from buffer
          } else if (processingOption != 10)
            errorCode = "23224"; // no other processing options besides RTM
        }
      }
    } else { //--- COMING DOWN ----
      if (newDirection != DIR_DOWN) {
        errorCode = "21111"; // pass frame to PHL
      } else if (!isValidFCS(msg, dllFCS)) {
        errorCode = "21113"; // reconsider FCS value
        // } else if (msg.msgFields[3] < 0) {
        // errMsg = "21114"; // fill in sequence number
      } else if (dllSrc != registeredNode) {
        errorCode = "21118"; // verify source
      } else if (dllDst != msg.msgFields[FIELD_NEXT]) {
        errorCode = "21117"; // check destination
      } else if (dllType != TYPE_DAT) {
        errorCode = "21116"; // send DAT frame!
      } else if ((nextSeqNo >= 0) && (dllSeq != nextSeqNo)) {
        errorCode = "21115";
        errorArg = "" + nextSeqNo; // should use seq no xy now
      } else if ((nextSeqNo < 0) && (dllSeq <0 || dllSeq > 100)) {
        errorCode = "21120"; // invalid starting seqNr (should be 0<=secNr<=100)
      } else if (!$('#idCBsave').is(':checked')) {
        errorCode = "21112"; // save frame copy
      } else if (processingOption != 1000) {
        errorCode = "21119"; // no other processing options besides SAVE
      } else {
  //      changedValue = msg.msgFields[FIELD_SEQNO];
        if (nextSeqNo < 0) nextSeqNo = dllSeq;
      }
    }
  }

  // --- check NL ---------------------------
  else if (registeredLayer == LAYER_NL) {
    // get values from msg form
    nlNext = parseInt($('#nlNextNode').val());
    nlFinal = parseInt($('#nlFinalNode').val());

    
    if (newDirection == DIR_DISC) { // down & discard
      errorCode = "00001"; // do not discard msg
    } else if (oldDirection == DIR_DOWN) { // down & ...
      if (newDirection == DIR_UP) { // down & up
        errorCode = "31111"; // no return to AL
      } else { // down & down
        if (nlNext == registeredNode) {
          errorCode = "31113"; // next field should not be your own
        } else if (nlFinal != msg.msgFields[FIELD_TO]) {
          errorCode = "31114"; // verify final node
        } else if (!connectivity[nlNext]) {
          errorCode = "30111"; // wrong routing decision (no link to next node)
        }
      }
    } else { // up & ...
      if (newDirection == DIR_UP) { // up & up
        if (msg.msgFields[FIELD_TO] != registeredNode) {
          errorCode = "33111"; // re-routing (final node not yet reached)
        } else if (nlFinal != msg.msgFields[FIELD_TO]) {
          errorCode = "33013"; // verify final node (don't change it)
        } else if (nlNext != msg.msgFields[FIELD_NEXT]) {
          errorCode = "33014"; // verify next node (don't change it)
        }
      } else if (newDirection == DIR_DOWN) { // up & down
        if (msg.msgFields[FIELD_TO] == registeredNode) {
          errorCode = "33311"; // deliver to AL (final node reached
        } else if (nlNext == registeredNode) {
          errorCode = "31113"; // next node equals your own
        } else if (nlNext == msg.msgFields[FIELD_SRC]
                   && hasRoutingAlternatives(nlNext)) {
          errorCode = "33012"; // next node is where we got msg from
        } else if (nlFinal != msg.msgFields[FIELD_TO]) {
          errorCode = "33013"; // verify final node (don't change it)
        } else if (!connectivity[nlNext]) {
          errorCode = "30111"; // wrong routing decision (incorrect next node)
        }
      }
    }
//    msg.msgFields[FIELD_NEXT] = nlNext;  // NOT NEEDED
//    msg.msgFields[FIELD_FINAL] = nlFinal;
//    changedValue = msg.msgFields[FIELD_NEXT];
  }
  
  console.log("errorCode = " + errorCode);
  if (errorCode !== null) {
    errorCodes[msg.ID].push(errorCode);
    $('#idMsgFormErrorMsg').html('['+errorCode+'] '+getMsgAndTip(errorCode, errorArg));
    $('#idMsgFormErrorDiv').slideDown();    
    return false;
  }
  $('#idMsgFormErrorDiv').slideUp();     
  return true;
};

/**
 * Checks if there is a link to any other node than the given one
 * @param nextNodeID : id of currently selected next node
 * @returns {boolean}
 */
function hasRoutingAlternatives(nextNodeID) {
  for (i=0; i<connectivity.length; i++) {
    if (connectivity[i] && i != nextNodeID)
      return true;
  }
  return false;
};

/*
function getNextNode(msg) {
  for (i=0; i<connectivity.length; i++)
    if (connectivity[i] && i != msg.msgFields[FIELD_SRC])
      return i; // found nextnode that is different from src
  return msg.msgFields[FIELD_SRC]; // no other choice, return to src
} */

/**
 * Check if msg field values and raw string match.
 * Used to check if player has well done his job.
 * @param msg : the message to analyse
 * @param direction : the current direction of this message
 * @returns {string} null if ok, otherwise a string indicating error position
 */
function compareRawAndFilledMsg(msg, direction) {
  var cmpStr, rawStr;
  if (direction == -1) { // coming down from DLL (to be sent)
    cmpStr = layer2Nodes[msg.msgFields[0]] + "/"
      + layer2Nodes[msg.msgFields[1]] + "/"
      + layer2Types[msg.msgFields[2]] + "/"
      + msg.msgFields[3];
    if (msg.msgFields[2] == TYPE_DAT) {
      cmpStr += "/"
        + layer3Nodes[msg.msgFields[4]] + "/"
        + layer3Nodes[msg.msgFields[5]] + "/"
        + nodes[msg.msgFields[6]] + "/"
        + nodes[msg.msgFields[7]] + "/"
        + msg.msgString.replace(/\//g,"//");
      console.log("char stuffed: " + msg.msgString.replace(/\//g,"//"));
    }
    cmpStr += "/" + msg.msgFields[9];
    rawStr = $('#idMsgFormPhyRaw').val();
  } else { // coming from PHL of an other node (received)
    cmpStr = layer2Nodes[$("#dllSrc").val()] + "/"
      + layer2Nodes[$("#dllDst").val()] + "/"
      + layer2Types[$("#dllType").val()] + "/"
      + $("#dllSeq").val();
    if (msg.msgFields[2] == TYPE_DAT) {
      cmpStr +=  "/"
        + layer3Nodes[$("#nlNextNode").val()] + "/"
        + layer3Nodes[$("#nlFinalNode").val()] + "/"
        + nodes[$("#alFrom").val()] + "/"
        + nodes[$("#alTo").val()] + "/"
        + $("#alMessage").val().replace(/\//g,"//");
    }
    cmpStr += "/" + $("#dllFCS").val();
    console.log("char de-stuffed: " + msg.rawMsg);
    rawStr = msg.rawMsg;
  }
  // compare cmpStr and phy field
//  return cmpStr == rawStr;
  var pos = findFirstDiffPos(cmpStr, rawStr);
  if (pos < 0) return null;
  return rawStr.substr(0,pos) + "<span class=\"error-pos\">"
    + rawStr.charAt(pos)+"</span>" + rawStr.substr(pos+1);
}

function findFirstDiffPos(a, b) {
  var shorterLen = Math.min(a.length, b.length);
  for (var i=0; i < shorterLen; i++) {
    if (a[i] !== b[i]) return i;
  }
  if (a.length !== b.length) return shorterLen;
  return -1;
}

/*
 *
 */
function isValidFCS(msg, fcs) {
  console.log('FCS for "'+ msg.msgString.replace(/\s/g,'') + '" is ' + msg.msgString.replace(/\s/g,'').length);
  return msg.msgString.replace(/\s/g,'').length == fcs;
}

/*
 * Get language specific text
 */
function langify1(str, arg1) { return str.replace('{1}',arg1); }
function langify2(str, arg1, arg2) { return str.replace('{1}',arg1).replace('{2}',arg2); }

//--- Finally open WebSocket connection ------------------
connect();
