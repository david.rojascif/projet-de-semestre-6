/****************************************************************************
 * NetGame Monitor
 * (c) 2015-17 R. Scheurer (HEIA-FR)
 ****************************************************************************/

var connectivity;
var playersInfo = {}; 
var gameSuspended = false;
var registrationOpen = true;
var LAYER_PHL = 0, LAYER_DLL = 1, LAYER_NL = 2, LAYER_AL = 3;
var MAX_NODES = 9;
var MAX_LAYERS = 3;

//-----------------  WEBSOCKET ----------------------------------------------

/**
 * Connect to the remote websocket.
 */
function connect() {
	var target = "ws://"+window.location.host+wsPort+"/websocket/monitor?gameMonitor&ip="+monitorIP;
  console.log("Websocket " + target);
  // check if browser supports WebSockets
	if ('WebSocket' in window) {
		ws = new WebSocket(target);
	} else if ('MozWebSocket' in window) {
		ws = new MozWebSocket(target);
	} else {
		alert('Unfortunately, WebSockets are not supported by this browser.');
		return;
	}
	/**
	 * WebSocket Connection opened
	 */
	ws.onopen = function () {
		log('Info: WebSocket connection opened.');
    initPlayersInfo();
	};
	/**
	 * Message received from websocket
	 */
	ws.onmessage = function (event) {
		log('Rcvd: ' + event.data);
		var jsonresult = JSON.parse(event.data);
		var type = jsonresult.type;
		var param = jsonresult.param;
    //---------------------
		if (type == "GameState") {
      gameSuspended = !param.gameState;
      $('#idNotifDisplay').modal(param.gameState ? 'hide' : 'show');
      registrationOpen = param.regState;
      $('#idRegistrationState').toggle(registrationOpen);
      updateAllPlayers();
    //---------------------
		} else if (type == "ConnectivityUpdate") {
			connectivity = param.connectivity;
			updateRouting();
    //---------------------
		} else if (type == "GameReset") {
			connectivity = param.connectivity;
      initPlayersInfo();
      $('#blackOverlay').hide();
      $('#idRoutingCanvas').hide();
      $('#idRankingDiv').hide(); 
      $('#idHistoryDiv').hide(); 
      updateAllPlayers();
    //---------------------
		} else if (type == "MonitorInit") {
      $('#idNotifDisplay').modal(param.gameState ? 'hide' : 'show');
      registrationOpen = param.regState;
      $('#idRegistrationState').toggle(registrationOpen);
			playersInfo.QueuedMsgCounters = param.queuedMsgCounters;
			playersInfo.BufferedMsgCounters = param.bufferedMsgCounters;
			playersInfo.Aliases = param.aliases;
			updateAllPlayers();
    //---------------------
		} else if (type == "LayerUpdate") {
      playersInfo.QueuedMsgCounters[param.nodeID][param.layerID] = param.queuedMsgCnt;
      playersInfo.BufferedMsgCounters[param.nodeID] = param.bufferedMsgCnt;
      updatePlayer(param.nodeID, param.layerID);
    //---------------------
		} else if (type == "AliasAllReset") {
      for (var n=0; n<MAX_NODES; n++ ) {
        for (var l=0; l<MAX_LAYERS; l++ ) {
          playersInfo.Aliases[n][l] = null;
        }
      }
			updateAllPlayers();
    //---------------------
		} else if (type == "AliasReset") {
      playersInfo.Aliases[param.nodeID][param.layerID] = null;
      updatePlayer(param.nodeID, param.layerID);
    //---------------------
		} else if (type == "AliasUpdate") {
      playersInfo.Aliases[param.nodeID][param.layerID] = param.alias;
      $("#idN" + param.nodeID + "L" + param.layerID + " .alias").html(param.alias);
    //---------------------
		} else if (type == "ToggleOverlay") {
      toggleOverlay(param);
    //---------------------
    } else {
      console.log("*** UNKNOWN message type '" + type + "' ***");
    }
	};
	/**
	 * Websocket connection is closed.
	 */
	ws.onclose = function (event) {
		log('The WebSocket has closed (reload the page to restart).');
    $("#idNotifDisplayText").html("Connection to server lost,<br>please reload page ...");
    $('#idNotifDisplay').modal('show');
	};
  
	ws.onerror = function (event) {
		log('Info: WebSocket error, Code: ' + event.code + (event.reason == "" ? "" : ", Reason: " + event.reason));
  };  

}

//-----------------  UTILITY FUNCTIONS ----------------------------------------

function initPlayersInfo() {
  playersInfo.Aliases = [];
  playersInfo.QueuedMsgCounters = Array(MAX_NODES);
  playersInfo.BufferedMsgCounters = Array(MAX_NODES);
  for (var n=0; n<MAX_NODES; n++ ) {
    playersInfo.BufferedMsgCounters[n] = 0;
    playersInfo.Aliases[n] = Array(MAX_LAYERS);
    playersInfo.QueuedMsgCounters[n] = Array(MAX_LAYERS);
    for (var l=0; l<MAX_LAYERS; l++ ) {
      playersInfo.Aliases[n][l] = null;
      playersInfo.QueuedMsgCounters[n][l] = 0;
    }
  }
}

//-----------------  DOM MANIPULATION ------------------------------------------

// iterate over all players to update monitor
function updateAllPlayers() {
  for (var nodeID=0; nodeID<MAX_NODES; nodeID++) {
	  for (var layerID=0; layerID<MAX_LAYERS; layerID++) {
      updatePlayer(nodeID, layerID);
    }
	}
}  

// update monitor for a single player using data stored in playersInfo
function updatePlayer(nodeID, layerID) {
  var selector = "#idN" + nodeID + "L" + layerID;
  var icon = "person";
  var bgColor = "bg-green";
	var alias = false;
	if (playersInfo.Aliases && playersInfo.Aliases[nodeID][layerID])
	  alias = playersInfo.Aliases[nodeID][layerID];
  if (alias === false) {
    alias = "&nbsp;";
    if (!registrationOpen) {
      icon = "agent"; bgColor = "bg-agent";
    } else {
      icon = "pc"; bgColor = "bg-white";
    }
  }
  var queuedMsgCount = playersInfo.QueuedMsgCounters[nodeID][layerID];
  if (queuedMsgCount > 0)
    bgColor = (queuedMsgCount > 2 ? "bg-red" : "bg-yellow");
  var badge = queuedMsgCount;
  if (layerID == LAYER_DLL)
    badge += " | " + (playersInfo.BufferedMsgCounters[nodeID] ? playersInfo.BufferedMsgCounters[nodeID] : 0);
  // now update DOM
  $(selector+" img").attr("src","../images/" + icon + ".gif");
  $(selector).removeClass("bg-agent bg-green bg-yellow bg-red").addClass(bgColor);
  $(selector+" .alias").html(alias);
  $(selector+" .badge").html(badge);
}

function updateRouting() {
	var matrixH = [[0,1], [1,2], [3,4], [4,5], [6,7], [7,8]];
	for (var m=0; m<6; m++)
	  $("#imgH"+m).toggle(connectivity[matrixH[m][0]][matrixH[m][1]]);
	var matrixV = [[0,3], [1,4], [2,5], [3,6], [4,7], [5,8]];
	for (var m=0; m<6; m++)
		$("#imgV"+m).toggle(connectivity[matrixV[m][0]][matrixV[m][1]]);
	var matrixD = [[0,4,1,3], [1,5,2,4], [3,7,4,6], [4,8,5,7]];
	for (var m=0; m<4; m++) {
		if (connectivity[matrixD[m][0]][matrixD[m][1]]) {
			if (connectivity[matrixD[m][2]][matrixD[m][3]]) { 
          $("#imgD"+m).prop("src", "../images/monitor/link_diag_crossed.png");
     		} else {  
          $("#imgD"+m).prop("src", "../images/monitor/link_diag_ullr.png");
     		}
  		$("#imgD"+m).show();
		} else if (connectivity[matrixD[m][2]][matrixD[m][3]]) { 
			$("#imgD"+m).prop("src", "../images/monitor/link_diag_llur.png");
			$("#imgD"+m).show();
 		} else { 
			$("#imgD"+m).hide();
		}
	}
}

//-----------------  OVERLAYS ---------------------------------------

function toggleOverlay(param) {
  if (param.overlay == null) {
    $('#blackOverlay:visible').fadeOut();
    $('#idRankingDiv').fadeOut();
    $('#idHistoryDiv').fadeOut();
    $('#idRoutingCanvas').fadeOut();
    if (gameSuspended)
      $('#idNotifDisplay').modal('show');
    return;
  } else {
    if (gameSuspended)
      $('#idNotifDisplay').modal('hide');
    if (param.overlay == "ranking") {
      $('#idRankingFrame').attr( 'src', "/admin/monitor/ranking" ); // load ranking
      $('#idRankingDiv').fadeIn();
    }
    else if (param.overlay == "history") {
      $('#idHistoryFrame').attr( 'src', "/admin/monitor/history" ); // load history
      $('#idHistoryDiv').fadeIn();
    }
    else if (param.overlay == "routing")
      showRoutingOverlay(param);
  }
}

function showRoutingOverlay(param) {
  var rows = 3, dy = 210, my = 86; // node placement
  var cols = 3, dx = 330, mx = 142; // node placement
  var circleBase = 1; // base radius [pixel]
  var circleFactor = 4; // radius increment per msg [pixel]
  // preprocess traffic data
  var data = [];
  for (var i = 0; i < param.trafficStats.length; i++) {
    for (var j = 0; j < param.trafficStats[i].length; j++) {
      var cnt = param.trafficStats[i][j];
      if (param.trafficStats[i][j] > 0) {
        data.push([i,j,cnt]);
      }
    }
  }
  // preprocess destined/arrived data
  var data2 = [];
  for (var k = 0; k < param.trafficStats.length; k++) {
    data2.push([param.arrivedStats[k],param.destinedStats[k]]);
  }
  // preprocess sent/arrived data
  var data3 = [];
  for (var k = 0; k < param.trafficStats.length; k++) {
    data3.push([param.sentAndArrivedStats[k],param.sentStats[k]]);
  }
  // start drawing
  var canvas = document.getElementById('idRoutingCanvas');
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.lineCap = "round";
    for (var n = 0; n < data.length; n++) {
      ctx.strokeStyle = "aqua";
      ctx.lineWidth = data[n][2];
      p1 = data[n][0];
      p2 = data[n][1];
      ctx.beginPath();
      ctx.moveTo(mx+((p1%cols)*dx), my+(Math.floor(p1/3)*dy));
      ctx.lineTo(mx+((p2%cols)*dx), my+(Math.floor(p2/3)*dy));
      ctx.stroke();
    }
    ctx.lineWidth = 1;
    ctx.font = "24px Arial";
    var ndx = 0;
    for (var r = 0; r < rows; r++)
      for (var c = 0; c < cols; c++) {
        ctx.strokeStyle = "white";
        // write arcs
        ctx.fillStyle = "darkgreen";
        ctx.beginPath();
        ctx.arc(mx+(c*dx), my+(r*dy),data2[ndx][1]*circleFactor,0,Math.PI,false);
        ctx.fill();
        //ctx.stroke();
        ctx.fillStyle = "darkmagenta";
        ctx.beginPath();
        ctx.arc(mx+(c*dx), my+(r*dy),data3[ndx][1]*circleFactor,Math.PI,2*Math.PI,false);
        ctx.fill();
        //ctx.stroke();
        ctx.fillStyle = "lightgreen";
        ctx.beginPath();
        ctx.arc(mx+(c*dx), my+(r*dy),circleBase + (data2[ndx][0]*circleFactor),0,Math.PI,false);
        ctx.fill();
        //ctx.stroke();
        ctx.fillStyle = "magenta";
        ctx.beginPath();
        ctx.arc(mx+(c*dx), my+(r*dy),circleBase + (data3[ndx][0]*circleFactor),Math.PI,2*Math.PI,false);
        ctx.fill();
        //ctx.stroke();
        // write counts
        var percent1 = data2[ndx][1] > 0 ? " ("+Math.floor(100*data2[ndx][0]/data2[ndx][1])+"%)" : "";
        var percent2 = data3[ndx][1] > 0 ? " ("+Math.floor(100*data3[ndx][0]/data3[ndx][1])+"%)" : "";
        var txt = ""+data2[ndx][0]+"/"+data2[ndx][1]+percent1;
        var txt2 = ""+data3[ndx][0]+"/"+data3[ndx][1]+percent2;
        ctx.fillStyle = "lightgreen";
        ctx.fillRect(mx+(c*dx)+20,my+(r*dy)+12,ctx.measureText(txt).width+10,34);
        ctx.fillStyle = "magenta";
        ctx.fillRect(mx+(c*dx)+20,my+(r*dy)-48,ctx.measureText(txt2).width+10,34);
        ctx.fillStyle = "black";
        ctx.fillText(txt, mx+(c*dx)+25, my+(r*dy)+37);
        ctx.fillText(txt2, mx+(c*dx)+25, my+(r*dy)-23);
        ndx++;
      }
    ctx.fillStyle = "magenta";
    ctx.fillRect(0,0,24,34);
    ctx.fillStyle = "lightgreen";
    ctx.fillRect(24,0,24,34);
    ctx.fillStyle = "black";
    ctx.fillText("S", 4, 26);
    ctx.fillText("R", 28, 26);
    $('#blackOverlay').fadeIn();
    $('#idRoutingCanvas').fadeIn();
  }
}

function log(message) {
  console.log(message);
}
//-----------------  CONNECT ----------------------------------------------

// finally:  connect to websocket
connect();
