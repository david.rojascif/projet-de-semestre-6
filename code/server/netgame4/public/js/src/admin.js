var playersInfo = {};
/**
 * Connect to the remote websocket.
 */
function connect() {
    var target = "ws://" + window.location.host + "/websocket/monitor?admin" + (role==='AdminMaster'?'Master':'Prof') + "&ip=" + remoteAddr;
    console.info("Websocket " + target);
    // check if WebSocket works with browser
    if ('WebSocket' in window) {
        ws = new WebSocket(target);
    } else {
        alert('WebSocket is not supported by this browser.');
    }

    /**
     * WebSocket Connection opened
     */
    ws.onopen = function () {
        console.log('Info: WebSocket connection opened.');
        initPlayersInfo();
    };
    /**
     * Message received from websocket
     */
    ws.onmessage = function (event) {
        console.log('Rcvd: ' + event.data);
        var jsonresult = JSON.parse(event.data);
        var type = jsonresult.type;
        var param = jsonresult.param;
        //---------------------
        if (type == "GameState") {
            $('#idGameState').html(param.gameState ? '' : 'SUSPENDED')
                .toggleClass('suspended', !param.gameState);
            $('#idRegistrationState').html(param.regState ? 'REGISTRATION OPEN' : '')
                .toggleClass('openReg', param.regState);
            $('#game-running-dot').toggleClass('dot-red', !param.gameState);
            $('#registration-dot').toggleClass('dot-red', !param.regState);
            $('#pause-resume-span').html((param.gameState ? "<button href='/admin/cmd/run' title=\"Temporarely suspend the game\"><img src='/images/suspend.png'> Suspend</button>" : "<button href='/admin/cmd/run'  title=\"Resume the game\"><img src='/images/resume.png'> Resume </button>"));
            $('#registration-span').html((param.regState ? "<button href='/admin/cmd/reg' title=\"Disable player registrations\"><img src='/images/registration-close.png'> Close Registration </button>" : "<button href='/admin/cmd/reg' title=\"Enable player registrations\"><img src='/images/registration.png'> Open Registration </button>"));
            registrationOpen = param.regState;
            updateAllPlayers();
            //---------------------
        } else if (type == "GameReset") {
            initPlayersInfo();
            updateAllPlayers();
            //---------------------
        } else if (type == "MonitorInit") {
            $('#idGameState').html(param.gameState ? '' : 'SUSPENDED')
                .toggleClass('suspended', !param.gameState);
            $('#idRegistrationState').html(param.regState ? 'REGISTRATION OPEN' : '')
                .toggleClass('openReg', param.regState);
            registrationOpen = param.regState;
            playersInfo.QueuedMsgCounters = param.queuedMsgCounters;
            playersInfo.BufferedMsgCounters = param.bufferedMsgCounters;
            playersInfo.Aliases = param.aliases;
            updateAllPlayers();
            //---------------------
        } else if (type == "LayerUpdate") {
            playersInfo.QueuedMsgCounters[param.nodeID][param.layerID] = param.queuedMsgCnt;
            playersInfo.BufferedMsgCounters[param.nodeID] = param.bufferedMsgCnt;
            updatePlayer(param.nodeID, param.layerID);
            //---------------------
        } else if (type == "AliasAllReset") {
            for (var n=0; n<nodeLength; n++ ) {
                for (var l=0; l<=layerLength; l++ ) {
                    playersInfo.Aliases[n][l] = null;
                }
            }
            updateAllPlayers();
            //---------------------
        } else if (type == "AliasReset") {
            playersInfo.Aliases[param.nodeID][param.layerID] = null;
            updatePlayer(param.nodeID, param.layerID);
            //---------------------
        } else if (type == "AliasUpdate") {
            playersInfo.Aliases[param.nodeID][param.layerID] = param.alias;
            $("#idN" + param.nodeID + "L" + param.layerID + " .alias").html(param.alias);
            //---------------------
        } else if (type == "ConnectivityUpdate") {
            // no connectivity info showed on stations monitor, thus we ignore this one
            //---------------------
        } else if (type == "ToggleOverlay") {
            $('#idOverlayState').html(!param.overlay ? '' : 'OVERLAY')
                .toggleClass('overlay', param.overlay);
            $('#idOverlayState-cmd').html("Monitor"+(param.overlay ? " <img src='/images/game-monitor.png'>" : ""))
                .toggleClass('background-yellow', param.overlay);
            $('#monitor-options-td').html(param.overlay ? '<button href="/admin/cmd/hideOverlay" title="Hide overlay on game monitor"><img src="/images/hide2.png">Hide Overlay</button>':
                '<button href="/admin/cmd/routing" title="Show routing stats on game monitor"><img src="/images/routing.png"> Show Routing</button><br>' +
                '<button href="/admin/cmd/ranking" title="Show player ranking on game monitor"><img src="/images/ranking.png"> Show Ranking</button>');
            //---------------------
        } else {
            console.log("*** UNKNOWN message type '" + type + "' ***");
        }
    };
    /**
     * Websocket connection is closed.
     */
    ws.onclose = function (event) {
        console.log('The WebSocket has closed (reload the page to restart).');
        $('#idNotifDisplay').show();
    };
};

function initPlayersInfo() {
    playersInfo.Aliases = [];
    playersInfo.QueuedMsgCounters = Array(nodeLength);
    playersInfo.BufferedMsgCounters = Array(nodeLength);
    for (var n=0; n<nodeLength; n++ ) {
        playersInfo.BufferedMsgCounters[n] = 0;
        playersInfo.Aliases[n] = Array(layerLength);
        playersInfo.QueuedMsgCounters[n] = Array(layerLength);
        for (var l=0; l<layerLength; l++ ) {
            playersInfo.Aliases[n][l] = false;
            playersInfo.QueuedMsgCounters[n][l] = 0;
        }
    }
};

// iterate over all players to update monitor
function updateAllPlayers() {
    for (var nodeID=0; nodeID<nodeLength; nodeID++) {
        for (var layerID=0; layerID<=layerLength-1; layerID++) {
            updatePlayer(nodeID, layerID);
        }
    }
};

// update monitor for a single player using data stored in playersInfo
function updatePlayer(nodeID, layerID) {
    var selector = "#idN" + nodeID + "L" + layerID;
    var icon = "person";
    var bgColor = "bg-green";
    var alias = false;
    if (playersInfo.Aliases && playersInfo.Aliases[nodeID][layerID])
        alias = playersInfo.Aliases[nodeID][layerID];
    if (alias === false) {
        alias = "<img src=\"/images/unknown.png\">";
        if (!registrationOpen) {
            icon = "agent"; bgColor = "bg-agent";
        } else {
            icon = "pc"; bgColor = "bg-white";
        }
    }
    var queuedMsgCount = playersInfo.QueuedMsgCounters[nodeID][layerID];
    if (queuedMsgCount > 0)
        bgColor = (queuedMsgCount > 2 ? "bg-red" : "bg-yellow");
    var badge = queuedMsgCount;
    if (layerID === 1)
        badge += " | " + (playersInfo.BufferedMsgCounters[nodeID] ? playersInfo.BufferedMsgCounters[nodeID] : 0);
    // now update DOM
    $(selector+" .agent").attr("src","/images/" + icon + ".gif");
    $(selector).removeClass("bg-agent bg-green bg-yellow bg-red").addClass(bgColor);
    $(selector+" .alias").html(alias);
    $(selector+" .badge").html(badge);
};

// connect WebSocket
connect();

/**
 * Functions related to command execution
 */

// used to prevent use of back button
history.pushState(null, 'do not use back button', '/admin');

function displayResponse(data) {
    $('#cmd-output').html(data);
    if ($('#timestamp')) {
        $('#durationString').html($('#timestamp').html());
    } 
    if ($('#okmsg')) { // show and then fadeout confirmation message
        $('#okmsg').show().delay(4000).fadeOut(1000);
    }
}

function handleCommands(e) {
    // don't handle buttons inside forms in this callback and the monitor button
    if (!e.currentTarget.attributes.href  || e.currentTarget.attributes.href.value.endsWith("/admin/monitor")) {
        console.log('ignoring', e.target);
        return;
    }
    if (typeof e.currentTarget.attributes['click']!=="undefined") {
        if (!confirm(e.currentTarget.attributes['click'].nodeValue)) {
            return;
        }
    }
    const href = e.currentTarget.attributes.href.value;
    console.log("Handling GET " + href);

    // links to '#' will reset result div
    if (href === '#') {
        $('#cmd-output').html('');
        return false;
    }
    // handle mode changes
    else if (href === '/admin/cmd/toggleMode') {
        $('.expert-cmd').toggle(); // locally change visibility of commands
        $('#icon-expert-mode').toggleClass('icon-less icon-more');
    }

    $.get(href)
        .done(displayResponse)
        .fail(function(jqXHR) {
            console.log(jqXHR);
            $('#cmd-output').html("<p class='errmsg'>Internal error: AJAX GET request failed !</p>");
        });
    return false; // prevents default action AND propagation
}

function handleForms(e) {
    var form;
    if (e.currentTarget.type === "select-one") {
        form = e.currentTarget.parentNode;
        console.log(form.action);
        // handle agent timer and direct feedback in command panel
        if (form.action.endsWith("/admin/cmd/periodic")) {
            console.log(e.currentTarget.value);
            $('#timer-running-icon').toggleClass('hidden', e.currentTarget.value === "0");
        }
    } else if (e.currentTarget.action.endsWith("/admin/logout?_method=DELETE")) {
        this.submit();
        return false;
    } else {
        form = e.currentTarget;
    }
    $.post(form.action, $(this).serialize())
        .done(displayResponse)
        .fail(function() {
            $('#cmd-output').html("<p class='errmsg'>Internal error: AJAX POST request failed !</p>");
        });
    return false; // prevents default action AND propagation
}

// prevent use of back button of browser
window.onpopstate = function (event) {
    console.log('popstate called, ignoring back button');
    $('#cmd-output').html("<p class='errmsg'>Please do NOT use back button !</p>");
    history.pushState(null, 'do NOT use back button', '/admin');
};

window.addEventListener('DOMContentLoaded', (event) => {
    // delegate click for buttons and links; delegate submit for forms
    $('#panel-stations').on('click', 'a', handleCommands);
    $('#panel-cmd, #cmd-output').on('click', 'button, a, area', handleCommands); // area --> routing changes
    $('#panel-cmd, #cmd-output').on('submit', 'form', handleForms);
    $('#panel-cmd, #cmd-output').on('change', '.select-form-submit', handleForms);
});
