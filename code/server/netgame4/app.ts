'use strict';
import express from 'express';
import router from "./router";
import wsrouter from './wsrouter';
import { Socket } from 'net';
import { logger } from './log';


const app = express();
import passport = require('passport');
import session = require('express-session');
import flash = require('express-flash'); // https://www.npmjs.com/package/express-flash
import methodOverride = require('method-override'); // https://www.npmjs.com/package/method-override

// Set template engine to ejs
app.set('view engine', 'ejs');

app.use(session({
   secret: "AAEHftfcb5EwlFfWBquAZ6z1",
   resave: false,
   saveUninitialized: false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride('_method'));

// Configure routes and static resources
app.use('/', router);
app.all('/js/src/*', function(req, res) { // protect directory '/js/src' in static folder 'public'
   res.redirect('/error/404.html');
});
app.use(express.static('public'));

// 404 handler
app.use((_req, res) => res.redirect('/error/404.html'));

// Configure the webserver on port 3000, ipv4 only
app.set('port', 3000);
const server = app.listen(app.get('port'), "0.0.0.0", () => {
   logger.info('NetGame4 server listening on http://localhost:' + app.get("port"));
});

// Handles the websocket connections and send to websocket router
server.on('upgrade', (request, socket, head) => wsrouter(request, (socket as Socket), head));
