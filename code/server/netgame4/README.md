# NetGame4

NetGame is a didactical network simulation game developped by Rudolf Scheurer (rudolf.scheurer@hefr.ch).

This version 4 of NetGame is implemented using **TypeScript** and **EJS** as template engine.

## Content Structure

This folder contains all the files necessary to build and
operate a working instance of the NetGame.

The **source code** is organised as follows :

- **netgame** : classes implementing the game engine
- **auth** : configuration of admin authentification
- **controllers** : classes handling messages from/to monitors and players
- **sockets** : websocket servers and JSON message definitions
- **public** : static files to be served by the express server
- **routes** : files to handle the logic for the different routes of the application
- **views** :  webpage templates rendered by EJS template engine

## Credits

- The initial commit represents code as developped by *Julien Torrent* during his semester project PS5 in 2020/21 (migration of NetGame3 for Tomcat towards Node).
- Implementation of EJS version of admin interface done by *Florian Chassot* during his internship spring 2021.
