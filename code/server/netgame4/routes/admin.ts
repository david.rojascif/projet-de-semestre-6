import * as express from "express";
// NetGame core
import { NetGame } from "../netgame/Netgame";
import { NetGameInit } from "../netgame/NetgameInit";
// WebSocket server
import { MonitorServer } from "../sockets/MonitorServer";
import { AdminController } from "../controllers/AdminController";
// monitor sub-router
import monitor from "./monitor";
// logger
import { logger } from "../log";

const router = express.Router();

//--- Authentication using Passport
import passport from "passport";
import { Accounts } from "../auth/Accounts";
require('../auth/passport-config');


// https://medium.com/@mmajdanski/express-body-parser-and-why-may-not-need-it-335803cd048c
router.use(express.json()); // support json encoded bodies
router.use(express.urlencoded({ extended: true })); // support encoded bodies

//======================
// route prefix : /admin
//======================

//--- login routes (unauthenticated access)

router.get('/login', assureNotAuthenticated, (req, res) => {
    req.session
    res.render('login');
});

router.post('/login', assureNotAuthenticated, passport.authenticate('local', {
    successReturnToOrRedirect :'/admin',
    failureRedirect: '/admin/login',
    failureFlash: true // enables flash error messages
}));

//--- assure authentication for all routes below

router.use(assureAuthenticated, (req, res, next) => { // all routes below pass through this check
    next();
});

//--- routes accessible if authenticated
//TODO what is the remoteAddr ?
router.get('/', (req, res) => {
    const remoteAddr = req.socket.remoteAddress;
    res.render('admin', {netGame: NetGame.getNetGame(), netGameInit: NetGameInit, admin: AdminController, remoteAddr, log: logger, role: req.user.role, monitorServer: MonitorServer});
});

router.get('/cmd', (req, res) => {
    res.render('cmd', {netGame: NetGame.getNetGame(), monitorServer: MonitorServer, admin: AdminController, netGameInit: NetGameInit, log : logger});
});

router.get('/cmd/:cmd', (req, res) => {
    const viewParms = AdminController.pickFunction(req);
    res.render('cmd', {parms: viewParms, netGame: NetGame.getNetGame(), monitorServer: MonitorServer, admin: AdminController, netGameInit: NetGameInit, log : logger, role: req.user.role});
});

router.post('/cmd/:cmd', (req, res) => {
    const viewParms = AdminController.pickFunction(req);
    res.render('cmd', {parms: viewParms, netGame: NetGame.getNetGame(), monitorServer: MonitorServer, admin: AdminController, netGameInit: NetGameInit, log : logger});
});

router.delete('/logout', (req, res, next) => {
    if (req.user.role !== "AdminMaster") {
        Accounts.isProfConnected=false;
    }
    req.logout(function(err) {
        if (err) { return next(err); }
        res.redirect('/');
    });
})

router.use('/monitor', monitor);

//--- functions to check authentication state

/* eslint-disable @typescript-eslint/no-explicit-any */
function assureAuthenticated(req: any, res: any, next: any) {
    if (req.isAuthenticated()) {
        return next();
    }
    if (req.session) {
        req.session.returnTo = req.originalUrl;
    }
    res.redirect('/admin/login');
}

function assureNotAuthenticated(req: any, res: any, next: any) {
    if (req.isAuthenticated()) {
        return res.redirect('/admin');
    }
    next();
}

export default router;