import * as express from "express";
import { PlayerModel } from "./models/playermodel";

const router = express.Router();

// route /player
router.get('/', (req, res) => {
    const model = new PlayerModel();
    model.remoteAddr = req.connection.remoteAddress;

    switch (req.query.lang as string) {
        case "fr":
            model.lang = "fr";
            break;
        case "de":
            model.lang = "de";
            break;
    }
    model.nodelayer = (req.query.nodelayer ?? "null") as string;

    res.render('player', { 'model': model });
});

export default router;