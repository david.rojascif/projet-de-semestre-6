export class PlayerModel {
    /** default language for the player */
    lang: string = "en";

    /** remote address of the player*/
    remoteAddr: string;

    /** Port for the websockets, if empty defaults to server port*/
    wsPort: string = "";

    /** default layer for this player*/
    nodelayer: string = "null";
}