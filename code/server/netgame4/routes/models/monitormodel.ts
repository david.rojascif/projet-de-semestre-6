import { NetGame } from "../../netgame/Netgame";

export class MonitorModel {
    /** Port for the websockets, if empty defaults to server port*/
    wsPort: string = "";

    /** Remote address of the client */
    remoteAddr: string = "";

    /** Allows to show an overlay if the game is not running*/
    isRunning: boolean = NetGame.getNetGame().gameRunning;

    /** Contains the translation to display in the monitor page*/
    translations: Translations = new Translations();
}

export class Translations {
    /** Text to show when the registrations are open */
    regOpen: string = "REGISTRATION OPEN";

    /** Name of the school */
    titleUni: string = "Univ. of Applied Sciences Fribourg (CH)";

    /** Title of the overlay when the game is supended */
    suspTitle: string = "Notification";

    /** Message to show when the game is suspended */
    suspText: string = "Game has been suspended!<br>Please wait on instructions ...";
}