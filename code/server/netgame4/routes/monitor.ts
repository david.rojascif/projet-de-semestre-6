import * as express from "express";
import { MessageHistory } from "../netgame/stats/MessageHistory";
import { Layers } from "../netgame/enums/Layers";
import { NetGame } from "../netgame/Netgame";
import { NetGameInit } from "../netgame/NetgameInit";
import { MonitorModel } from "./models/monitormodel";

const router = express.Router();

//==============================
// route prefix : /admin/monitor
//==============================

router.get('/', (req, res) => {

    const model = new MonitorModel();
    model.remoteAddr = req.connection.remoteAddress;

    switch (req.query.lang as string) {
        case "de":
            model.translations.regOpen = "REGISTRATION OFFEN";
            model.translations.titleUni = "HTA-FR, Fribourg (CH)";
            model.translations.suspTitle = "Mitteilung";
            model.translations.suspText = "Das Spiel wurde unterbrochen!<br>Bitte warten Sie auf Instruktionen ...";
            break;
        case "fr":
            model.translations.regOpen = "REGISTRATION OUVERTE";
            model.translations.titleUni = "HES-SO Fribourg (CH)";
            model.translations.suspTitle = "Notification";
            model.translations.suspText = "NetGame est suspendu!<br>Prière d'attendre des instructions ...!";
            break;
    }

    const link = drawLinks();
    const nodes = drawNodes();

    res.render('monitor', { model: model, "link": link, "nodes": nodes });
});

router.get("/ranking",(req, res) => {
    const ranking = drawRanking();
    res.render('ranking', {ranking : ranking});
});

router.get("/history", (req, res) => {
    res.render('history', {msgID : MessageHistory.lastMsgID, history: MessageHistory.lastData});
});
export default router;

//------------------------------------------------------------------

const drawLinks = function () {
    const sx = 170, sy = 90, dx = 330, dy = 210;
    let html = "";
    for (let m = 0; m < 6; m++) {
        html += '<img id="imgH' + m + '" class="connectivity" src="../images/monitor/link_horiz.png" style="top:' + (sy + Math.floor(m / 2) * dy) + 'px; left:' + (sx + 80 + (m % 2) * dx) + 'px;">';
        html += '<img id="imgV' + m + '" class="connectivity" src="../images/monitor/link_vert.png" style="top:' + (sy + 10 + Math.floor(m / 3) * dy) + 'px; left:' + (sx + (m % 3) * dx) + 'px;">';
    }
    for (let m = 0; m < 4; m++) {
        html += '<img id="imgD' + m + '" class="connectivity" src="../images/monitor/link_diag_none.png" style="top:' + (sy + 50 + Math.floor(m / 2) * dy) + 'px; left:' + (sx + 110 + (m % 2) * dx) + 'px;">';
    }
    return html;
};

const drawNodes = function () {
    const sx = 50, sy = 30, dx = 330, dy = 210;
    let html = "";
    for (let m = 0; m < 9; m++) {
        html += '<div id="idST1" class="panel panel-primary station" style="top:' + (sy + Math.floor(m / 3) * dy) + 'px; left:' + (sx + (m % 3) * dx) + 'px;">';
        html += '<div class="panel-heading">' + (m + 1) + '- ' + NetGameInit.NODE_NAMES[m] + '</div>';
        html += '<ul class="list-group">';
        html += '<li id="idN' + m + 'L2" class="list-group-item"><img src="../images/pc.gif"><span class="alias">Layer 3</span><span class="badge">0</span></li>';
        html += '<li id="idN' + m + 'L1" class="list-group-item"><img src="../images/pc.gif"><span class="alias">Layer 2</span><span class="badge">0 | 0</span></li>';
        html += '<li id="idN' + m + 'L0" class="list-group-item"><img src="../images/pc.gif"><span class="alias">Layer 1</span><span class="badge">0</span></li>';
        html += '</ul>';
        html += '</div>';
    }
    return html;
};
const drawRanking = function () {

    const stats = NetGame.getNetGame().RankingStats;
    const ranking: number[][] = Array.from(Array(Layers.AL), () => new Array(NetGameInit.NODE_NAMES.length));

    for (let i = 0; i < Layers.AL; i++) {
        for (let j = 0; j < NetGameInit.NODE_NAMES.length; j++) {
            ranking[i][j] = (stats[j][i].scoreAvg * 10) + j;
        }

        ranking[i] = ranking[i].sort();
    }

    const sx = 50, sy = 30, dx = 330, dy = 210;
    const bgcolors = ["bg-gold", "bg-silver", "bg-bronze"];

    let html = "";
    for (let m = 0; m < 3; m++) {
        let oldscore = -1, rank = 0, exaequo = 1;

        html += '<div class="panel panel-primary station" style = "top:' + (sy + Math.floor((m / 3)) * dy) + 'px; left:' + (sx + (m % 3) * dx) + 'px;">';
        html += '<div class="panel-heading" >' + NetGameInit.LAYER_NAMES[m] + '</div>';
        html += '<ul class="list-group" >';
        for (let i = 8; i >= 0; i--) {

            const alias = NetGame.getNetGame().Nodes[ranking[m][i] % 10].Layers[m].player?.alias;
            const score = ranking[m][i] / 10;
            let bg = "";
            if (alias && score > 0) {
                if (score == oldscore)
                    exaequo++;
                else {
                    rank += exaequo;
                    exaequo = 1;
                }
                if (rank < 4) bg = bgcolors[rank - 1];
                html += '<li class="list-group-item">';
                html += '<span class="alias">' + (exaequo > 1 ? "&nbsp;&nbsp;&nbsp;" : rank + ". ") + alias + '</span>';
                html += '<span class="badge ' + bg + '" >' + score + '</span>';
                html += '</li>'
                oldscore = score;
            }
        }
        if (oldscore < 0) { // no players displayed
            html += '<li class="list-group-item "> no players </li>';
        }
        html += '</ul>';
        html += '</div>';
    }
    return html;
};
