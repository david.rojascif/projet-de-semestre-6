import * as express from "express";
// import sub-routers
import admin from "./routes/admin";
import index from "./routes/index";
import player from "./routes/player";

const router = express.Router();

// mount express paths, any addition middleware can be added as well.
router.use('/admin', admin);
router.use('/player', player);
router.use('/', index);

// Export the router
export default router;
