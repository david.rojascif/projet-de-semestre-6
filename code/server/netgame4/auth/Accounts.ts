/**
 * Local & static definition of user accounts
 */
export class Accounts {

    /** list of users */
    public static users = [
        {id : 0, username : "admin", role : "AdminMaster", password : "$2b$10$msEHFHd6P418HvuNNEjXDOa9UOMgVxSGOCcerYEslzM9.4xvCEK2C"},  //password master
        {id : 1, username : "prof",  role : "AdminProf",   password : "$2b$10$v9eYy3wQg8luFHcNVgpejuVc0sGavlB1omvRfS8xJsg1sIvsJYi1e"}]; //password prof



    /** remembers if there's already a user with role 'AdminProf' connected */
    public static isProfConnected = false;
}