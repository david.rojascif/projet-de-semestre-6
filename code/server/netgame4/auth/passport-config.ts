/**
 * Configures Passport to use Passport-Local strategy with locally defined accounts
 */

import bcrypt = require('bcryptjs');
import passport from "passport";
import { Strategy as LocalStrategy} from "passport-local";
import { Accounts } from "./Accounts";
import { logger } from "../log";

// prepare functions
const getUserByUsername = (username: string) => Accounts.users.find((user: { username: string; }) => user.username === username);
const getUserById = (id: number) => Accounts.users.find(user => user.id === id);
const authenticateUser = async (username:string, password:string, done:any) => {
  const msg = "Wrong username and/or password";
  const user = getUserByUsername(username);
  if (user == null) {
    logger.warn("Invalid login attempt for user : "+username);
    return done(null, false, { message: msg })
  }
  try {
    if (await bcrypt.compare(password, user.password)) {
      if (user.role !== "AdminMaster") {
          if (Accounts.isProfConnected) {
            logger.warn("Refusing second user 'prof'");
            return done(null, false, { message: "There's already a user 'prof' logged in" });
          }
          Accounts.isProfConnected = true;
          logger.info("User 'prof' is now logged in")
          return done(null, user)
      }
      logger.info("User 'admin' is now logged in")
      return done(null, user);
    } else {
      logger.warn("Invalid login attempt for user : "+username);
      return done(null, false, { message: msg });
    }
  } catch (e) {
    return done(e);
  }
}

// now configure Passport
passport.use(new LocalStrategy({ }, authenticateUser));
passport.serializeUser((user:Express.User, done:any) => done(null, user.id));
passport.deserializeUser((id:number, done:any) => {
  return done(null, getUserById(id));
});
