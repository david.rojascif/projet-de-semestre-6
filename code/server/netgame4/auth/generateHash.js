/**
 * Tool to generate password hash (using bcrypt)
 * run it using "node dist/auth/generateHash.js"
 */
/* eslint-disable @typescript-eslint/no-var-requires */
const express = require('express');
const app = express();
const session = require('express-session');
const readline = require("readline");
const bcrypt = require('bcryptjs');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

app.use(session({
    secret: "AAEHftfcb5EwlFfWBquAZ6z1",
    resave: false,
    saveUninitialized: false
}))

rl.question("Enter the password to hash : ", async function(password) {
    let hash = await bcrypt.hash(password, 10);
    console.log(`The hash for password '${password}' is : ${hash}`);
    rl.close();
})

rl.on("close", function() {
    process.exit(0);
});